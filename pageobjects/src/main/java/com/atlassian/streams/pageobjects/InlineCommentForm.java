package com.atlassian.streams.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.streams.api.common.Option;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

import static com.atlassian.streams.pageobjects.ActivityItemInlineAction.STATUS_BAR;
import static com.atlassian.streams.pageobjects.Waits.hasClass;
import static com.google.common.base.Suppliers.ofInstance;

public class InlineCommentForm
{
    private static final By COMMENT_FORM = By.className("activity-item-comment-form");
    private static final By COMMENT_FORM_TEXT_AREA = By.tagName("textarea");
    private static final By COMMENT_FORM_SUBMIT = By.name("submit");

    private @Inject AtlassianWebDriver driver;

    private @Inject PageBinder pageBinder;

    private final Supplier<WebElement> activityItem;

    public InlineCommentForm(WebElement activityItem)
    {
        this.activityItem = ofInstance(activityItem);
    }

    @WaitUntil
    public void waitUntilForIsVisible()
    {
        driver.waitUntilElementIsLocatedAt(COMMENT_FORM_TEXT_AREA, activityItem.get());
        driver.waitUntil(hasClass("ready", COMMENT_FORM, Option.<SearchContext>some(activityItem.get())));
    }

    public InlineCommentForm enterText(String message)
    {
        activityItem.get().findElement(COMMENT_FORM_TEXT_AREA).sendKeys(message);
        return this;
    }

    public ActivityItem submit()
    {
        activityItem.get().findElement(COMMENT_FORM_SUBMIT).click();
        driver.waitUntilElementIsNotVisibleAt(COMMENT_FORM_TEXT_AREA, activityItem.get());
        driver.waitUntilElementIsVisibleAt(STATUS_BAR, activityItem.get());
        return pageBinder.bind(ActivityItem.class, activityItem.get());
    }

}
