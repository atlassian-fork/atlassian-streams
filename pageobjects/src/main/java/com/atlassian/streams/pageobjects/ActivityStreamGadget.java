package com.atlassian.streams.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.streams.api.common.Option;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.utils.MouseEvents;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.pageobjects.Waits.hasClass;
import static com.atlassian.streams.pageobjects.Waits.not;
import static com.atlassian.streams.pageobjects.Waits.or;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.transform;

public class ActivityStreamGadget
{
    private static final String STREAM_CONTAINER_CLASSNAME = "activity-stream-container";
    private static final By STREAM_CONTAINER = By.className(STREAM_CONTAINER_CLASSNAME);

    private @Inject AtlassianWebDriver driver;

    private @Inject PageBinder pageBinder;

    @FindBy(id = "stream-title")
    private WebElement title;

    @FindBy(id = "activity-stream-feed")
    private WebElement feedLink;

    @FindBy(id = "filter-icon")
    private WebElement filter;

    @FindBy(css = "div.aui-message.aui-message-warning")
    private WebElement warningBox;


    public Iterable<ActivityItem> getActivityItems()
    {
        return transform(driver.findElements(By.className("activity-item")), toActivityItem);
    }

    @WaitUntil
    public void waitUntilInitializedAndFilterOptionsLoaded()
    {
        driver.waitUntil(not(hasClass("initializing", By.tagName("body"))));
        driver.waitUntil(or(
                or(hasClass("filter-view", STREAM_CONTAINER), hasClass("list-view", STREAM_CONTAINER), hasClass("full-view", STREAM_CONTAINER)),
                hasClass("not-configurable", STREAM_CONTAINER))
        );
    }

    public Link getFeedLink()
    {
        return new Link(feedLink);
    }

    /**
     * Sets the path to the servlet and reloads the activity stream.
     * @param servletPath the path, such as "/servlet/plugins/streams".
     * Do not inlcude the base url (http://localhost:3990/streams).
     * @return a new reloaded ActivityStreamGadget.
     */
    public ActivityStreamGadget setServletPath(String servletPath)
    {
        String js = String.format("ActivityStreams.setServletPath('%s');", servletPath);
            driver.executeScript(js);
        driver.switchTo().defaultContent();

        // Hover over the title
        WebElement gadgetRenderBox = driver.findElement(By.cssSelector("div.gadget"));
        String frameId = gadgetRenderBox.findElement(By.tagName("iframe")).getAttribute("id");
        MouseEvents.hover(gadgetRenderBox, driver);

        // Click on the drop-down menu
        driver.findElement(By.cssSelector(".gadget-menu .aui-dd-trigger")).click();

        // Click on Refresh
        gadgetRenderBox.findElement(By.cssSelector("li.dropdown-item.reload")).click();

        // then switch back to gadget frame
        driver.switchTo().frame(frameId);

        return pageBinder.bind(ActivityStreamGadget.class);
    }

    /**
     * Returns the list of sources which have timed out.
     * @return a list of source names, or null if no warning about timeout is displayed
     */
    public List<String> getTimeoutSources()
    {
        String text = warningBox.getText();
        int from = text.indexOf("(");
        int to = text.indexOf(")");
        if (from == -1 || to == -1)
        {
            return null;
        }
        String sourceList = text.substring(from + 1, to);
        return Lists.newArrayList(sourceList.split(", "));
    }

    /**
     * Gets the first activity item matching the specified textual patterns.
     */
    public Option<ActivityItem> getActivityItemWhere(Predicate<ActivityItem> p)
    {
        Iterable<ActivityItem> activityItems = getActivityItemsWhere(p);
        try
        {
            return some(get(activityItems, 0));
        }
        catch (IndexOutOfBoundsException e)
        {
            return none();
        }
    }

    public Iterable<ActivityItem> getActivityItemsWhere(Predicate<ActivityItem> p)
    {
        return filter(getActivityItems(), p);
    }

    public ActivityStreamConfiguration openConfiguration()
    {
        if (!driver.elementIsVisible(By.id("filter-options")))
        {
            filter.click();
        }
        return pageBinder.bind(ActivityStreamConfiguration.class);
    }

    public String getTitle()
    {
        return title.getText();
    }

    public void close()
    {
        driver.switchTo().defaultContent();
    }

    private final Function<WebElement, ActivityItem> toActivityItem = new Function<WebElement, ActivityItem>()
    {
        public ActivityItem apply(WebElement item)
        {
            return pageBinder.bind(ActivityItem.class, item);
        }
    };

    public static class Link
    {
        private final WebElement link;

        public Link(WebElement link)
        {
            this.link = link;
        }

        public String getHref()
        {
            return link.getAttribute("href");
        }
    }
}
