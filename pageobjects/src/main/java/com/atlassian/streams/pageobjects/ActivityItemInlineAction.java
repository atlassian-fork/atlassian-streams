package com.atlassian.streams.pageobjects;

import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.streams.pageobjects.WebElements.exists;

/**
 * Represents a single entry's inline actions in the Activity Stream UI.
 */
public class ActivityItemInlineAction
{
    private static final By LINK = By.tagName("a");
    private static final By LABEL = By.tagName("span");
    public static final By STATUS_BAR = By.className("activity-item-action-status-container").className("aui-message");

    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageBinder pageBinder;

    private final AtomicReference<WebElement> container;
    private final AtomicReference<WebElement> activityItem;

    public ActivityItemInlineAction(WebElement container, WebElement activityItem)
    {
        this.container = new AtomicReference<WebElement>(container);
        this.activityItem = new AtomicReference<WebElement>(activityItem);
    }

    public String getText()
    {
        StringBuilder text = new StringBuilder();
        if (getLink() != null)
        {
            text.append(getLink().getText());
        }
        if (getLabel() != null)
        {
            text.append(getLabel().getText());
        }
        return text.toString();
    }

    public void click()
    {
        click(null);
    }

    public <T> T clickOpen(Class<T> pageComponent)
    {
        click();
        return pageBinder.bind(pageComponent, activityItem.get());
    }

    public void clickAndWaitForStatusBar()
    {
        click(STATUS_BAR);
    }

    private void click(By whatToWaitFor)
    {
        WebElement link = getLink();
        if (link != null)
        {
            link.click();
        }
        else
        {
            throw new IllegalArgumentException("Attempting to click null element");
        }

        if (whatToWaitFor != null)
        {
            driver.waitUntilElementIsVisibleAt(whatToWaitFor, activityItem.get());
        }
    }

    /**
     * Pause until the inline action's status bar appears.
     */
    public void waitForStatusBar()
    {
        driver.waitUntilElementIsVisibleAt(STATUS_BAR, activityItem.get());
    }

    public WebElement getLink()
    {
        if (exists(container.get(), LINK))
        {
            return container.get().findElement(LINK);
        }
        else
        {
            return null;
        }
    }

    public WebElement getLabel()
    {
        if (exists(container.get(), LABEL))
        {
            return container.get().findElement(LABEL);
        }
        else
        {
            return null;
        }
    }
}
