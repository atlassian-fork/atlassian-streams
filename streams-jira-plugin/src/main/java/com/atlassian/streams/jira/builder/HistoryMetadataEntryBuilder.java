package com.atlassian.streams.jira.builder;

import java.net.URI;

import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadata;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.jira.AggregatedJiraActivityItem;
import com.atlassian.streams.jira.JiraActivityItem;
import com.atlassian.streams.jira.renderer.HistoryMetadataRendererFactory;

import com.google.common.base.Function;
import com.google.common.base.Strings;

import org.ofbiz.core.entity.GenericValue;

import static com.google.common.base.Preconditions.checkNotNull;

public class HistoryMetadataEntryBuilder
{
    private final StatusChangeEntryBuilder statusChangeEntryBuilder;
    private final GeneralUpdateEntryBuilder generalUpdateEntryBuilder;
    private final HistoryMetadataRendererFactory historyMetadataRendererFactory;
    private final I18nResolver i18nResolver;

    public HistoryMetadataEntryBuilder(final StatusChangeEntryBuilder statusChangeEntryBuilder,
            final GeneralUpdateEntryBuilder generalUpdateEntryBuilder,
            final HistoryMetadataRendererFactory historyMetadataRendererFactory, final I18nResolver i18nResolver)
    {
        this.statusChangeEntryBuilder = checkNotNull(statusChangeEntryBuilder, "statusChangeEntryBuilder");
        this.generalUpdateEntryBuilder = checkNotNull(generalUpdateEntryBuilder, "generalUpdateEntryBuilder");
        this.historyMetadataRendererFactory = checkNotNull(historyMetadataRendererFactory, "historyMetadataRendererFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public Option<StreamsEntry> buildStatusChangeEntry(final JiraActivityItem item, final URI baseUri, final GenericValue genericValue)
    {
        return statusChangeEntryBuilder.build(item, baseUri, genericValue).map(new DecorateWithMetadata(item.getHistoryMetadata()));
    }

    public Option<StreamsEntry> buildGeneralUpdateEntry(URI baseUri, AggregatedJiraActivityItem aggregatedItem)
    {
        return generalUpdateEntryBuilder.build(baseUri, aggregatedItem).map(new DecorateWithMetadata(aggregatedItem.getActivityItem().getHistoryMetadata()));
    }

    private final class DecorateWithMetadata implements Function<StreamsEntry, StreamsEntry>
    {
        private final Option<HistoryMetadata> maybeMetadata;

        private DecorateWithMetadata(final Option<HistoryMetadata> maybeMetadata)
        {
            this.maybeMetadata = maybeMetadata;
        }

        @Override
        public StreamsEntry apply(final StreamsEntry original)
        {
            if (maybeMetadata.isDefined())
            {
                final HistoryMetadata historyMetadata = maybeMetadata.get();
                final String descriptionKey = historyMetadata.getActivityDescriptionKey();
                if (!Strings.isNullOrEmpty(descriptionKey) && (!i18nResolver.getRawText(descriptionKey).equals(descriptionKey)))
                {
                    return new StreamsEntry(StreamsEntry.params(original)
                            .renderer(historyMetadataRendererFactory.newCustomKeyRenderer(original, historyMetadata)), i18nResolver);

                }
                else if (!Strings.isNullOrEmpty(historyMetadata.getActivityDescription()))
                {
                    return new StreamsEntry(StreamsEntry.params(original)
                            .renderer(historyMetadataRendererFactory.newSuffixRenderer(original, historyMetadata.getActivityDescription())), i18nResolver);

                }
            }
            return original;
        }
    }
}
