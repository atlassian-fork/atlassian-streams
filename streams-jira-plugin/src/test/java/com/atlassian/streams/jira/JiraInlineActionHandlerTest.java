package com.atlassian.streams.jira;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JiraInlineActionHandlerTest
{
    private static final String ISSUE_KEY = "issue-key";

    @Mock
    private WatcherManager watcherManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private VoteManager voteManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ApplicationUser user;
    @Mock
    private MutableIssue issue;
    @InjectMocks
    private JiraInlineActionHandlerImpl actionHandler;

    @Before
    public void setUp()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(issueManager.getIssueObject(ISSUE_KEY)).thenReturn(issue);
    }

    @Test
    public void willWatchIssueSuccessfully()
    {
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)).thenReturn(true);
        when(watcherManager.getCurrentWatcherUsernames(issue))
                .thenReturn(Collections.singletonList("user1"))
                .thenReturn(Arrays.asList("user1", "user2"));

        boolean result = actionHandler.startWatching(ISSUE_KEY);
        assertThat(result, is(true));

        verify(watcherManager, times(2)).getCurrentWatcherUsernames(any());
        verify(watcherManager, times(1)).startWatching(user, issue);
    }

    @Test
    public void willNotWatchIssueWithoutPermission()
    {
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)).thenReturn(false);

        boolean result = actionHandler.startWatching(ISSUE_KEY);
        assertThat(result, is(false));

        verify(watcherManager, never()).getCurrentWatcherUsernames(any());
        verify(watcherManager, never()).startWatching(user, issue);
    }

    @Test
    public void willVerifyWatchingIssue()
    {
        when(watcherManager.isWatching(user, issue)).thenReturn(true);

        boolean result = actionHandler.hasPreviouslyWatched(ISSUE_KEY);
        assertThat(result, is(true));

        verify(watcherManager, times(1)).isWatching(user, issue);
    }

    @Test
    public void willVoteOnIssueSuccessfully()
    {
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)).thenReturn(true);
        when(voteManager.addVote(user, issue)).thenReturn(true);

        boolean result = actionHandler.voteOnIssue(ISSUE_KEY);
        assertThat(result, is(true));

        verify(voteManager, times(1)).addVote(user, issue);
    }

    @Test
    public void willNotVoteOnIssueWithoutPermission()
    {
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)).thenReturn(false);

        boolean result = actionHandler.voteOnIssue(ISSUE_KEY);
        assertThat(result, is(false));

        verify(voteManager, never()).addVote(user, issue);
    }

    @Test
    public void willVerifyIfVotedPreviously()
    {
        when(voteManager.hasVoted(user, issue)).thenReturn(true);

        boolean result = actionHandler.hasPreviouslyVoted(ISSUE_KEY);
        assertThat(result, is(true));

        verify(voteManager, times(1)).hasVoted(user, issue);
    }
}
