package com.atlassian.streams.confluence.renderer;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.confluence.changereport.AttachmentActivityItem;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AttachmentRendererFactoryTest {

    @Mock
    private StreamsEntryRendererFactory rendererFactory;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private Attachment attachment;
    @InjectMocks
    private AttachmentRendererFactory attachmentRendererFactory;

    @Test
    public void testMakeSureUsingAbsolutePath() throws IOException {
        List<AttachmentActivityItem.Entry> entries = new ArrayList<>();
        entries.add(new AttachmentActivityItem.Entry(attachment, Option.none()));

        when(attachment.getDownloadPath()).thenReturn("/download/image.test");
        when(applicationProperties.getBaseUrl(eq(UrlMode.ABSOLUTE))).thenReturn("http://localhost:8080/confluence");
        StreamsEntry.Renderer renderer = attachmentRendererFactory.newInstance(entries);
        ArgumentCaptor<Map<String, Object>> paramArgument = ArgumentCaptor.forClass((Class) Map.class);
        Option<Html> content = renderer.renderContentAsHtml(null);
        verify(templateRenderer).render(anyString(), paramArgument.capture(), any());

        Map templateContext = paramArgument.getValue();
        List<AttachmentRendererFactory.EntryWrapper> entryWrapperList = (List<AttachmentRendererFactory.EntryWrapper>)
                templateContext.get("nonpreviewable");
        assertThat(entryWrapperList, is(not(empty())));
        AttachmentRendererFactory.EntryWrapper wrapper = entryWrapperList.get(0);
        assertThat(wrapper.getAbsoluteDownloadPath(), equalTo("http://localhost:8080/confluence/download/image.test"));
    }
}
