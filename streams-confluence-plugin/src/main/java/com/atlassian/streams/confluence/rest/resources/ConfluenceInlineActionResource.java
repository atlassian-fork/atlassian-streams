package com.atlassian.streams.confluence.rest.resources;

import com.atlassian.streams.confluence.ConfluenceWatchInlineActionHandler;
import com.atlassian.streams.confluence.ConfluenceWatchPageInlineActionHandler;
import com.atlassian.streams.confluence.ConfluenceWatchSpaceInlineActionHandler;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;

/**
 * REST resource for any Confluence-specific inline action tasks.
 */
@Path("/actions")
public class ConfluenceInlineActionResource
{
    private final ConfluenceWatchPageInlineActionHandler pageWatchHandler;
    private final ConfluenceWatchSpaceInlineActionHandler spaceWatchHandler;

    public ConfluenceInlineActionResource(ConfluenceWatchPageInlineActionHandler pageWatchHandler,
                                          ConfluenceWatchSpaceInlineActionHandler spaceWatchHandler)
    {
        this.pageWatchHandler = checkNotNull(pageWatchHandler, "pageWatchHandler");
        this.spaceWatchHandler = checkNotNull(spaceWatchHandler, "spaceWatchHandler");
    }

    @Path("page-watch/{key}")
    @POST
    public Response watchPage(@PathParam("key") Long key)
    {
        return watchEntity(pageWatchHandler, key);
    }

    @Path("space-watch/{key}")
    @POST
    public Response watchSpace(@PathParam("key") String key)
    {
        return watchEntity(spaceWatchHandler, key);
    }
    
    private <K> Response watchEntity(ConfluenceWatchInlineActionHandler<K> handler, K key)
    {
        boolean success = handler.startWatching(key);
        if (success)
        {
            return Response.noContent().build();
        }
        else
        {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }
}
