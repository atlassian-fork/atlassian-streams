package com.atlassian.streams.confluence;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import java.net.URI;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class UriProvider
{
    private static final Iterable<String> contentStyles = ImmutableList.of(
            "master.css", "wiki-content.css", "tables.css", "renderer-macros.css");
    private static final Iterable<String> panelStyles = ImmutableList.of("panels.css");
    private static final Iterable<String> iconStyles = ImmutableList.of("icons.css");
    private final WebResourceManager webResourceManager;

    public UriProvider(WebResourceManager webResourceManager)
    {
        this.webResourceManager = checkNotNull(webResourceManager, "webResourceManager");
    }

    public URI getEntityUri(final URI baseUri, final ContentEntityObject entity)
    {
        //STRM-1633 always link the latest page versions
        if (entity instanceof AbstractPage && !entity.isLatestVersion() && entity.getLatestVersion() instanceof AbstractPage)
        {
            return URI.create(baseUri.toASCIIString() + ((AbstractPage)entity.getLatestVersion()).getUrlPath());
        }
        else
        {
            return URI.create(baseUri.toASCIIString() + entity.getUrlPath());
        }
    }

    public URI getPageDiffUri(final URI baseUri,
                              final ContentEntityObject entity,
                              final int originalVersion,
                              final int newerVersion)
    {
        checkNotNull(entity, "entity");
        String diffUrl = baseUri.toASCIIString() +
                "/pages/diffpagesbyversion.action?pageId=" + entity.getId() +
                "&originalVersion=" + originalVersion +
                "&revisedVersion=" + newerVersion;
        return URI.create(diffUrl).normalize();
    }

    public Iterable<URI> getContentCssUris()
    {
        return transform(contentStyles, getStaticPluginResource("confluence.web.resources:content-styles"));
    }

    public Iterable<URI> getPanelCssUris()
    {
        return transform(panelStyles, getStaticPluginResource("confluence.web.resources:panel-styles"));
    }

    public Iterable<URI> getIconCssUris()
    {
        return transform(iconStyles, getStaticPluginResource("confluence.web.resources:master-styles"));
    }
    
    private Function<String, URI> getStaticPluginResource(final String moduleKey)
    {
        return s -> URI.create(webResourceManager.getStaticPluginResource(moduleKey, s,
                com.atlassian.plugin.webresource.UrlMode.ABSOLUTE)).normalize();
    }
}
