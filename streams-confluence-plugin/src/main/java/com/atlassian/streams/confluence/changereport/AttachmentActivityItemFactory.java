package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.pages.Attachment;

import java.net.URI;

interface AttachmentActivityItemFactory
{

    ActivityItem newActivityItem(URI baseUri, Attachment attachment);

    ActivityItem newActivityItem(URI baseUri, Attachment attachment, AttachmentActivityItem attachmentItem);

}