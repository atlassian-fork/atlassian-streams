package com.atlassian.streams.confluence;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilderFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.log.JdkLogChute;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ConfluenceStreamsServlet extends HttpServlet
{
    private final VelocityEngine velocity;
    private final WebResourceManager webResourceManager;
    private final I18nResolver i18nResolver;
    private final StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory;

    public ConfluenceStreamsServlet(WebResourceManager webResourceManager, I18nResolver i18nResolver,
            StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory)
    {
        this.webResourceManager = webResourceManager;
        this.i18nResolver = i18nResolver;
        this.streamsFeedUriBuilderFactory = streamsFeedUriBuilderFactory;
        velocity = new VelocityEngine();
        velocity.addProperty(Velocity.RUNTIME_LOG_LOGSYSTEM_CLASS, JdkLogChute.class.getName());
        velocity.addProperty(Velocity.RESOURCE_LOADER, "classpath");
        velocity.addProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
    }

    public void init() throws ServletException
    {
        try
        {
            velocity.init();
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        webResourceManager.requireResource("com.atlassian.streams.confluence:streamsWebResources");
        Template template = getTemplate("/templates/confluence-streams.vm");
        Map<String, Object> map = new HashMap<>();
        map.put("baseurl", request.getContextPath());
        map.put("feedUrl", streamsFeedUriBuilderFactory.getStreamsFeedUriBuilder(request.getContextPath()).getUri().toASCIIString());
        map.put("resourceKey", "com.atlassian.streams.confluence");
        map.put("i18n", i18nResolver);
        map.put("title", "Confluence Activity Stream");
        map.put("detailedTitle", true);
        render(template, map, response);
    }

    private Template getTemplate(String templateName) throws ServletException
    {
        try
        {
            return velocity.getTemplate(templateName);
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }
    }

    private void render(Template template, Map<String, Object> map, HttpServletResponse response) throws IOException
    {
        response.setContentType("text/html");
        VelocityContext context = new VelocityContext(map);
        template.merge(context, response.getWriter());
    }
}
