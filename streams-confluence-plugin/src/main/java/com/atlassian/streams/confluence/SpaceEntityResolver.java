package com.atlassian.streams.confluence;

import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityResolver;

import static com.google.common.base.Preconditions.checkNotNull;

public class SpaceEntityResolver implements EntityResolver
{
    private final SpaceManager spaceManager;

    public SpaceEntityResolver(SpaceManager spaceManager)
    {
        this.spaceManager = checkNotNull(spaceManager, "spaceManager");
    }

    public Option<Object> apply(String key)
    {
        return Option.option(spaceManager.getSpace(key));
    }
}
