package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.confluence.renderer.ContentEntityRendererFactory;
import com.atlassian.streams.confluence.renderer.SpaceRendererFactory;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.confluence.changereport.ContentEntityObjects.isBlogPost;
import static com.atlassian.streams.confluence.changereport.ContentEntityObjects.isPage;
import static com.google.common.base.Preconditions.checkNotNull;

final class ContentEntityActivityItemFactoryImpl implements ContentEntityActivityItemFactory
{

    private static final Logger log = LoggerFactory.getLogger(ContentEntityActivityItemFactoryImpl.class);

    private final ActivityObjectFactory activityObjectFactory;
    private final ContentEntityRendererFactory contentEntityRendererFactory;
    private final SpaceRendererFactory spaceRendererFactory;
    private final CanCommentPredicateFactory canCommentPredicateFactory;

    public ContentEntityActivityItemFactoryImpl(ActivityObjectFactory activityObjectFactory,
            ContentEntityRendererFactory contentEntityRendererFactory,
            SpaceRendererFactory spaceRendererFactory,
            CanCommentPredicateFactory canCommentPredicateFactory)
    {
        this.activityObjectFactory = checkNotNull(activityObjectFactory, "activityObjectFactory");
        this.contentEntityRendererFactory = checkNotNull(contentEntityRendererFactory, "contentEntityRendererFactory");
        this.spaceRendererFactory = checkNotNull(spaceRendererFactory, "spaceRendererFactory");
        this.canCommentPredicateFactory = checkNotNull(canCommentPredicateFactory, "canCommentPredicateFactory");
    }

    /* (non-Javadoc)
     * @see com.atlassian.streams.confluence.changereport.ContentEntityActivityItemFactory1#newActivityItem(com.atlassian.confluence.pages.AbstractPage)
     */
    @Override
    public ActivityItem newActivityItem(final URI baseUri,
                                        final AbstractPage page)
    {
        return new ContentEntityActivityItem(page,
                getActivityObjects(baseUri, page),
                some(getTarget(page)),
                contentEntityRendererFactory.newInstance(baseUri, page),
                canCommentPredicateFactory.canCommentOn(page));
    }

    private Iterable<ActivityObject> getActivityObjects(final URI baseUri,
                                                        final AbstractPage page)
    {
        return ImmutableList.of(isBlogPost(page) ?
            activityObjectFactory.newActivityObject(baseUri, (BlogPost) page) :
                activityObjectFactory.newActivityObject(baseUri, (Page) page));
    }

    /* (non-Javadoc)
     * @see com.atlassian.streams.confluence.changereport.ContentEntityActivityItemFactory1#newActivityItem(com.atlassian.confluence.pages.Comment)
     */
    @Override
    public ActivityItem newActivityItem(URI baseUri, Comment comment)
    {
        return new CommentActivityItem(comment,
                ImmutableList.of(activityObjectFactory.newActivityObject(baseUri, comment)),
                getTarget(baseUri, comment),
                contentEntityRendererFactory.newInstance(baseUri, comment),
                canCommentPredicateFactory.canCommentOn(comment));
    }

    private Option<ActivityObject> getTarget(URI baseUri, Comment comment)
    {
        ContentEntityObject owner = comment.getContainer();

        // Make sure the owner is legit, with no missing pieces.
        if (owner == null)
        {
            log.debug("Encountered a comment with no owner: " + comment);
            return none();
        }
        else if (owner.getType() == null)
        {
            log.debug("Encountered a comment with an owner with no type. Comment: " + comment + ", Owner: " + owner);
            return none();
        }

        if (isBlogPost(owner))
        {
            return some(activityObjectFactory.newActivityObject(baseUri, (BlogPost) owner));
        }
        else if (isPage(owner))
        {
            return some(activityObjectFactory.newActivityObject(baseUri, (Page) owner));
        }
        else
        {
            return none();
        }
    }

    /* (non-Javadoc)
     * @see com.atlassian.streams.confluence.changereport.ContentEntityActivityItemFactory1#newActivityItem(com.atlassian.confluence.spaces.SpaceDescription, boolean)
     */
    @Override
    public ActivityItem newActivityItem(SpaceDescription space, boolean isCreationEvent)
    {
        return new SpaceActivityItem(space,
                isCreationEvent,
                ImmutableList.of(activityObjectFactory.newActivityObject(space)),
                none(ActivityObject.class),
                spaceRendererFactory.newInstance(space));
    }

    private ActivityObject getTarget(SpaceContentEntityObject entity)
    {
        Space space = entity.getSpace();
        //STRM-1502 only the most recent revision has a space. all older revisions are officially orphaned.
        if (space == null && entity.getLatestVersion() instanceof SpaceContentEntityObject)
        {
            space = ((SpaceContentEntityObject)entity.getLatestVersion()).getSpace();
        }
        return activityObjectFactory.newActivityObject(new SpaceDescription(space));
    }
}
