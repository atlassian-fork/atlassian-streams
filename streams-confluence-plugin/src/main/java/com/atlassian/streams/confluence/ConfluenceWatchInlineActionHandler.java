package com.atlassian.streams.confluence;

/**
 * Handles Confluence-specific inline action tasks.
 */
public interface ConfluenceWatchInlineActionHandler<K>
{
    /**
     * Registers the current user as a watcher on the specified entity.
     *  
     * @param key the entity to watch
     * @return true if the watcher was added (or was already watching the entity), false if not
     */
    boolean startWatching(K key);
}
