package com.atlassian.streams.confluence;

import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.CaptchaManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Link;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.confluence.changereport.ActivityItem;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.user.User;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;

import java.net.URI;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.getActivityObjectTypes;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.uri.Uris.encode;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.page;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.atlassian.streams.confluence.ConfluenceStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.spi.ServletPath.COMMENTS;
import static com.atlassian.streams.spi.StreamsActivityProvider.CSS_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.REPLY_TO_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.WATCH_LINK_REL;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.contains;
import static com.google.common.collect.Iterables.transform;

public class ConfluenceEntryFactoryImpl implements ConfluenceEntryFactory
{
    public static final String CONFLUENCE_APPLICATION_TYPE = "com.atlassian.confluence";
    public static final String PAGE_WATCH_ACTION = "page-watch";
    public static final String SPACE_WATCH_ACTION = "space-watch";

    private final ApplicationProperties applicationProperties;
    private final NotificationManager notificationManager;
    private final PageManager pageManager;
    private final SpaceManager spaceManager;
    private final UserProfileAccessor userProfileAccessor;
    private final CaptchaManager captchaManager;
    private final UserManager userManager;
    private final UriProvider uriProvider;
    private final StreamsI18nResolver i18nResolver;

    public ConfluenceEntryFactoryImpl(ApplicationProperties applicationProperties,
           NotificationManager notificationManager,
           PageManager pageManager,
           SpaceManager spaceManager,
           UserProfileAccessor userProfileAccessor,
           CaptchaManager captchaManager,
           UserManager salUserManager,
           UriProvider uriProvider,
           StreamsI18nResolver i18nResolver)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.notificationManager = checkNotNull(notificationManager, "notificationManager");
        this.pageManager = checkNotNull(pageManager, "pageManager");
        this.spaceManager = checkNotNull(spaceManager, "spaceManager");
        this.userProfileAccessor = checkNotNull(userProfileAccessor, "userProfileAccessor");
        this.captchaManager = checkNotNull(captchaManager, "captchaManager");
        this.userManager = checkNotNull(salUserManager, "salUserManager");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public StreamsEntry buildStreamsEntry(final URI baseUri, final ActivityItem activityItem)
    {
        URI url = URI.create(baseUri.toASCIIString() + activityItem.getUrlPath());

        return new StreamsEntry(StreamsEntry.params()
                .id(url)
                .postedDate(new DateTime(activityItem.getModified()))
                .applicationType(CONFLUENCE_APPLICATION_TYPE)
                .alternateLinkUri(url)
                .inReplyTo(buildReplyTo(activityItem))
                .addLink(buildReplyTo(activityItem), REPLY_TO_LINK_REL, none(String.class))
                .addLink(buildIconUrl(activityItem), ICON_LINK_REL, some(i18nResolver.getText("streams.item.confluence.tooltip." + activityItem.getContentType())))
                .addLinks(getWatchLink(activityItem))
                .addLinks(transform(uriProvider.getContentCssUris(), toLink(CSS_LINK_REL)))
                .addLinks(transform(uriProvider.getPanelCssUris(), toLink(CSS_LINK_REL)))
                .addLinks(transform(uriProvider.getIconCssUris(), toLink(CSS_LINK_REL)))
                .categories(buildCategory(activityItem))
                .addActivityObjects(activityItem.getActivityObjects())
                .verb(activityItem.getVerb())
                .target(activityItem.getTarget())
                .renderer(activityItem.getRenderer())
                .baseUri(baseUri)
                .authors(ImmutableNonEmptyList.of(userProfileAccessor.getUserProfile(baseUri, activityItem.getChangedBy()))), i18nResolver);
    }

    private Function<URI, Link> toLink(final String rel)
    {
        return uri -> new Link(uri, rel, none(String.class));
    }

    protected Option<URI> buildReplyTo(final ActivityItem item)
    {
        final UserProfile remoteUser = userManager.getRemoteUser();
        if (!captchaManager.showCaptchaForCurrentUser() && item.isAcceptingCommentsFromUser(remoteUser != null ? remoteUser.getUsername() : null))
        {
            return some(URI.create(getBaseUrl() + COMMENTS.getPath() + '/' +
                    encode(PROVIDER_KEY) + '/' +
                    encode(item.getContentType()) + '/' +
                    item.getId())
                .normalize());
        }
        else
        {
            return none();
        }
    }

    private Iterable<String> buildCategory(final ActivityItem activityItem)
    {
        final String type = activityItem.getType();
        final int separatorPos = type.indexOf('.');
        return separatorPos >= 0 ? ImmutableList.of(type.substring(0, separatorPos)) : ImmutableList.of(type);
    }

    private URI buildIconUrl(final ActivityItem activityItem)
    {
        return URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + activityItem.getIconPath());
    }

    private String getBaseUrl()
    {
        return applicationProperties.getBaseUrl(UrlMode.CANONICAL);
    }

    protected Option<Link> getWatchLink(ActivityItem activityItem)
    {
        final User user = AuthenticatedUserThreadLocal.get();
        if (user == null)
        {
            return none();
        }

        final Iterable<ActivityObjectType> activityItemTypes = getActivityObjectTypes(activityItem.getActivityObjects());
        if (contains(activityItemTypes, page()) || contains(activityItemTypes, article()))
        {
            return getPageWatchLink(user, activityItem);
        }
        if (contains(activityItemTypes, space()) || contains(activityItemTypes, personalSpace()))
        {
            return getSpaceWatchLink(user, activityItem);
        }

        return none();
    }

    private Option<Link> getPageWatchLink(User user, ActivityItem activityItem)
    {
        final Long pageId = activityItem.getId();
        final AbstractPage page = pageManager.getAbstractPage(pageId);
        if (page == null)
        {
            return none();
        }
        if (notificationManager.isUserWatchingPageOrSpace(user, page.getSpace(), page))
        {
            return none();
        }

        return some(new Link(buildWatchUrl(String.valueOf(pageId), PAGE_WATCH_ACTION), WATCH_LINK_REL, none(String.class)));
    }

    private Option<Link> getSpaceWatchLink(User user, ActivityItem activityItem)
    {
        final String spaceKey = activityItem.getSpaceKey().get();
        final Space space = spaceManager.getSpace(spaceKey);
        if (space == null)
        {
            return none();
        }
        if (notificationManager.isUserWatchingPageOrSpace(user, space, null))
        {
            return none();
        }

        return some(new Link(buildWatchUrl(spaceKey, SPACE_WATCH_ACTION), WATCH_LINK_REL, none(String.class)));
    }

    private URI buildWatchUrl(String key, String action)
    {
        return URI.create(String.format("%s/rest/confluence-activity-stream/1.0/actions/%s/%s",
                                        getBaseUrl(), encode(action), encode(key))).normalize();
    }
}
