package com.atlassian.streams.thirdparty.api;

import java.net.URI;

import com.atlassian.streams.api.common.Either;
import org.hamcrest.Matchers;
import org.junit.Test;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.api.Application.application;
import static com.atlassian.streams.thirdparty.api.TestData.assertNotValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.assertValidationError;
import static com.atlassian.streams.thirdparty.api.TestData.makeAbsoluteUri;
import static com.atlassian.streams.thirdparty.api.TestData.makeString;
import static com.atlassian.streams.thirdparty.api.TestData.makeUri;
import static com.atlassian.streams.thirdparty.api.ValidationErrors.MAX_STRING_LENGTH;
import static org.hamcrest.MatcherAssert.assertThat;

public class ApplicationTest
{
    @Test(expected=NullPointerException.class)
    public void displayNameNullInSimpleFactoryMethodThrowsException()
    {
        application(null, URI.create("http://id"));
    }

    @Test
    public void displayNameOmittedReturnsValidationError()
    {
        assertValidationError(application(none(String.class), some("http://id")));
    }

    @Test(expected=IllegalArgumentException.class)
    public void displayNameBlankInSimpleFactoryMethodThrowsException()
    {
        Application.application("  \t\n ", URI.create("http://id"));
    }

    @Test
    public void displayNameBlankReturnsValidationError()
    {
        assertValidationError(application(some("  \t\n "), some("http://id")));
    }

    @Test
    public void displayNameOfMaxLengthInSimpleFactoryMethodDoesNotThrowException()
    {
        application(makeString(MAX_STRING_LENGTH), URI.create("http://id"));
    }

    @Test
    public void displayNameOfMaxLengthDoesNotReturnValidationError()
    {
        assertNotValidationError(application(some(makeString(MAX_STRING_LENGTH)), some("http://id")));
    }

    @Test(expected=IllegalArgumentException.class)
    public void displayNameOverMaxLengthInSimpleFactoryMethodThrowsException()
    {
        application(makeString(MAX_STRING_LENGTH + 1), URI.create("http://id"));
    }

    @Test
    public void displayNameOverMaxLengthReturnsValidationError()
    {
        assertValidationError(application(some(makeString(MAX_STRING_LENGTH + 1)), some("http://id")));
    }

    @Test(expected=NullPointerException.class)
    public void idNullInSimpleFactoryMethodThrowsException()
    {
        application("name", (URI)null);
    }

    @Test
    public void idWithMaliciousSchemeReturnsError()
    {
        final Either<ValidationErrors, Application> result =
                application(some("name"), some("javascript:alert('you_have_been_hacked')"));
        assertValidationError(result);
        // Check the error message to ensure the test is not passing for spurious reasons
        assertThat(
                result.left().get().getMessages(),
                Matchers.hasItem(Matchers.containsString("must start with a valid scheme")));
    }

    @Test
    public void idOmittedReturnsValidationError()
    {
        application(some("name"), none(String.class));
    }
    
    @Test
    public void idOfMaxLengthDoesNotThrowException()
    {
        application("name", makeAbsoluteUri(MAX_STRING_LENGTH));
    }

    @Test
    public void idOfMaxLengthDoesNotReturnValidationError()
    {
        assertNotValidationError(application(some("name"), some(makeAbsoluteUri(MAX_STRING_LENGTH).toASCIIString())));
    }

    @Test(expected=IllegalArgumentException.class)
    public void idOverMaxLengthThrowsException()
    {
        application("name", makeAbsoluteUri(MAX_STRING_LENGTH + 1));
    }

    @Test
    public void idOverMaxLengthReturnsValidationError()
    {
        assertValidationError(application(some("name"), some(makeAbsoluteUri(MAX_STRING_LENGTH + 1).toASCIIString())));
    }
    
    @Test
    public void idNotAbsoluteReturnsValidationError()
    {
        assertValidationError(application(some("name"), some(makeUri(MAX_STRING_LENGTH).toASCIIString())));
    }
}
