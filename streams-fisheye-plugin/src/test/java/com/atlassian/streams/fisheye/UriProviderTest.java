package com.atlassian.streams.fisheye;

import java.net.URI;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.data.FileRevisionData;

import com.cenqua.fisheye.rep.RepositoryHandle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class UriProviderTest
{
    private UriProvider uriProvider;
    @Mock
    private FileRevisionData revision;
    @Mock
    private RepositoryHandle repository;
    @Mock
    private ChangesetDataFE changeSetData;
    private final URI baseUri = URI.create("http://localhost/streams");

    @Before
    public void createUriProvider()
    {
        uriProvider = new UriProvider();
    }

    @Before
    public void stubMocks()
    {
        when(revision.getPath()).thenReturn("trunk/some/path");
        when(revision.getCsid()).thenReturn("98456");
        when(repository.getName()).thenReturn("some-repo");
        when(changeSetData.getCsid()).thenReturn("1563");
    }

    @Test
    public void shouldEncodeChangelogUriPath()
    {
        when(revision.getPath()).thenReturn("trunk/Some : crazily named % file ?");

        final URI actualUri = uriProvider.getChangelogUri(baseUri, revision, repository);

        assertUriAsciiString(actualUri, "http://localhost/streams/browse/some-repo/trunk/Some%20:%20crazily%20named%20%25%20file%20%3F?r=98456");
    }

    private void assertUriAsciiString(URI uri, String expectedAsciiString)
    {
        final String actualAsciiString = uri.toASCIIString();
        assertThat(actualAsciiString, equalTo(expectedAsciiString));
    }

    @Test
    public void shouldGetChangeSetUri()
    {
        final URI changeSetUri = uriProvider.getChangeSetUri(baseUri, changeSetData, repository);

        assertUriAsciiString(changeSetUri, "http://localhost/streams/changelog/some-repo?cs=1563");
    }

    @Test
    public void shouldGetRepositoryUri()
    {
        final URI repositoryUri = uriProvider.getRepositoryUri(baseUri, repository);

        assertUriAsciiString(repositoryUri, "http://localhost/streams/changelog/some-repo");
    }

    @Test
    public void shouldGetChangeSetReviewUri()
    {
        final URI changeSetReviewUri = uriProvider.getChangesetReviewUri(baseUri, "69843", "PRJ");

        assertUriAsciiString(changeSetReviewUri, "http://localhost/streams/cru/create?csid=69843&repo=PRJ");
    }

    @Test
    public void shouldGetCommitIconUri()
    {
        final URI commitIconUri = uriProvider.getCommitIconUri(baseUri);

        assertUriAsciiString(commitIconUri, "http://localhost/streams" + UriProvider.FISHEYE_COMMIT_ICON_PATH);
    }


}
