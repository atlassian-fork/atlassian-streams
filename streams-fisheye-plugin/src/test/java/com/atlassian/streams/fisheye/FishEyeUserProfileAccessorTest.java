package com.atlassian.streams.fisheye;

import java.net.URI;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.testing.AbstractUserProfileAccessorTestSuite;

import com.cenqua.fisheye.rep.DbException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.api.common.Option.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FishEyeUserProfileAccessorTest extends AbstractUserProfileAccessorTestSuite
{
    private static final URI BASE_URI = URI.create("http://localhost/streams");

    @Mock
    private UserManager userManager;
    @Mock
    private StreamsI18nResolver i18nResolver;

    @Before
    public void createUserProfileBuilder()
    {
        userProfileAccessor = new FishEyeUserProfileAccessor(getApplicationProperties(), userManager, i18nResolver);
    }

    @Before
    public void prepareUserManager() throws DbException
    {
        UserProfile user = mock(UserProfile.class);
        when(user.getFullName()).thenReturn("User");
        when(user.getEmail()).thenReturn("u@c.com");
        when(user.getProfilePageUri()).thenReturn(URI.create("/user/user"));
        when(user.getProfilePictureUri()).thenReturn(URI.create("http://localhost/streams/avatar/user"));
        when(userManager.getUserProfile("user")).thenReturn(user);

        UserProfile userWithSpaceInUsername = mock(UserProfile.class);
        when(userWithSpaceInUsername.getFullName()).thenReturn("User 2");
        when(userWithSpaceInUsername.getEmail()).thenReturn("u2@c.com");
        when(userWithSpaceInUsername.getProfilePageUri()).thenReturn(URI.create("/user/user+2"));
        when(userWithSpaceInUsername.getProfilePictureUri()).thenReturn(URI.create("http://localhost/streams/avatar/user+2"));
        when(userManager.getUserProfile("user 2")).thenReturn(userWithSpaceInUsername);

        UserProfile userWithFunnyCharactersInName = mock(UserProfile.class);
        when(userWithFunnyCharactersInName.getFullName()).thenReturn("User <3&'>");
        when(userWithFunnyCharactersInName.getEmail()).thenReturn("u3@c.com");
        when(userWithFunnyCharactersInName.getProfilePageUri()).thenReturn(URI.create("/user/user3"));
        when(userWithFunnyCharactersInName.getProfilePictureUri()).thenReturn(URI.create("http://localhost/streams/avatar/user3"));
        when(userManager.getUserProfile("user3")).thenReturn(userWithFunnyCharactersInName);

        UserProfile userWithRelativeAvatarUri = mock(UserProfile.class);
        when(userWithRelativeAvatarUri.getFullName()).thenReturn("User4");
        when(userWithRelativeAvatarUri.getEmail()).thenReturn("u4@c.com");
        when(userWithRelativeAvatarUri.getProfilePageUri()).thenReturn(URI.create("/user/user4"));
        when(userWithRelativeAvatarUri.getProfilePictureUri()).thenReturn(URI.create("/avatar/user4"));
        when(userManager.getUserProfile("user4")).thenReturn(userWithRelativeAvatarUri);
    }

    @Override
    protected String getProfilePathTemplate()
    {
        return "/user/{username}";
    }

    @Override
    protected String getProfilePicturePathTemplate()
    {
        return "http://localhost/streams/avatar/{profilePictureParameter}";
    }

    /**
     * Return an empty string, because the application base url is already included in the profile picture URI.
     */
    @Override
    protected String getProfilePicApplicationBaseUrl()
    {
        return "";
    }

    @Test
    public void assertThatProfilePictureUriIsAbsolute()
    {
        when(getApplicationProperties().getBaseUrl()).thenReturn(BASE_URI.toString());
        assertThat(userProfileAccessor.getUserProfile(BASE_URI, "user4").getProfilePictureUri(),
                is(some(URI.create("http://localhost/streams/avatar/user4"))));
    }

}
