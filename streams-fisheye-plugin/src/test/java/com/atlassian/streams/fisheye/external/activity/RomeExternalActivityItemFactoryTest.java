package com.atlassian.streams.fisheye.external.activity;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import com.atlassian.fisheye.activity.ExternalActivityItem;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
public class RomeExternalActivityItemFactoryTest
{
    private static final String JIRA_TITLE = "commented on <a href=\"http://localhost:4990/streams/browse/ONE-1\">ONE-1</a> (Issue One)";
    private static final String JIRA_DESCRIPTION = "<div class=\"activity-item-description\">This is a comment.</div>";
    private static final String BAMBOO_TITLE = "Project ONE - one 1.0 build <a href=\"http://localhost:6990/streams/browse/ONE-ONE10-2\">2</a>  finished successfully";
    private static final String BAMBOO_DESCRIPTION = "<div class=\"activity-item-description\">Build updated by " +
    		"<a href=\"http://localhost:6990/streams/browse/user/admin\">Admin</a>.<br>1 passed<br>Build took 11 seconds.</div>";
    private static final String ENTRY_ID = "ONE-1";
    private static final String ENTRY_NAME = "Issue One";
    private static final String ENTRY_URL = "http://localhost:4990/streams/browse/ONE-1";
    private static final String USERNAME = "admin";
    private static final String FULLNAME = "A. D. Ministrator";

    private static String feedContents;

    private ExternalActivityItemFactory itemFactory;

    @BeforeClass
    public static void readStream() throws IOException
    {
        feedContents = getContents("/feed.xml");
    }

    @Before
    public void setup()
    {
        itemFactory = new RomeExternalActivityItemFactory();
    }

    @Test
    public void verifyCorrectNumberOfEntries()
    {
        assertThat(size(getItems()), is(equalTo(5)));
    }

    @Test
    public void verifyUsernameIsCorrect()
    {
        ExternalActivityItem item = get(getItems(), 0);
        assertThat(item.getUsername(), is(equalTo(USERNAME)));
    }

    @Test
    public void verifyUsernameGetsCorrectAuthorFullName()
    {
        ExternalActivityItem item = get(getItems(), 0);
        assertThat(item.getAuthor(), is(equalTo(FULLNAME)));
    }

    @Test
    public void verifyTitleIsCorrectWhenUsernameIsHyperlinked()
    {
        ExternalActivityItem item = get(getItems(), 0);
        assertThat(item.getTitle(), is(equalTo(JIRA_TITLE)));
    }

    @Test
    public void verifyTitleIsCorrectWhenUsernameIsNotHyperlinked()
    {
        ExternalActivityItem item = get(getItems(), 1);
        assertThat(item.getTitle(), is(equalTo(JIRA_TITLE)));
    }

    @Test
    public void verifyTitleIsCorrectWhenUsernameIsNotPresent()
    {
        ExternalActivityItem item = get(getItems(), 2);
        assertThat(item.getTitle(), is(equalTo(BAMBOO_TITLE)));
    }

    @Test
    public void verifySummaryIsEqualToFeedContent()
    {
        ExternalActivityItem item = get(getItems(), 1);
        assertThat(item.getSummary(), is(equalTo(FULLNAME + " " + JIRA_TITLE + " " + JIRA_DESCRIPTION)));
    }

    @Test
    public void verifyUrlIsCorrect()
    {
        ExternalActivityItem item = get(getItems(), 0);
        assertThat(item.getUrl(), is(equalTo(ENTRY_URL)));
    }

    private static String getContents(String fileName) throws IOException
    {
        try (InputStream input = RomeExternalActivityItemFactoryTest.class.getResourceAsStream(fileName))
        {
            if (input == null)
            {
                throw new RuntimeException("Cannot find file: " + fileName);
            }
            return IOUtils.toString(input, StandardCharsets.UTF_8);
        }
    }

    private Iterable<ExternalActivityItem> getItems()
    {
        return itemFactory.getItems(feedContents);
    }
}
