package com.atlassian.streams.fisheye.external.activity;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.crucible.spi.data.ProjectData;
import com.atlassian.crucible.spi.services.ProjectService;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;
import com.atlassian.fisheye.spi.data.RepositoryDataFE;
import com.atlassian.fisheye.spi.services.RepositoryService;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilder;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilderFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class RemoteStreamsFeedUriBuilderImpl implements RemoteStreamsFeedUriBuilder
{
    private static final String CRU_PROJECT_PREFIX = "CR-";

    private final StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory;
    private final ProjectService projectService;
    private final RepositoryService repositoryService;
    private final EntityLinkService entityLinkService;

    public RemoteStreamsFeedUriBuilderImpl(StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory,
                                           ProjectService projectService,
                                           RepositoryService repositoryService,
                                           EntityLinkService entityLinkService)
    {
        this.streamsFeedUriBuilderFactory = checkNotNull(streamsFeedUriBuilderFactory, "streamsFeedUriBuilderFactory");
        this.projectService = checkNotNull(projectService, "projectService");
        this.repositoryService = checkNotNull(repositoryService, "repositoryService");
        this.entityLinkService = checkNotNull(entityLinkService, "entityLinkService");
    }

    public URI buildUri(ApplicationLink appLink, ExternalActivityItemSearchParams params)
    {
        String baseUrl = appLink.getRpcUrl().toASCIIString();
        StreamsFeedUriBuilder uriBuilder = streamsFeedUriBuilderFactory.getStreamsFeedUriBuilder(baseUrl).setMaxResults(params.getMaxItems()).addLocalOnly(true);
        if (params.getMaxDate() != null)
        {
            uriBuilder.setLegacyMaxDate(params.getMaxDate().getTime());
        }
        if (params.getMinDate() != null)
        {
            uriBuilder.setLegacyMinDate(params.getMinDate().getTime());
        }
        if (isNotEmpty(params.getUserFilter()))
        {
            uriBuilder.addLegacyFilterUser(params.getUserFilter());
        }
        if (params.getProjectId() != null)
        {
            ProjectData project = projectService.getProject(params.getProjectId());
            boolean specifiedKey = false;

            for (String key : getJiraEntityLinkKeys(appLink, project))
            {
                uriBuilder.addLegacyKey(key);
                specifiedKey = true;
            }
            if (!specifiedKey)
            {
                // Add a key filter for the project name so we don't necessarily request all issue activity.
                String projectKey = project.getKey();
                uriBuilder.addLegacyKey(projectKey.startsWith(CRU_PROJECT_PREFIX) ?
                    projectKey.substring(CRU_PROJECT_PREFIX.length()) :
                    projectKey);
            }
        }
        if (params.getRepFilter() != null)
        {
            RepositoryDataFE repository = repositoryService.getRepositoryInfo(params.getRepFilter());
            boolean specifiedKey = false;
            for (String key : getJiraEntityLinkKeys(appLink, repository))
            {
                uriBuilder.addLegacyKey(key);
                specifiedKey = true;
            }
            if (!specifiedKey)
            {
                // Add a key filter for the repository name so we don't necessarily request all issue activity.
                uriBuilder.addLegacyKey(params.getRepFilter());
            }
        }

        if (params.getProjectId() == null && params.getRepFilter() == null)
        {
            // For the Dashboard view, filter on all known JIRA entity links.
            // If no JIRA entity links are defined, the URI will request all issue activity.
            List<Object> entities = new LinkedList<>(projectService.getAllProjects());
            entities.addAll(repositoryService.listRepositories());
            for (Object entity : entities)
            {
                for (String key : getJiraEntityLinkKeys(appLink, entity))
                {
                    uriBuilder.addLegacyKey(key);
                }
            }
        }

        //must use full servlet uri as this plugin supports both streams 3.0 and 4.0
        return uriBuilder.getServletUri();
    }

    private Iterable<String> getJiraEntityLinkKeys(ApplicationLink appLink, Object entity)
    {
        List<String> keys = new LinkedList<>();
        Iterable<EntityLink> entityLinks = entityLinkService.getEntityLinks(entity, JiraProjectEntityType.class);
        for (EntityLink link : entityLinks)
        {
            if (link.getApplicationLink().equals(appLink))
            {
                keys.add(link.getKey());
            }
        }
        return keys;
    }
}
