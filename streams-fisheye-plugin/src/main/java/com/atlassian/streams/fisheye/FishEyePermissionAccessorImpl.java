package com.atlassian.streams.fisheye;

import com.atlassian.fisheye.spi.TxTemplate;

import com.cenqua.crucible.model.Principal;
import com.cenqua.crucible.model.managers.UserActionManager;
import com.cenqua.fisheye.AppConfig;
import com.cenqua.fisheye.rep.RepositoryHandle;
import com.cenqua.fisheye.user.UserManager;

import static com.cenqua.crucible.model.Principal.Anonymous.isAnon;
import static com.cenqua.crucible.tags.ReviewUtil.principalCanDoAction;
import static com.google.common.base.Preconditions.checkNotNull;

public class FishEyePermissionAccessorImpl implements FishEyePermissionAccessor
{
    private final TxTemplate txTemplate;
    private final UserManager userManager;

    public FishEyePermissionAccessorImpl(TxTemplate txTemplate, UserManager userManager)
    {
        this.txTemplate = checkNotNull(txTemplate, "txTemplate");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public boolean isCreateReviewAllowed()
    {
        return principalCanDoAction(getCurrentUser(), UserActionManager.ACTION_CREATE);
    }

    public boolean currentUserCanView(RepositoryHandle repository)
    {
        Principal user = getCurrentUser();
        return userManager.hasPermissionToAccess(user, repository) ||
               (isAnon(user) && AppConfig.getsConfig().isCruAnonAccessAllowed() && repository.getCfg().isAnonAccessAllowed());
    }

    /**
     * Returns the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     * @return the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     */
    private Principal getCurrentUser()
    {
        return txTemplate.getEffectivePrincipal();
    }
}
