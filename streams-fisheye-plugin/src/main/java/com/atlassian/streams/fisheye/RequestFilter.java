package com.atlassian.streams.fisheye;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class RequestFilter implements Filter
{
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
            throws IOException, ServletException
    {
        FisheyeUriAuthenticationParameterProvider.request.set((HttpServletRequest) request);
        try
        {
            chain.doFilter(request, response);
        }
        finally
        {
            FisheyeUriAuthenticationParameterProvider.request.set(null);
        }
    }

    public void init(FilterConfig filterConfig) throws ServletException {}
    public void destroy() {}
}
