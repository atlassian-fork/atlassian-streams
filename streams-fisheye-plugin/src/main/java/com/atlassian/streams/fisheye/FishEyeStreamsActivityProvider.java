package com.atlassian.streams.fisheye;

import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.common.Function2;
import com.atlassian.streams.api.common.Suppliers;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.StreamsActivityProvider;

import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

import static com.atlassian.streams.api.StreamsEntry.byPostedDate;
import static com.atlassian.streams.api.common.Fold.foldl;
import static com.atlassian.streams.api.common.Iterables.mergeSorted;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.changeset;
import static com.atlassian.streams.fisheye.FishEyeActivityVerbs.push;
import static com.atlassian.streams.spi.Filters.inActivities;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;

public class FishEyeStreamsActivityProvider implements StreamsActivityProvider
{
    public static final String PROVIDER_KEY = "source";
    public static final String CHANGESET_REVIEW_REL = "http://streams.atlassian.com/syndication/changeset-review";

    private final I18nResolver i18nResolver;
    private final FishEyeFinder finder;

    public FishEyeStreamsActivityProvider(final I18nResolver i18nResolver,
            final FishEyeFinder finder)
    {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.finder = checkNotNull(finder, "finder");
    }

    /**
     * Get the activity feed for the given request
     *
     * @param request The request
     * @return The ATOM feed
     */
    @Override
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest request) throws StreamsException
    {
        return new CancellableTask<StreamsFeed>()
        {
            final AtomicBoolean cancelled = new AtomicBoolean(false);

            @Override
            public StreamsFeed call() throws Exception
            {
                // COMMIT (aka "push") is the only FishEye verb currently supported
                if (!inActivities(request).apply(pair(changeset(), push())))
                {
                    return buildFeed(ImmutableList.<StreamsEntry>of());
                }

                return buildFeed(take(request.getMaxResults(),
                                      mergeSorted(getChangesets(request, Suppliers.forAtomicBoolean(cancelled)),
                                                  byPostedDate())));
            }

            @Override
            public Result cancel()
            {
                cancelled.set(true);
                return Result.CANCELLED;
            }
        };
    }

    private StreamsFeed buildFeed(final Iterable<StreamsEntry> entries)
    {
        return new StreamsFeed(i18nResolver.getText("streams.feed.fisheye.title"), entries, none(String.class));
    }

    private Iterable<Iterable<StreamsEntry>> getChangesets(final ActivityRequest request, Supplier<Boolean> cancelled)
    {
        return foldl(finder.getRepositories(request), ImmutableList.<Iterable<StreamsEntry>>of(), appendChangesets(request, cancelled));
    }

    private Function2<RepositoryHandle, Iterable<Iterable<StreamsEntry>>, Iterable<Iterable<StreamsEntry>>>
        appendChangesets(final ActivityRequest request, final Supplier<Boolean> cancelled)
    {
        final URI baseUri = request.getContextUri();
        return new Function2<RepositoryHandle, Iterable<Iterable<StreamsEntry>>, Iterable<Iterable<StreamsEntry>>>()
        {
            @Override
            public Iterable<Iterable<StreamsEntry>> apply(RepositoryHandle repository,
                    Iterable<Iterable<StreamsEntry>> acc)
            {
                if (cancelled.get())
                {
                    throw new CancelledException();
                }
                Iterable<StreamsEntry> changesets = finder.getChangesets(baseUri, repository, request, cancelled);
                return concat(acc, ImmutableList.of(changesets));
            }
        };
    }
}
