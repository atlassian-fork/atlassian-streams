package com.atlassian.streams.internal.feed;

/**
 * Marker interface for data that can be attached to a feed rather than to any
 * individual entry.
 */
public interface FeedHeader
{
}
