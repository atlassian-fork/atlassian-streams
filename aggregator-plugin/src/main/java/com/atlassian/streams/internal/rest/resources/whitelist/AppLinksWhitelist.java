package com.atlassian.streams.internal.rest.resources.whitelist;

import java.net.URI;

import com.atlassian.sal.api.ApplicationProperties;

import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.concat;

/**
 * Whitelist implementation which uses applinks as the whitelist source.
 */
public class AppLinksWhitelist implements Whitelist
{
    private final ApplicationProperties applicationProperties;
    private final Supplier<Iterable<URI>> whitelist;

    public AppLinksWhitelist(ApplicationProperties applicationProperties, AppLinksUriSupplier whitelist)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.whitelist = checkNotNull(whitelist, "whitelist");
    }

    public boolean allows(URI uri)
    {
        //allow any URI that is either applinked or the current server, UNLESS it is the url proxy resource
        return any(concat(whitelist.get(), ImmutableList.of(self())), prefixes(checkNotNull(uri, "uri"))) && notProxyResource().apply(uri);
    }

    private Predicate<URI> prefixes(URI uri)
    {
        return new UriPrefixPredicate(uri);
    }

    private static final class UriPrefixPredicate implements Predicate<URI>
    {
        private final String uri;

        public UriPrefixPredicate(URI uri)
        {
            this.uri = uri.normalize().toASCIIString().toLowerCase();
        }

        public boolean apply(URI prefix)
        {
            return uri.startsWith(prefix.normalize().toASCIIString().toLowerCase());
        }
    }

    private Predicate<URI> notProxyResource()
    {
        return new NotProxyResourcePredicate();
    }

    private final class NotProxyResourcePredicate implements Predicate<URI>
    {
        public boolean apply(URI uri)
        {
            return !uri.normalize().toASCIIString().toLowerCase().contains("url-proxy");
        }
    }

    private URI self()
    {
        return URI.create(applicationProperties.getBaseUrl());
    }
}
