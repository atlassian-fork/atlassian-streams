package com.atlassian.streams.internal.feed;


import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

public class ActivitySourceBannedFeedHeader implements FeedHeader
{
    private final String sourceName;

    public ActivitySourceBannedFeedHeader(@Nonnull String sourceName)
    {
        this.sourceName = checkNotNull(sourceName);
    }

    @Nonnull public String getSourceName()
    {
        return sourceName;
    }
}
