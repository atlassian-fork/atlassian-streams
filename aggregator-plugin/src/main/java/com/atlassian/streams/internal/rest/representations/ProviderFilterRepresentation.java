package com.atlassian.streams.internal.rest.representations;

import java.util.Collection;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;
import com.atlassian.streams.spi.StreamsFilterOptionProvider.ActivityOption;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.streams.api.StreamsFilterType.SELECT;
import static com.atlassian.streams.internal.rest.representations.FilterOptionRepresentation.toFilterOptionEntry;
import static com.atlassian.streams.spi.ActivityOptions.toActivityOptionKey;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_KEY;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.transformValues;
import static com.google.common.collect.Maps.uniqueIndex;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class ProviderFilterRepresentation
{
    private final static String KEY = "key";

    @JsonProperty final String key;
    @JsonProperty final String name;
    @JsonProperty final String applinkName;
    @JsonProperty final Collection<FilterOptionRepresentation> options;
    @JsonProperty final String providerAliasOptionKey;

    @JsonCreator
    public ProviderFilterRepresentation(
        @JsonProperty("key") String key,
        @JsonProperty("name") String name,
        @JsonProperty("applinkName") String applinkName,
        @JsonProperty("options") Collection<FilterOptionRepresentation> options,
        @JsonProperty("providerAliasOptionKey") String providerAliasOptionKey)
    {
        this.key = key;
        this.name = name;
        this.applinkName = applinkName;
        this.options = optionAlphaSorter.sortedCopy(options);
        this.providerAliasOptionKey = providerAliasOptionKey;
    }
    
    public ProviderFilterRepresentation(String key,
                                        String name,
                                        String applinkName,
                                        StreamsFilterOptionProvider provider,
                                        I18nResolver i18nResolver)
    {
        this.key = key;
        this.name = name;
        this.applinkName = applinkName;
        if (provider.getFilterOptions() == null)
        {
            this.options = null;
            this.providerAliasOptionKey = null;
        }
        else
        {
            Iterable<StreamsFilterOption> options = provider.getFilterOptions();
            this.options = optionAlphaSorter.sortedCopy(concat(
                transform(options, toFilterOptionEntry(i18nResolver)),
                ImmutableList.of(activityOptionEntry(provider, i18nResolver))));
            this.providerAliasOptionKey = findProviderAliasOptionKey(options);
        }
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public String getApplinkName()
    {
        return applinkName;
    }

    public Collection<FilterOptionRepresentation> getOptions()
    {
        return options;
    }

    public String getProviderAliasOptionKey()
    {
        return providerAliasOptionKey;
    }
    
    @Override
    public String toString()
    {
        if (isEmpty(applinkName))
        {
            return name;
        }
        else
        {
            return name + "@" + applinkName;
        }
    }

    private String findProviderAliasOptionKey(Iterable<StreamsFilterOption> options)
    {
        for (StreamsFilterOption option: options)
        {
            if (option.isProviderAlias())
            {
                return option.getKey();
            }
        }
        return null;
    }
    
    private FilterOptionRepresentation activityOptionEntry(StreamsFilterOptionProvider provider, I18nResolver i18nResolver)
    {
        StreamsFilterOption activityOption = new StreamsFilterOption.Builder(ACTIVITY_KEY, SELECT)
            .displayName(i18nResolver.getText("streams.filter.option.activity"))
            .helpTextI18nKey("streams.filter.option.help.activity")
            .i18nKey("streams.filter.option.activity")
            .unique(true)
            .values(transformValues(uniqueIndex(activityAlphaSorter.sortedCopy(provider.getActivities()),
                toActivityOptionKey()), toDisplayName()))
            .build();
        return new FilterOptionRepresentation(i18nResolver, activityOption);
    }

    private Function<StreamsFilterOptionProvider.ActivityOption, String> toDisplayName()
    {
        return ActivityOptionToDisplayName.INSTANCE;
    }
    
    private enum ActivityOptionToDisplayName implements Function<StreamsFilterOptionProvider.ActivityOption, String>
    {
        INSTANCE;
        
        public String apply(StreamsFilterOptionProvider.ActivityOption a)
        {
            return a.getDisplayName();
        }
    }

    private static final Ordering<FilterOptionRepresentation> optionAlphaSorter = new Ordering<FilterOptionRepresentation>()
    {
        public int compare(FilterOptionRepresentation option1, FilterOptionRepresentation option2)
        {
            //STRM-1307 project filter should always be listed first
            if (option1.getKey().equals(KEY) && option2.getKey().equals(KEY))
            {
                return 0;
            }
            else if (option1.getKey().equals(KEY))
            {
                return -1;
            }
            else if (option2.getKey().equals(KEY))
            {
                return 1;
            }
            
            return option1.getName().compareTo(option2.getName());
        }
    };

    private static final Ordering<ActivityOption> activityAlphaSorter = new Ordering<ActivityOption>()
    {
        public int compare(ActivityOption option1, ActivityOption option2)
        {
            return option1.getDisplayName().compareTo(option2.getDisplayName());
        }
    };
}
