package com.atlassian.streams.internal.rest.resources;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.ActivityProviders;
import com.atlassian.streams.internal.AppLinksActivityProvider;
import com.atlassian.streams.internal.rest.resources.whitelist.Whitelist;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.sal.api.net.Request.MethodType.GET;
import static com.atlassian.sal.api.net.Request.MethodType.POST;
import static com.atlassian.sal.api.net.Request.MethodType.PUT;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * A resource to act as a simple proxy between our js and remote applications.
 *
 * Entity objects must be sent in responses, as required to bypass servlet error-page config in web.xml http://java.net/jira/browse/JERSEY-389
 */
@Path("url-proxy")
public class UrlProxyResource
{
    private final ActivityProviders activityProviders;
    private final Whitelist whitelist;

    public UrlProxyResource(ActivityProviders activityProviders,
                            Whitelist whitelist)
    {
        this.activityProviders = checkNotNull(activityProviders, "activityProviders");
        this.whitelist = checkNotNull(whitelist, "whitelist");
    }

    /**
     * GET proxy for the given url.
     *
     * @param url the url to proxy. Request parameters should be url encoded to prevent
     *          being interpreted as for use by this resource.
     * @return the proxied url's response
     */
    @GET
    public Response get(@QueryParam("url") String url)
    {
        return execute(url, GET, null);
    }

    /**
     * POST proxy for the given url.
     *
     * @param url the url to proxy. Request parameters should be url encoded to prevent
     *          being interpreted as for use by this resource.
     * @return the proxied url's response
     */
    @POST
    @Consumes("application/json")
    public Response post(@QueryParam("url") String url)
    {
        return execute(url, POST, MediaType.APPLICATION_JSON);
    }

    /**
     * PUT proxy for the given url.
     *
     * @param url the url to proxy. Request parameters should be url encoded to prevent
     *          being interpreted as for use by this resource.
     * @return the proxied url's response
     */
    @PUT
    @Consumes("application/json")
    public Response put(@QueryParam("url") String url)
    {
        return execute(url, PUT, MediaType.APPLICATION_JSON);
    }

    private Response execute(String url, MethodType methodType, @Nullable String contentType)
    {
        Uri uri = Uri.parse(url);
        if (!whitelist.allows(uri.toJavaUri()))
        {
            return Response.status(FORBIDDEN).entity("").build();
        }

        Option<AppLinksActivityProvider> provider = activityProviders.getRemoteProviderForUri(uri);

        try
        {
            if (!provider.isDefined())
            {
                return Response.status(FORBIDDEN).entity("").build();
            }
            Request<?, com.atlassian.sal.api.net.Response> request = provider.get().createRequest(url, methodType);
            request.setFollowRedirects(false);
            if (contentType != null)
            {
                request.addHeader(HttpHeaders.CONTENT_TYPE, contentType);
            }
            Response response = executeRequest(request);

            //if your oauth token on the remote server is revoked, you may get a 401 without having a CredentialsRequiredException thrown
            if (HttpServletResponse.SC_UNAUTHORIZED == response.getStatus())
            {
                return retryRequestAsAnonymous(provider.get(), url, methodType);
            }

            return response;
        }
        catch (final CredentialsRequiredException cre)
        {
            return retryRequestAsAnonymous(provider.get(), url, methodType);
        }
    }

    private Response retryRequestAsAnonymous(AppLinksActivityProvider provider, String url, MethodType methodType)
    {
        try
        {
            return executeRequest(provider.createAnonymousRequest(url, methodType));
        }
        catch (final CredentialsRequiredException cre)
        {
            return Response.status(UNAUTHORIZED).entity("").build();
        }
    }

    private Response executeRequest(Request<?, com.atlassian.sal.api.net.Response> request)
    {
        try
        {
            return Response.status(request.executeAndReturn(new ProxyResponseHandler()).getStatusCode()).entity("").build();
        }
        catch (ResponseException e)
        {
            throw new StreamsException(e);
        }
    }

    private final class ProxyResponseHandler implements ReturningResponseHandler<com.atlassian.sal.api.net.Response, com.atlassian.sal.api.net.Response>
    {
        public com.atlassian.sal.api.net.Response handle(final com.atlassian.sal.api.net.Response response) throws ResponseException
        {
            return response;
        }
    }
}
