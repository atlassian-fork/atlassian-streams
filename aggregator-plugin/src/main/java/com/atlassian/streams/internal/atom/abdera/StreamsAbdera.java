package com.atlassian.streams.internal.atom.abdera;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.internal.feed.FeedEntry;
import com.atlassian.streams.internal.feed.FeedHeader;
import com.atlassian.streams.internal.feed.FeedModel;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.apache.abdera.Abdera;
import org.apache.abdera.ext.thread.ThreadExtensionFactory;
import org.apache.abdera.factory.ExtensionFactory;
import org.apache.abdera.factory.Factory;
import org.apache.abdera.factory.StreamBuilder;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.Entry;
import org.apache.abdera.parser.NamedParser;
import org.apache.abdera.parser.Parser;
import org.apache.abdera.parser.ParserFactory;
import org.apache.abdera.parser.stax.FOMFactory;
import org.apache.abdera.parser.stax.FOMParser;
import org.apache.abdera.parser.stax.FOMParserFactory;
import org.apache.abdera.parser.stax.FOMWriter;
import org.apache.abdera.parser.stax.FOMWriterFactory;
import org.apache.abdera.parser.stax.FOMXPath;
import org.apache.abdera.parser.stax.StaxStreamWriter;
import org.apache.abdera.parser.stax.util.PrettyWriter;
import org.apache.abdera.util.Configuration;
import org.apache.abdera.writer.NamedWriter;
import org.apache.abdera.writer.StreamWriter;
import org.apache.abdera.writer.Writer;
import org.apache.abdera.writer.WriterFactory;
import org.apache.abdera.xpath.XPath;
import org.joda.time.DateTime;

public final class StreamsAbdera
{
    private final static Abdera abdera = newAbdera(newConfiguration());
    
    private StreamsAbdera() {}

    public static Abdera getAbdera()
    {
        return abdera;
    }

    private static Configuration newConfiguration()
    {
        return new StreamsAbderaConfiguration();
    }
    
    private static Abdera newAbdera(Configuration config)
    {
        return new Abdera(config);
    }
    
    /**
     * A {@link FeedEntry} that is just a wrapper for an Atom entry, produced by
     * {@link AbderaAtomFeedParser}.  {@link AbderaAtomFeedRenderer} recognizes this class
     * and reuses the same Atom entry when producing output.
     */
    public static final class AtomParsedFeedEntry extends FeedEntry
    {
        private final Entry atomEntry;
        private final DateTime entryDate;
        
        AtomParsedFeedEntry(Entry atomEntry, DateTime entryDate, Option<FeedModel> sourceFeed)
        {
            super(sourceFeed);
            this.atomEntry = atomEntry;
            this.entryDate = entryDate;
        }
        
        AtomParsedFeedEntry(Entry atomEntry)
        {
            super();
            this.atomEntry = atomEntry;
            // memoize date to avoid reconverting it repeatedly during sorts
            this.entryDate = new DateTime(atomEntry.getPublished());
        }
        
        public Entry getAtomEntry()
        {
            return atomEntry;
        }
        
        public StreamsEntry getStreamsEntry()
        {
            throw new IllegalStateException("can't convert Atom entry back into StreamsEntry");
        }
        
        public DateTime getEntryDate()
        {
            return entryDate;
        }

        @Override
        public FeedEntry toAggregatedEntry(Option<FeedModel> sourceFeed)
        {
            return new AtomParsedFeedEntry(atomEntry, entryDate, sourceFeed);
        }
    }
    
    /**
     * A {@link FeedHeader} that is just a wrapper for an Atom element, produced by
     * {@link AbderaAtomFeedParser}.  {@link AbderaAtomFeedRenderer} recognizes this class
     * and reuses the same element when producing output, since these elements do not need
     * to be parsed.
     */
    public static final class AtomParsedFeedHeader implements FeedHeader
    {
        private Element element;
        
        public AtomParsedFeedHeader(Element element)
        {
            this.element = element;
        }
        
        public Element getElement()
        {
            return element;
        }
    }
    
    @SuppressWarnings("serial")
    private static final class StreamsAbderaConfiguration implements Configuration
    {
        final List<ExtensionFactory> extensionFactories= ImmutableList.<ExtensionFactory>of(
                new ThreadExtensionFactory(), new ActivityStreamsExtensionFactory());
        final Map<String, NamedWriter> namedWriters = ImmutableMap.<String, NamedWriter>of("prettyxml", new PrettyWriter());
        final Map<String, NamedParser> namedParsers = Collections.emptyMap();
        final Map<String, Class<? extends StreamWriter>> streamWriters = ImmutableMap.<String, Class<? extends StreamWriter>>of(
                "default", StaxStreamWriter.class, "fom", StreamBuilder.class);
        
        public String getConfigurationOption(String id)
        {
            return null;
        }

        public String getConfigurationOption(String id, String defaultValue)
        {
            return defaultValue;
        }

        public List<ExtensionFactory> getExtensionFactories()
        {
            return extensionFactories;
        }

        public Map<String, NamedParser> getNamedParsers()
        {
            return namedParsers;
        }

        public Map<String, NamedWriter> getNamedWriters()
        {
            return namedWriters;
        }

        public Map<String, Class<? extends StreamWriter>> getStreamWriters()
        {
            return streamWriters;
        }

        public Factory newFactoryInstance(Abdera abdera)
        {
            return new FOMFactory(abdera);
        }

        public ParserFactory newParserFactoryInstance(Abdera abdera)
        {
            return new FOMParserFactory(abdera);
        }

        public Parser newParserInstance(Abdera abdera)
        {
            return new FOMParser(abdera);
        }

        public StreamWriter newStreamWriterInstance(Abdera abdera)
        {
            return new StaxStreamWriter(abdera);
        }

        public WriterFactory newWriterFactoryInstance(Abdera abdera)
        {
            return new FOMWriterFactory(abdera);
        }

        public Writer newWriterInstance(Abdera abdera)
        {
            return new FOMWriter(abdera);
        }

        public XPath newXPathInstance(Abdera abdera)
        {
            return new FOMXPath(abdera);
        }

        public Configuration addExtensionFactory(ExtensionFactory factory)
        {
            throw new UnsupportedOperationException();
        }

        public Configuration addNamedParser(NamedParser parser)
        {
            throw new UnsupportedOperationException();
        }

        public Configuration addNamedWriter(NamedWriter writer)
        {
            throw new UnsupportedOperationException();
        }

        public Configuration addStreamWriter(Class<? extends StreamWriter> sw)
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object clone()
        {
            throw new UnsupportedOperationException();
        }
    }
}
