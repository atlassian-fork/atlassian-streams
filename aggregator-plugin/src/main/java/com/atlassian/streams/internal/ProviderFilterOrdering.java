package com.atlassian.streams.internal;

import java.util.Set;

import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ordering;

import static com.atlassian.streams.spi.StandardStreamsFilterOption.STANDARD_FILTERS_PROVIDER_KEY;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * An {@link Ordering} that sorts in the following order:
 *
 *   1. "streams" provider
 *   2. Prioritized (as specified during construction), if local
 *   3. Local product providers
 *   4. Local third party providers
 *   5. Grouped per remote applink:
 *     a. Product providers
 *     b. Third party providers
 *
 *   Providers which are the same according to all of the above rules will be sorted alphabetically via name.
 */
public abstract class ProviderFilterOrdering extends Ordering<ProviderFilterRepresentation>
{
    private final Set<String> prioritized;
    private final String THIRD_PARTY = "thirdparty";

    private ProviderFilterOrdering(Iterable<String> prioritized)
    {
        this.prioritized = ImmutableSet.copyOf(prioritized);
    }
    
    public static Ordering<ProviderFilterRepresentation> prioritizing(String... providerNames)
    {
        return prioritizing(asList(providerNames));
    }
    
    public static Ordering<ProviderFilterRepresentation> prioritizing(Iterable<String> providerNames)
    {
        return new ProviderFilterOrdering(providerNames) {};
    }

    public int compare(ProviderFilterRepresentation p1, ProviderFilterRepresentation p2)
    {
        //"streams" provider is always prioritized
        if (p1.getKey().equals(STANDARD_FILTERS_PROVIDER_KEY) || p2.getKey().equals(STANDARD_FILTERS_PROVIDER_KEY))
        {
            if (p1.getKey().equals(STANDARD_FILTERS_PROVIDER_KEY))
            {
                return -1;
            }
            if (p2.getKey().equals(STANDARD_FILTERS_PROVIDER_KEY))
            {
                return 1;
            }
        }

        //prioritized elements are prioritized, if they are local
        if (prioritized.contains(p1.getName()) && !prioritized.contains(p2.getName()) && isEmpty(p1.getApplinkName()))
        {
            return -1;
        }
        if (!prioritized.contains(p1.getName()) && prioritized.contains(p2.getName()) && isEmpty(p2.getApplinkName()))
        {
            return 1;
        }

        if (!p1.getApplinkName().equals(p2.getApplinkName()))
        {
            //group providers by applink name
            return p1.getApplinkName().compareToIgnoreCase(p2.getApplinkName());
        }
        else
        {
            //since both providers are from the same server, place product providers before thirdparty providers
            if (p1.getKey().startsWith(THIRD_PARTY) && !p2.getKey().startsWith(THIRD_PARTY))
            {
                return 1;
            }
            if (!p1.getKey().startsWith(THIRD_PARTY) && p2.getKey().startsWith(THIRD_PARTY))
            {
                return -1;
            }

            //otherwise, alphabetize by name
            return p1.getName().compareToIgnoreCase(p2.getName());
        }
    }
}
