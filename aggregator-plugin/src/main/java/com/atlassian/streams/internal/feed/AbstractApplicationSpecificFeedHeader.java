package com.atlassian.streams.internal.feed;

import java.net.URI;

/**
 * A {@link FeedHeader} that is related to an application link.
 */
public abstract class AbstractApplicationSpecificFeedHeader implements FeedHeader
{
    private final String applicationId;
    private final String applicationName;
    private final URI applicationUri;
                                 
    protected AbstractApplicationSpecificFeedHeader(String applicationId,
                                                    String applicationName,
                                                    URI applicationUri)
    {
        super();
        this.applicationId = applicationId;
        this.applicationName = applicationName;
        this.applicationUri = applicationUri;
    }

    public String getApplicationId()
    {
        return applicationId;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public URI getApplicationUri()
    {
        return applicationUri;
    }
}
