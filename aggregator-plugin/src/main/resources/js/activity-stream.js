var ActivityStreams = ActivityStreams || {};

var Cookie = AJS.Cookie;

/**
 * Creates an activity stream object
 * @method stream
 * @param {Object} options
 *  - {String} streamId Unique identifier for the stream
 *  - {String} maxResults Maximum number of activity items to show
 *  - {HTMLElement} container DOM element to attach stream html to
 *  - {String} title (optional) Title of the stream
 *  - {Function} loaded (optional) Function to run when stream is done loading
 *  - {Function} resized (optional) Function to run when stream content has changed size
 * @return {Object} Activity stream object
 */
ActivityStreams.stream = function(options) {

    var getPref = ActivityStreams.getPref,
        setPref = ActivityStreams.setPref,
        getMsg = ActivityStreams.getMsg,
        streamId = options.id,
        baseUrl = options.baseUrl || '',
        parentContainer = options.container,
        streamContainer,
        loadedCallback =  options.loaded || function() {},
        resizedCallback =  options.resized || function() {},
        isConfigurable = !!options.isConfigurable,
        isConfigured = getPref('isReallyConfigured') === 'true',
        showStreamHeader = !options.hideHeader,
        context = getPref('renderingContext'),
        config,
        maxResults = (options.maxResults && parseInt(options.maxResults)) || 25,
        filterOptions = {
            maxResults: maxResults
        },
        settings = {
            title: getPref('title'),
            numofentries: getPref('numofentries'),
            refresh: getPref('refresh'),
            rules: getSavedRules()
        };

    ActivityStreams.setBaseUrl(baseUrl);

    /**
     * Gets and returns the currently saved filtering rules as an object
     * @method getSavedRules
     * @return {Object} Filtering rules
     */
    function getSavedRules() {
        var json = getPref("rules"),
            obj;
        if (json) {
            obj = getObjectFromEscapedJson(json);
            return obj && obj.providers;
        }
        return '';
    }

    /**
     * Given a JSON string with encoded quote marks, returns an object
     * @method getObjectFromEscapedJson
     * @param {String} str JSON string with quote marks encoded as &#34;
     * @return {Object} Object corresponding to given JSON string
     */
    function getObjectFromEscapedJson(str) {
        if (!str) {
            return undefined;
        }
        return AJS.json.parse(str.replace(/&#34;/g, '\"'));
    }

    function showConfigurationDialog() {
        // the resize will make sure there is enough room to show the complete dialog
        streamContainer.addClass('filter-view')
            .trigger('contentResize.streams');
    }

    /**
     * Hide the configuration dialogue i.e. filter from the stream container
     */
    function hideConfigurationDialog() {
        streamContainer.removeClass('filter-view');
    }

    /**
     * Returns html for the stream feed icon
     * @method buildFeedIcon
     * @return {HTMLElement}
     */
    function buildFeedIcon() {
        return AJS.$('<a href="#" id="activity-stream-feed" target="_top"></a>')
            .attr('title', getMsg('gadget.activity.stream.feed'));
    }

    /**
     * Returns html for the stream header
     * @method buildHeader
     * @param {String} title (optional) Title of the stream
     * @return {HTMLElement}
     */
    function buildHeader(title) {
        var element = AJS.$('<div class="activity-stream-header"></div>'),
            titleContainer = AJS.$('<div class="activity-stream-title-container"></div>').appendTo(element),
            viewsContainer;
        title = title || settings.title;

        AJS.$('<h2 id="stream-title"></h2>')
            .text(title)
            .appendTo(titleContainer);
        buildFeedIcon()
            .appendTo(element);
        viewsContainer = AJS.$('<div class="activity-stream-views"></div>')
            .append(AJS.$('<span class="activity-stream-view full-view-icon"></span>').attr('title', getMsg('gadget.activity.stream.full.view')))
            .append(AJS.$('<span class="activity-stream-view list-view-icon"></span>').attr('title', getMsg('gadget.activity.stream.list.view')));
        if (isConfigurable) {
            viewsContainer.append(AJS.$('<span class="activity-stream-view filter-view-icon" id="filter-icon"></span>').attr('title', getMsg('gadget.activity.stream.filter')));
        }
        viewsContainer.appendTo(element);
        AJS.$('<div class="clearer"></div>')
            .appendTo(element);

        return element;
    }

    /**
     * Returns html for the stream's sparse header (just the feed icon)
     * @method buildSparseHeader
     * @return {HTMLElement}
     */
    function buildSparseHeader() {
        return AJS.$('<div class="activity-stream-sparse-header"></div>')
            .append(buildFeedIcon())
            .append('<div class="throbber-placeholder"></div>')
            .append('<div class="clearer"></div>');
    }

    /**
     * Returns html of error message for sources
     * @method buildBannedSourcesMessage
     * @return {HTMLElement}
     */
    function buildErrorSourcesMessage(errorSources, errorMsg) {
        var div = AJS.$('<div/>');
        var link,
            msgText,
            plural;

        plural = errorSources.length > 1 ? ".plural" : "";
        msgText = getMsg(errorMsg + plural);

        var sources = AJS.$.makeArray(AJS.$.map(errorSources, function (value) {
            // Escape the name of the source
            return AJS.$("<div/>").text(value).html();
        })).join(", ");

        var
            msg = AJS.$('<div></div>').html(AJS.format(msgText, errorSources.length, sources, link)),
            warning = AJS.messages.warning(div, {
                body: msg.html(),
                closeable: true
            });
        ActivityStreams.makeMessageCloseable(warning.find('div.aui-message'));
        warning && warning.find("a").click(function (e) {
            e.preventDefault();
            updateFromUrl(link);
        });
        return div;
    }

    /**
     * Returns html for authorisation request messages, if any
     * @method buildAuthMessages
     * @return {HTMLElement}
     */
    function buildAuthMessages(authMessages) {
        var element = AJS.$('<div class="applinks-auth-messages"></div>'),
            messages = AJS.$('<div class="aui-message aui-message-warning closeable shadowed applinks-auth-request"></div>');

        messages.append(buildAuthTitle());
        messages.append(buildAuthMessageList(authMessages));

        // Workaround for incomplete AJS availability in some products
        ActivityStreams.makeMessageCloseable(messages);

        element.append(messages);
        return element;
    }

    function buildAuthMessageList(authMessages) {
        var messageList = AJS.$('<ul></ul>');

        for (var i = 0, len = authMessages.length; i < len; i++) {
            var authMessage = authMessages[i],
                message = ActivityStreamsApplinks.createAuthRequestBanner(authMessage);
            messageList.append(message);
        }

        return messageList;
    }

    /**
     * Returns html for title of authentication message box.
     * @return {HTMLElement}
     */
    function buildAuthTitle() {
        var title = AJS.$('<p><span class="aui-icon icon-applinks-key"></span></p>');
        title.append(ActivityStreams.getMsg("gadget.activity.stream.auth.request.header"));

        return title;
    }

    /**
     * Returns html for the "no activity found" placeholder
     * @method buildNoActivityMessage
     * @return {HTMLElement}
     */
    function buildNoActivityMessage() {
        return AJS.$('<div class="activity-item no-activity"></div>')
            .text(getMsg('gadget.activity.stream.no.activity.found'));
    }

    /**
     * Returns html for the activity info (icon, timestamp, inline actions)
     * @method buildActionsContainer
     * @param {Object} feedItem Object representing the activity item
     * @param {Array} actionBuilders An array of methods to build HTML elements with events attached to them
     * @return {HTMLElement}
     */
    function buildActionsContainer(feedItem, actionBuilders) {
        var container = AJS.$('<div class="activity-item-actions"></div>'),
            actionContainer = AJS.$('<span class="activity-item-action"></span>');
        for (var i = 0, len = actionBuilders.length; i < len; i++) {
            var action = actionBuilders[i](feedItem);
            action && action.clone && actionContainer.clone().append(action.clone(true)).appendTo(container);
        }
        return container;
    }

    /**
     * Returns html for the activity info (icon, timestamp, inline actions)
     * @method buildActivityInfo
     * @param {Object} feedItem Object representing the activity item
     * @param {string} timestamp String representing when this activity occurred
     * @param {string} timestampISO original ISO formatted string like occurs in XML feed
     * @return {HTMLElement}
     */
    function buildActivityInfo(feedItem, timestamp, timestampISO) {
        var element = AJS.$('<div class="activity-item-info"></div>'),
            actions = ActivityStreams.getActions(feedItem.type),
            iconLink = feedItem.links['http://streams.atlassian.com/syndication/icon'],
            alternateLink = feedItem.links['alternate'],
            timeElement;

        if (iconLink) {
            AJS.$('<img class="icon" height="16" width="16" alt="">')
                .attr({
                          'src': iconLink,
                          'alt': feedItem.iconTitle,
                          'title': feedItem.iconTitle
                      })
                .appendTo(element);
        }

        if (alternateLink) {
            timeElement = AJS.$('<a></a>').attr('href', alternateLink);
        } else {
            timeElement = AJS.$('<span></span>');
        }

        timeElement.addClass('livestamp timestamp')
                   .text(timestamp || '');

        if (typeof moment === 'function') {
            var momentDate = moment(timestampISO).zone(feedItem.timezoneOffset);
            timeElement.attr("datetime", momentDate.format())
                       .attr('title', momentDate.format('LLL'));
        }

        timeElement.appendTo(element);

        if (ActivityStreams.getDateRelativize()) {
        	timeElement.data('relativize', true);
        }

        if (alternateLink) {
            AJS.$('<input type="hidden" class="activity-item-link">')
                .attr('value', alternateLink)
                .appendTo(element);
        }

        if (actions && context !== 'view-issue') {
            buildActionsContainer(feedItem, actions).appendTo(element);
        }

        AJS.$('<div class="activity-item-action-status-container hidden">').appendTo(element);

        return element;
    }

    /**
     * Returns html for an activity item element
     * @method buildActivityItem
     * @param {Object} feedItem Object representing the activity item:
     *  - {String} feedItem.updated When the activity occurred
     *  - {String} feedItem.authors[0].photoUri Location of the profile pic for the activity "author"
     *  - {String} feedItem.summary Summary of the activity
     *  - {String} feedItem.detail (optional) Detail info for the activity, eg comment content
     *  - {String} feedItem.replyTo (optional) URL of issue that can be commented on
     * @return {HTMLElement}
     */
    function buildActivityItem(feedItem) {
        var now = feedItem.now || AJS.Date.handleTimezoneOffset(new Date()),
            element = AJS.$('<div class="activity-item"></div>'),
            timestamp = AJS.Date.getFineRelativeDate(feedItem.date || AJS.Date.parse(feedItem.updated, feedItem.timezoneOffset), now),
            userpic = AJS.$('<span class="user-icon"></span>').appendTo(element),
            author = feedItem.authors[0],
            authorDisplayName = author && (author.name || author.username),
            summary = feedItem.summary || feedItem.content,
            application = feedItem.application,
            desiredIconWidth = 48,
            authorIcon,
            photo,
            summaryContainer = AJS.$('<div class="activity-item-summary"></div>'),
            commitList;

        if (application) {
            application = application.replace('com.atlassian.', '').replace(/\./, '-');
            element.addClass(application + '-activity-item');
        }

        if (author && author.photos) {
            for (var i = 0, ii = author.photos.length; i < ii; i++) {
                photo = author.photos[i];
                if (photo.width == desiredIconWidth) {
                    break;
                }
            }
            authorIcon = AJS.$('<img alt="">').attr({
                                                        'alt': authorDisplayName,
                                                        'title': authorDisplayName,
                                                        'src': photo.uri,
                                                        'width': photo.width,
                                                        'height': photo.height
                                                    });
            if (author.uri) {
                AJS.$('<a href=""></a>')
                    .attr('href', author.uri)
                    .append(authorIcon)
                    .appendTo(userpic);
            } else {
                authorIcon.appendTo(userpic);
            }
        }

        // check if the title and content are in the summary element or if they are the same. in either case,
        // we just want to display the summary.
        //  otherwise use the feedItem title
        if (AJS.$("<div/>").html(summary).find("div.activity-item-description").size() > 0 ||
                feedItem.title == summary) {
            summaryContainer.html(summary);
        } else {
            summaryContainer
                .html(feedItem.title)
                .append(AJS.$("<div class='activity-item-description'></div>").html(summary));
        }

        commitList = summaryContainer.find('ul.commit-list');
        // we have to inject some markup to fisheye commit activity entries to get them to render the way we want
        if (commitList.length) {
            commitList.addClass('modified');
        }

        summaryContainer.appendTo(element);

        element.append(buildActivityInfo(feedItem, timestamp, feedItem.updated));

        AJS.$('<div class="clearer"></div>')
            .appendTo(element);

        return element;
    }

    /**
     * Returns html for a date header that separates activity items by day
     * @method buildDateHeader
     * @return {HTMLElement}
     */
    function buildDateHeader(dateString) {
        return AJS.$('<div class="date-header"></div>')
            .text(dateString);
    }

    /**
     * Returns html for the "Show more" link at the bottom of the stream
     * @method buildShowMoreLink
     * @return {HTMLElement}
     */
    function buildShowMoreLink() {
        var anchor = AJS.$('<a href="#" id="activity-stream-show-more" class="hidden"></a>')
            .click(function(e) {
                e.preventDefault();
                AJS.$('#activity-stream-show-more').addClass('loading');
                filterOptions.maxResults = parseInt(filterOptions.maxResults) + maxResults;
                update();
            });
        AJS.$('<span></span>')
            .text(getMsg('gadget.activity.stream.show.more'))
            .appendTo(anchor);
        return anchor;
    }

    /**
     * Update the configuration and show the filtering/configuration UI.
     * @method buildFilteringOptions
     * @param {HTMLElement} container
     */
    function buildFilteringOptionsAndShowConfigurationDialog(container) {
        // remove any existing AUI error message
        container.empty();
        AJS.$.ajax({
            url: ActivityStreams.getBaseUrl() + '/rest/activity-stream/1.0/config',
            type: 'get',
            dataType: 'json',
            global: false,
            success: function(data) {
                config = ActivityStreams.config(data.filters, settings, container);
                streamContainer.addClass('is-configurable');
                showConfigurationDialog()
            },
            error: function(request) {
                AJS.messages.error(container, {
                    body: getMsg('gadget.activity.stream.error.loading.filters'),
                    closeable: false
                });
                streamContainer.trigger('contentUpdate.streams', getParamsFromSavedSettings());
                showConfigurationDialog()
            }
        });
    }

    /**
     * Retrieves and returns any legacy preferences that were previously defined for the stream
     * @method getLegacyPrefs
     * @return {Object} An object containing any defined legacy preferences
     */
    function getLegacyPrefs() {
        var keys = ActivityStreams.getPrefArray('keys'),
            username = getPref('username'),
            itemKeys = getPref('itemKeys'),
            prefs;
        if (keys && keys.length) {
            prefs = {
                keys: keys
            };
        }
        if (username) {
            prefs = prefs || {};
            prefs.username = username;
        }
        if (itemKeys) {
            prefs = prefs || {};
            prefs.itemKeys = itemKeys;
        }
        return prefs;
    }

    /**
     * Checks for the existence of legacy prefs and converts them to the new rules syntax if necessary
     * @method convertLegacyPrefsToNewStyle
     */
    function convertLegacyPrefsToNewStyle() {
        var legacyPrefs = getLegacyPrefs(),
            prefNameMapping,
            rules,
            // if the stream isn't configurable, don't bother trying to save back to prefs
            set = isConfigurable ? setPref : function() {},
            providers;
        if (legacyPrefs) {
            prefNameMapping = {
                'keys': 'key',
                'username': 'user',
                'itemKeys': 'issue-key'
            };
            AJS.$.each(legacyPrefs, function(prefName, prefValue) {
                rules = rules || [];
                // exclude the __all_projects__ value, since we no longer handle it and that's the default anyways
                if (prefValue != '__all_projects__') {
                    rules.push({
                        rule: prefNameMapping[prefName],
                        operator: 'is',
                        value: prefValue
                    });
                }
                set(prefName, '');
            });
            if (rules) {
                providers = [{
                    'provider': 'streams',
                    'rules': rules
                }];
                set('rules', AJS.json.stringify({
                    'providers': providers
                }));
                settings.rules = providers;
            }
            isConfigured = true;
            set('isReallyConfigured', true);
        }
    }

    /**
     * Converts the settings object to a params object that can be used to construct the streams feed url.
     * This is called when the stream isn't configurable and there are no config settings to parse the params from.
     * @method getParamsFromSavedSettings
     * @return {Object}
     */
    function getParamsFromSavedSettings() {
        var params = {
                maxResults: filterOptions.maxResults,
                relativeLinks: true
            },
            providers = settings.rules || [],
            enabledProviders = [];
        for (var i = 0, ii = providers.length; i < ii; i++) {
            var provider = providers[i],
                providerKey = provider.provider,
                rules = provider.rules,
                stringifiedRules = [];
            if (!provider.disabled) {
                if (providerKey !== 'streams') {
                    enabledProviders.push(providerKey);
                }
                for (var j = 0, jj = rules.length; j < jj; j++) {
                    var rule = rules[j],
                        modifiedRuleObj = ActivityStreams.handleSpecialCases(rule),
                        operator = modifiedRuleObj.operator,
                        value = modifiedRuleObj.value;
                    stringifiedRules.push([rule.rule, operator.toUpperCase(), value].join(' '));
                }
                params[providerKey] = stringifiedRules;
            }
        }
        if (enabledProviders.length < (providers.length - 1)) {
            params['providers'] = enabledProviders.join(' ');
        }
        return params;
    }

    /**
     * Returns html for the stream container (everything except the actual activity items)
     * @method buildStreamContainer
     * @param {String} id Unique id of the stream gadget
     * @return {HTMLElement}
     */
    function buildStreamContainer(id) {
        var view = Cookie.read("streams.view." + streamId) || 'full-view',
            container = AJS.$('<div class="activity-stream-container"></div>')
                .attr('id', id)
                .addClass(view);

        if (showStreamHeader) {
            container.append(buildHeader());
        } else {
            container.append(buildSparseHeader());
            AJS.$('body').addClass('no-title');
        }

        if (!isConfigured) {
            // we need to check if there are saved legacy prefs
            convertLegacyPrefsToNewStyle();
        }

        AJS.$('<div id="filter-options"></div>').appendTo(container);

        AJS.$('<div class="blanket-container"></div>')
            .append(AJS.$('<div class="aui-blanket"></div>'))
            .append(AJS.$('<div class="applinks-auth-confirmation-container"></div>'))
            .append(AJS.$('<div id="activity-stream"></div>'))
            .append(buildShowMoreLink())
            .appendTo(container);

        return container;
    }

    /**
     * Returns html for a stylesheet link element
     * @method buildStylesheetLink
     * @param {String} url Location of the stylesheet
     * @return {HTMLElement}
     */
    function buildStylesheetLink(url) {
        var def = '@import url("' + url + '");',
            stylesheet = AJS.$('<style type="text/css"></style>');
        if (stylesheet[0].styleSheet) {   // Internet Explorer
            stylesheet[0].styleSheet.cssText = def;
            return stylesheet;
        } else {                          // everything else
            return AJS.$('<link media="all" rel="stylesheet" type="text/css">')
                .attr('href', url);
        }

    }

    /**
     * Adds an activity feed element to the stream
     * @method addItem
     * @param {Object} feedItem Object representing the activity item
     * @param {HTMLElement} container DOM element to append activity item element to
     * @param {Boolean} groupWithPrevious If true, item is grouped with the previous item container
     */
    function addItem(feedItem, container, groupWithPrevious) {
        var item = buildActivityItem(feedItem);
        if (groupWithPrevious) {
            item.addClass('activity-item-grouped');
        }
        item.appendTo(container);
    }

    /**
     * Switches between "Full" and "List" stream views
     * @method switchViews
     * @param {Event} e Event object
     */
    function switchViews(e) {
        var target = AJS.$(e.target);
        if (target.hasClass('full-view-icon')) {
            streamContainer
                .addClass('full-view')
                .removeClass('list-view filter-view');
            Cookie.save('streams.view.' + streamId, 'full-view');
        } else if (target.hasClass('list-view-icon')) {
            streamContainer
                .removeClass('full-view filter-view')
                .addClass('list-view');
            Cookie.save('streams.view.' + streamId, 'list-view');
        } else if (target.hasClass('filter-view-icon')) {
            if (streamContainer.hasClass('filter-view')) {
                streamContainer.trigger('hideConfiguration.streams');
            } else {
                streamContainer.trigger('showConfiguration.streams');
            }
        }
        resizedCallback();
    }

    /**
     * Update the href of the feed icon with the specified url
     * @method updateFeedUrl
     * @param {String} url Url to set the feed link to
     */
    function updateFeedUrl(url) {
        AJS.$('#activity-stream-feed').attr('href', url);
    }

    /**
     * Gets the stream feed and populates the container with activity items
     * @method update
     */
    function update(e, options) {
        var url,
            isRuleUpdate = !!options,
            title = getStreamTitle();
        if (isRuleUpdate) {
            filterOptions = options;
            maxResults = parseInt(options.maxResults) || maxResults;
        } else {
            options = filterOptions;
        }
        url = ActivityStreams.getStreamUrl(options);

        // don't make duplicate requests
        if (isRuleUpdate && update.url == url && update.title == title) {
            return;
        }

        update.url = url;
        update.title = title;
        updateFromUrl(url);
    }

    /**
     * Gets the stream feed from the given url and populates the container with activity items
     * @method updateFromUrl
     */
    function updateFromUrl(url) {
        // if there's an existing ajax request, abort it before initiating a new one
        if (update.xhr) {
            // test for existence of update.xhr.abort to work around a bug in jQuery in IE7 (http://bugs.jquery.com/ticket/6498)
            update.xhr.abort && update.xhr.abort();
        }

        streamContainer.addClass('loading');
        update.xhr = AJS.$.ajax({
            type: 'get',
            url: url,
            global: false,
            dataType: 'xml',
            success: function(data, status) {
                // don't do anything if there's an error parsing -- that probably means we aborted the request
                // (note that makeRequest returns status as undefined, so we can't rely on that to determine success)
                if (status !== 'parsererror') {
                    var feed = jAtom(data),
                        items,
                        itemsLength,
                        itemsContainer,
                        authMessages,
                        clone,
                        dayGroup,
                        stylesheets = {},
                        shouldGroupWithPrevious,
                        now = AJS.Date.handleTimezoneOffset(new Date(), feed.timezoneOffset);

                    // Workarounds for rapid prototyping
                    // ActivityStreams.sortActivityItems is not defined by default but can be defined to change the sorting
                    // (or content) of the activity items
                    if (typeof(ActivityStreams.sortActivityItems) === 'function') {
                        feed = ActivityStreams.sortActivityItems(feed) || feed;
                    }
                    // Grouping by username+avatar is the default, but ActivityStreams.groupActivityItems can be defined to change
                    // the grouping rules.  Grouping is still broken up by date headers no matter what.
                    shouldGroupWithPrevious = (typeof(ActivityStreams.groupActivityItems) === 'function') ? ActivityStreams.groupActivityItems : function(current, previous) {
                        // group with previous item if usernames and avatars are identical
                        var username = current.authors[0].username;
                        if (username === 'anonymous') {
                            return false;
                        }

                        var sameUsernames = username && username === previous.authors[0].username;
                        var currentPhoto = current.authors[0].photos[0];
                        var previousPhoto = previous.authors[0].photos[0];
                        var noAvatars = !currentPhoto && !previousPhoto;
                        var sameAvatars = (currentPhoto && previousPhoto && currentPhoto.uri === previousPhoto.uri);

                        return sameUsernames && (noAvatars || sameAvatars);
                    };
                    // End prototyping workarounds

                    items = feed.items;
                    itemsLength = items.length;
                    authMessages = feed.authMessages;

                    //We need the title for the feed xml
                    updateFeedUrl(feed.self + '&title=' + encodeURIComponent(getStreamTitle()));

                    itemsContainer = AJS.$('#activity-stream');
                    clone = itemsContainer.empty().clone();

                    // FIXME: blocked by STRM-2168
                    // if (feed.bannedSources.length > 0) {
                    //     buildErrorSourcesMessage(feed.bannedSources, 'gadget.activity.stream.error.banned').appendTo(clone);
                    // }

                    if (feed.timedOutSources.length > 0) {
                        buildErrorSourcesMessage(feed.timedOutSources, 'gadget.activity.stream.error.timeout').appendTo(clone);
                    }

                    if (feed && authMessages && authMessages.length > 0) {
                        buildAuthMessages(authMessages).appendTo(clone);
                    }

                    if (!feed || !itemsLength) {
                        buildNoActivityMessage().appendTo(clone);
                    } else {
                        for (var i = 0; i < itemsLength; i++) {
                            var item = items[i],
                                groupWithPrevious = false,
                                username = item.authors[0].username,
                                timestamp = item.date = AJS.Date.parse(item.updated, item.timezoneOffset),
                                day = AJS.Date.getCoarseRelativeDate(timestamp, now),
                                entryStylesheets = item.links['http://streams.atlassian.com/syndication/css'];
                            item.now = now;
                            // add date header if necessary
                            if (day && day !== dayGroup) {
                                buildDateHeader(day).appendTo(clone);
                                dayGroup = day;
                            } else {
                                // only group items that occur within the same "date block"
                                groupWithPrevious = i && shouldGroupWithPrevious(item, items[i-1]);
                            }
                            if (entryStylesheets) {
                                AJS.$.each(entryStylesheets, function(i, stylesheet) {
                                    stylesheets[stylesheet] = stylesheets[stylesheet] || stylesheet;
                                });
                            }
                            addItem(item, clone, groupWithPrevious);
                        }

                        // Highlight stream item when mouse is over it
                        AJS.$('div.activity-item', clone)
                            .mouseover(function() {
                                AJS.$(this).addClass('hover');
                            })
                            .mouseout(function() {
                                AJS.$(this).removeClass('hover');
                            });
                    }

                    for (var stylesheet in stylesheets) {
                        var existing = AJS.$('link[href="' + stylesheet + '"][rel="stylesheet"]');
                        // only create a new link if one for this stylesheet doesn't already exist
                        if (!existing.length) {
                            buildStylesheetLink(stylesheet).appendTo(AJS.$('head'));
                        }
                    }

                    // STRM-124: if we asked for 10 results and got back 5, there's no point displaying the show more link
                    AJS.$('#activity-stream-show-more')
                        .removeClass('loading')
                        .toggleClass('hidden', (itemsLength < filterOptions.maxResults));

                    itemsContainer.replaceWith(clone);

                    streamContainer.trigger('contentLoaded.streams');
                    loadedCallback();


                    // re-resize if any images aren't yet loaded:
                    if (AJS.$("img:uncached", clone).length > 0) {
                        if(AJS.debug) {
                            console.log("At least one uncached image is in the stream. We will have to delay resize until they are loaded.");
                        }
                        AJS.$("#activity-stream").waitForImages(function () {
                            if(AJS.debug) {
                                console.log("All images in stream have now loaded. Proceeding with resize...");
                            }
                            resizedCallback();
                        });
                    } else {
                        if(AJS.debug) {
                            console.log("All images are loaded, so no need to wait to resize.");
                        }
                        resizedCallback();
                    }
                }
            },
            error: function(request) {
                AJS.messages.error(AJS.$('#activity-stream'), {
                    body: getMsg('gadget.activity.stream.error.loading.feed'),
                    closeable: true
                });
                AJS.$('#activity-stream-show-more').removeClass('loading').addClass('hidden');
                streamContainer.trigger('contentLoaded.streams');
                loadedCallback();
                resizedCallback();
            },
            complete: function() {
                streamContainer.removeClass('loading');
                delete update.xhr;
            }
        });
    }

    /**
     * Builds out the container html and updates the stream content
     * @method load
     */
    function load() {
        var body = AJS.$('body').addClass('initializing');

        streamContainer = buildStreamContainer(streamId).appendTo(parentContainer);

        streamContainer.bind('contentLoaded.streams', function() {
            body.removeClass('initializing');
            if (typeof AJS.$.fn.livestamp === 'function') {
                AJS.$("span.livestamp").livestamp();
            }
        });

        // Callbacks for the "view" icons
        AJS.$('span.activity-stream-view', streamContainer)
            .click(switchViews)
            .mouseover(function() {
                AJS.$(this).addClass('hover');
            })
            .mouseout(function() {
                AJS.$(this).removeClass('hover');
            });

        streamContainer
            .bind('contentResize.streams contentResize.smartConfig', resizedCallback)
            .bind('contentUpdate.streams', update)
            .bind('showConfiguration.streams', function () {
                if (!streamContainer.hasClass('is-configurable')) {
                    buildFilteringOptionsAndShowConfigurationDialog(AJS.$('#filter-options', streamContainer));
                }
                else {
                    showConfigurationDialog()
                }
            })
            .bind('hideConfiguration.streams', hideConfigurationDialog);

        AJS.$(document)
            .bind('applinks.auth.approved', function (eventObject) {
                eventObject.preventDefault();
                update();
            });

        if (isConfigurable) {
            // if the gadget hasn't been configured yet, we want to show the filtering ui
            if (!isConfigured) {
                streamContainer.trigger('showConfiguration.streams');
                streamContainer.bind('saveFilter.streams', function() {
                    // we have to set isConfigured to true (even though that's the default) so that Confluence
                    // will enable the "Insert" button for the streams macro
                    setPref('isConfigured', true);
                    setPref('isReallyConfigured', true);
                    streamContainer.unbind('saveFilter.streams');
                });
                streamContainer.addClass('filter-view');
            } else {
                streamContainer.trigger('contentUpdate.streams', getParamsFromSavedSettings());
            }
        } else {
            streamContainer.trigger('contentUpdate.streams', getParamsFromSavedSettings());
            streamContainer.addClass('not-configurable');
        }
    }

    /**
     * Animates the stream for wallboard view
     * @method animate
     */
    function animate() {
        AJS.$('#activity-stream .activity-item, #activity-stream .date-header').css('display', 'none').addClass('streams-animate-hidden');
        fadeInLast();
    }

    /**
     * Get the title for this stream's config. Take it from the user input box if it's being edited
     * @method getStreamTitle
     * @return {String} The title text
     */
    function getStreamTitle() {
        var $input = AJS.$('#stream-title-edit');
        return $input.css('display') == 'none' ?  AJS.$('#stream-title').text() : $input.val();
    }

    /**
     * Fade in the last hidden activity item
     * @method fadeInLast
     */
    function fadeInLast() {
        var hiddenElements = AJS.$('#activity-stream .streams-animate-hidden'),
            elementToShow,
            dateHeader;

        if (hiddenElements.length) {

            elementToShow = hiddenElements.filter('.activity-item:last');
            dateHeader = elementToShow.prevAll('.date-header:first');

            if (dateHeader.hasClass('streams-animate-hidden')) {
                dateHeader.fadeIn('slow', function() {
                    dateHeader.removeClass('streams-animate-hidden');
                });
            }

            elementToShow.fadeIn('slow', function() {
                elementToShow.removeClass('streams-animate-hidden');
            });

            setTimeout(fadeInLast, 3000);
        }
    }

    load();

    return {
        // completely reloads the stream
        load: load,

        // updates the stream with the latest entries
        update: update,

        // returns the current number of maximum feed results to get
        getMaxResults: function () {
            return filterOptions.maxResults;
        },

        // animates the stream for wallboard view
        animate: animate

    };
};

// To bridge existing JIRA functionality on View Issue screen, should be temporary
function ActivityFeed(id, url, maxResults) {
    return ActivityStreams.stream({id: id, url: url, maxResults: maxResults});
}
