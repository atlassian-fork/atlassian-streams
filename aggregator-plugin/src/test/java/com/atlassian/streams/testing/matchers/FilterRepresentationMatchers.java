package com.atlassian.streams.testing.matchers;

import java.util.List;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.bamboo.BambooFilterOptionProvider;
import com.atlassian.streams.confluence.ConfluenceFilterOptionProvider;
import com.atlassian.streams.crucible.CrucibleFilterOptionProvider;
import com.atlassian.streams.internal.rest.representations.FilterOptionRepresentation;
import com.atlassian.streams.internal.rest.representations.FilterOptionRepresentation.FilterOptionOperatorRepresentation;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.jira.JiraFilterOptionProvider;
import com.atlassian.streams.spi.StreamsFilterOption;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.core.CombinableMatcher;

import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_OBJECT_VERB_SEPARATOR;
import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.core.CombinableMatcher.CombinableBothMatcher;

public final class FilterRepresentationMatchers
{
    public static Matcher<? super StreamsConfigRepresentation> hasStandardFilters()
    {
        return hasStandardFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of());
    }

    public static Matcher<? super StreamsConfigRepresentation> hasStandardFilters(Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return hasStandardFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(matcher));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasStandardFilters(Matcher<? super ProviderFilterRepresentation> m1, Matcher<? super ProviderFilterRepresentation> m2)
    {
        return hasStandardFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(m1, m2));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasStandardFilters(Matcher<? super ProviderFilterRepresentation> m1, Matcher<? super ProviderFilterRepresentation> m2, Matcher<? super ProviderFilterRepresentation> m3)
    {
        return hasStandardFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(m1, m2, m3));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasStandardFilters(Matcher<? super ProviderFilterRepresentation>... matchers)
    {
        return hasStandardFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>copyOf(matchers));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasStandardFilters(Iterable<Matcher<? super ProviderFilterRepresentation>> matchers)
    {
        CombinableBothMatcher<ProviderFilterRepresentation> isStandardProvider = both(withProviderKey(equalTo("streams")));
        return hasProviderFilter(isStandardProvider.and(allOf(matchers)));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasFisheyeFilters()
    {
        return hasFisheyeFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of());
    }

    public static Matcher<? super StreamsConfigRepresentation> hasFisheyeFilters(Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return hasFisheyeFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(matcher));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasFisheyeFilters(Matcher<? super ProviderFilterRepresentation>... matchers)
    {
        return hasFisheyeFilters(ImmutableList.copyOf(matchers));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasFisheyeFilters(Iterable<Matcher<? super ProviderFilterRepresentation>> matchers)
    {
        CombinableBothMatcher<ProviderFilterRepresentation> isFisheyeProvider = both(withProviderKey(equalTo("source")));
        return hasProviderFilter(isFisheyeProvider.and(allOf(matchers)));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasFisheyeActivityFilterOption()
    {
        return hasFisheyeFilters(withOption(withOptionKey(ACTIVITY_KEY), withOptionValueKeys("changeset:push")));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasCrucibleFilters()
    {
        return hasCrucibleFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of());
    }

    public static Matcher<? super StreamsConfigRepresentation> hasCrucibleFilters(Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return hasCrucibleFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(matcher));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasCrucibleFilters(Matcher<? super ProviderFilterRepresentation>... matchers)
    {
        return hasCrucibleFilters(ImmutableList.copyOf(matchers));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasCrucibleFilters(Iterable<Matcher<? super ProviderFilterRepresentation>> matchers)
    {
        CombinableBothMatcher<ProviderFilterRepresentation> isCrucibleProvider = both(withProviderKey(equalTo("reviews")));
        return hasProviderFilter(isCrucibleProvider.and(allOf(matchers)));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasCrucibleActivityFilterOption()
    {
        return hasCrucibleFilters(withOption(withOptionKey(ACTIVITY_KEY), withOptionValueKeys(
            transform(CrucibleFilterOptionProvider.activities, toActivityOptionKey()))));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasBambooFilters()
    {
        return hasBambooFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of());
    }

    public static Matcher<? super StreamsConfigRepresentation> hasBambooFilters(Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return hasBambooFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(matcher));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasBambooFilters(Matcher<? super ProviderFilterRepresentation>... matchers)
    {
        return hasBambooFilters(ImmutableList.copyOf(matchers));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasBambooFilters(Iterable<Matcher<? super ProviderFilterRepresentation>> matchers)
    {
        CombinableBothMatcher<ProviderFilterRepresentation> isBambooProvider = both(withProviderKey(equalTo("builds")));
        return hasProviderFilter(isBambooProvider.and(allOf(matchers)));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasBambooActivityFilterOption()
    {
        return hasBambooFilters(withOption(withOptionKey(ACTIVITY_KEY), withOptionValueKeys(
            transform(BambooFilterOptionProvider.activities, toActivityOptionKey()))));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasConfluenceFilters()
    {
        return hasConfluenceFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of());
    }

    public static Matcher<? super StreamsConfigRepresentation> hasConfluenceFilters(Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return hasConfluenceFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(matcher));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasConfluenceFilters(Matcher<? super ProviderFilterRepresentation>... matchers)
    {
        return hasConfluenceFilters(ImmutableList.copyOf(matchers));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasConfluenceFilters(Iterable<Matcher<? super ProviderFilterRepresentation>> matchers)
    {
        CombinableBothMatcher<ProviderFilterRepresentation> isConfluenceProvider = both(withProviderKey(equalTo("wiki")));
        return hasProviderFilter(isConfluenceProvider.and(allOf(matchers)));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasConfluenceActivityFilterOption()
    {
        return hasConfluenceFilters(withOption(withOptionKey(ACTIVITY_KEY), withOptionValueKeys(
            transform(ConfluenceFilterOptionProvider.activities, toActivityOptionKey()))));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasJiraFilters()
    {
        return hasJiraFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of());
    }

    public static Matcher<? super StreamsConfigRepresentation> hasJiraFilters(Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return hasJiraFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(matcher));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasJiraFilters(Matcher<? super ProviderFilterRepresentation>... matchers)
    {
        return hasJiraFilters(ImmutableList.copyOf(matchers));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasJiraFilters(Iterable<Matcher<? super ProviderFilterRepresentation>> matchers)
    {
        CombinableBothMatcher<ProviderFilterRepresentation> isJiraProvider = both(withProviderKey(equalTo("issues")));
        return hasProviderFilter(isJiraProvider.and(allOf(matchers)));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasJiraActivityFilterOption()
    {
        return hasJiraFilters(withOption(withOptionKey(ACTIVITY_KEY), withOptionValueKeys(
            transform(JiraFilterOptionProvider.activities, toActivityOptionKey()))));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasThirdPartyFilters()
    {
        return hasThirdPartyFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of());
    }

    public static Matcher<? super StreamsConfigRepresentation> hasThirdPartyFilters(Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return hasThirdPartyFilters(ImmutableList.<Matcher<? super ProviderFilterRepresentation>>of(matcher));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasThirdPartyFilters(Matcher<? super ProviderFilterRepresentation>... matchers)
    {
        return hasThirdPartyFilters(ImmutableList.copyOf(matchers));
    }

    public static Matcher<? super StreamsConfigRepresentation> hasThirdPartyFilters(Iterable<Matcher<? super ProviderFilterRepresentation>> matchers)
    {
        CombinableBothMatcher<ProviderFilterRepresentation> isThirdPartyProvider = both(withProviderKey(equalTo("thirdparty")));
        return hasProviderFilter(isThirdPartyProvider.and(allOf(matchers)));
    }

    public static Matcher<? super FilterOptionRepresentation> withOptionValueKeys(final Iterable<String> valueKeys)
    {
        return withOptionValueKeys(toArray(valueKeys, String.class));
    }

    public static Matcher<? super FilterOptionRepresentation> withOptionValueKeys(final String... valueKeys)
    {
        return new TypeSafeDiagnosingMatcher<FilterOptionRepresentation>()
        {
            private final Matcher<Iterable<String>> matcher = hasItems(valueKeys);

            @Override
            protected boolean matchesSafely(FilterOptionRepresentation option, Description mismatchDescription)
            {
                if (!matcher.matches(option.getValues().keySet()))
                {
                    mismatchDescription.appendText("filter option value keys was ");
                    matcher.describeMismatch(option.getValues().keySet(), mismatchDescription);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("filter option value keys is ").appendDescriptionOf(matcher);
            }
        };
    }

    public static Matcher<? super ProviderFilterRepresentation> withProviderKey(final Matcher<? super String> keyMatcher)
    {
        return new TypeSafeDiagnosingMatcher<ProviderFilterRepresentation>()
        {
            @Override
            protected boolean matchesSafely(ProviderFilterRepresentation option, Description mismatchDescription)
            {
                if (!keyMatcher.matches(option.getKey()))
                {
                    mismatchDescription.appendText("key was ").appendValue(option.getKey());
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendDescriptionOf(keyMatcher);
            }
        };
    }

    public static Matcher<? super StreamsConfigRepresentation> hasProviderFilter(final Matcher<? super ProviderFilterRepresentation> matcher)
    {
        return new TypeSafeDiagnosingMatcher<StreamsConfigRepresentation>()
        {
            private final Matcher<? super Iterable<ProviderFilterRepresentation>> m = hasItem(matcher);

            @Override
            protected boolean matchesSafely(StreamsConfigRepresentation rep, Description mismatchDescription)
            {
                if (!m.matches(rep.getFilters()))
                {
                    mismatchDescription.appendText("provider filters were ");
                    m.describeMismatch(rep.getFilters(), mismatchDescription);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("provider filters are ").appendDescriptionOf(m);
            }
        };
    }

    public static Matcher<ProviderFilterRepresentation> hasOption(StreamsFilterOption option)
    {
        return withOption(
                withOptionKey(option.getKey()),
                withFilterName(equalTo(option.getDisplayName())),
                withOperators(option.getFilterType().getOperators()));
    }

    public static Matcher<? super ProviderFilterRepresentation> withFilterProviderName(final Matcher<? super String> matcher)
    {
        return new TypeSafeDiagnosingMatcher<ProviderFilterRepresentation>()
        {
            @Override
            protected boolean matchesSafely(ProviderFilterRepresentation rep, Description mismatchDescription)
            {
                if (!matcher.matches(rep.getName()))
                {
                    mismatchDescription.appendText("application name ");
                    matcher.describeMismatch(rep.getName(), mismatchDescription);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("application name").appendDescriptionOf(matcher);
            }
        };
    }

    public static Matcher<ProviderFilterRepresentation> withOption(Matcher<? super FilterOptionRepresentation> matcher)
    {
        return withOption(ImmutableList.<Matcher<? super FilterOptionRepresentation>>of(matcher));
    }

    public static Matcher<ProviderFilterRepresentation> withOption(Matcher<? super FilterOptionRepresentation> m1, Matcher<? super FilterOptionRepresentation> m2)
    {
        return withOption(ImmutableList.<Matcher<? super FilterOptionRepresentation>>of(m1, m2));
    }

    public static Matcher<ProviderFilterRepresentation> withOption(Matcher<? super FilterOptionRepresentation> m1, Matcher<? super FilterOptionRepresentation> m2, Matcher<? super FilterOptionRepresentation> m3)
    {
        return withOption(ImmutableList.<Matcher<? super FilterOptionRepresentation>>of(m1, m2, m3));
    }

    public static Matcher<ProviderFilterRepresentation> withOption(Matcher<? super FilterOptionRepresentation>... matchers)
    {
        return withOption(ImmutableList.copyOf(matchers));
    }

    public static Matcher<ProviderFilterRepresentation> withOption(final Iterable<Matcher<? super FilterOptionRepresentation>> matchers)
    {
        return new TypeSafeDiagnosingMatcher<ProviderFilterRepresentation>()
        {
            private final Matcher<? super Iterable<? super FilterOptionRepresentation>> m = hasItem(allOf(matchers));

            @Override
            protected boolean matchesSafely(ProviderFilterRepresentation providerFilters, Description mismatchDescription)
            {
                if (!m.matches(providerFilters.getOptions()))
                {
                    mismatchDescription.appendText("filter options was ");
                    m.describeMismatch(providerFilters.getOptions(), mismatchDescription);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("filter options is ").appendDescriptionOf(m);
            }
        };
    }

    public static Matcher<FilterOptionRepresentation> withOptionKey(String optionKey)
    {
        return new WithOptionKey(optionKey);
    }

    private static final class WithOptionKey extends TypeSafeDiagnosingMatcher<FilterOptionRepresentation>
    {
        private final String optionKey;

        public WithOptionKey(String optionKey)
        {
            this.optionKey = optionKey;
        }

        @Override
        protected boolean matchesSafely(FilterOptionRepresentation option, Description mismatchDescription)
        {
            if (!option.getKey().equals(optionKey))
            {
                mismatchDescription.appendText("Filter option key is ").appendValue(option.getKey());
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("filter option key is ").appendValue(optionKey);
        }
    }

    public static Matcher<FilterOptionRepresentation> withOptionName(String optionName)
    {
        return new WithOptionName(optionName);
    }

    private static final class WithOptionName extends TypeSafeDiagnosingMatcher<FilterOptionRepresentation>
    {
        private final String optionName;

        public WithOptionName(String optionName)
        {
            this.optionName = optionName;
        }

        @Override
        protected boolean matchesSafely(FilterOptionRepresentation option, Description mismatchDescription)
        {
            if (!option.getName().equals(optionName))
            {
                mismatchDescription.appendText("Filter option name is ").appendValue(option.getName());
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("filter option name is ").appendValue(optionName);
        }
    }

    public static Matcher<FilterOptionRepresentation> withFilterName(Matcher<? super String> matcher)
    {
        return new WithFilterName(matcher);
    }

    private static final class WithFilterName extends TypeSafeDiagnosingMatcher<FilterOptionRepresentation>
    {
        private final Matcher<? super String> matcher;

        public WithFilterName(Matcher<? super String> matcher)
        {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(FilterOptionRepresentation option, Description mismatchDescription)
        {
            if (option.getName() == null)
            {
                mismatchDescription.appendText("no display name");
                return false;
            }
            if (!matcher.matches(option.getName()))
            {
                mismatchDescription.appendText("display name ");
                matcher.describeMismatch(option.getName(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("filter display name ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<FilterOptionRepresentation> withOperators(Operator... operators)
    {
        return new WithOperators(ImmutableList.copyOf(operators));
    }

    public static Matcher<FilterOptionRepresentation> withOperators(Iterable<Operator> operators)
    {
        return new WithOperators(operators);
    }

    private static final class WithOperators extends TypeSafeDiagnosingMatcher<FilterOptionRepresentation>
    {
        private final Iterable<Operator> operators;

        public WithOperators(Iterable<Operator> operators)
        {
            this.operators = operators;
        }

        @Override
        protected boolean matchesSafely(FilterOptionRepresentation option, Description mismatchDescription)
        {
            if (option.getOperators() == null || option.getOperators().size() == 0)
            {
                mismatchDescription.appendText("no filter operator");
                return false;
            }
            return containsOperators(transform(operators, toOperatorKeys)).matches(transform(option.getOperators(), toOperatorRepresentationKeys));
        }

        public void describeTo(Description description)
        {
            description.appendText("filter operators ").appendValueList("[", ", ", "]", operators);
        }
    }

    private static Function<FilterOptionOperatorRepresentation, String> toOperatorRepresentationKeys = new Function<FilterOptionOperatorRepresentation, String>()
    {
        public String apply(FilterOptionOperatorRepresentation representation)
        {
            return representation.getKey();
        }
    };

    public static Matcher<? super Iterable<String>> containsOperators(Iterable<String> keys)
    {
        List<Matcher<? super String>> matchers =  ImmutableList.copyOf(transform(keys, toOperatorMatcher));
        return containsInAnyOrder(matchers);
    }

    private static final Function<String, Matcher<? super String>> toOperatorMatcher = new Function<String, Matcher<? super String>>()
    {
        public Matcher<? super String> apply(String key)
        {
            return equalTo(key);
        }
    };

    private static final Function<Operator, String> toOperatorKeys = new Function<Operator, String>()
    {
        public String apply(Operator operator)
        {
            return operator.getKey();
        }
    };

    public static Matcher<? super FilterOptionRepresentation> withValues(String... keys)
    {
        return new FilterOptionWithValuesMatcher(keys);
    }

    private static final class FilterOptionWithValuesMatcher extends TypeSafeDiagnosingMatcher<FilterOptionRepresentation>
    {
        private final Matcher<Iterable<? extends String>> matcher;

        public FilterOptionWithValuesMatcher(String... keys)
        {
            this.matcher = containsInAnyOrder(keys);
        }

        @Override
        protected boolean matchesSafely(FilterOptionRepresentation option, Description mismatchDescription)
        {
            return matcher.matches(option.getValues().keySet());
        }

        public void describeTo(Description description)
        {
            description.appendText("value keys ").appendDescriptionOf(matcher);
        }
    }

    private static Function<Pair<ActivityObjectType, ActivityVerb>, String> toActivityOptionKey()
    {
        return ActivityObjectTypeVerbPairToActivityOptionKey.INSTANCE;
    }

    private enum ActivityObjectTypeVerbPairToActivityOptionKey implements Function<Pair<ActivityObjectType, ActivityVerb>, String>
    {
        INSTANCE;

        public String apply(Pair<ActivityObjectType, ActivityVerb> p)
        {
            return p.first() + ACTIVITY_OBJECT_VERB_SEPARATOR + p.second();
        }
    }
}
