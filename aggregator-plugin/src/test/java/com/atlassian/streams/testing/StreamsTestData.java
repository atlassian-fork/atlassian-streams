package com.atlassian.streams.testing;

import java.net.URI;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityObjectTypes;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.ActivityVerbs;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.StreamsEntry.HasAlternateLinkUri;
import com.atlassian.streams.api.StreamsEntry.HasApplicationType;
import com.atlassian.streams.api.StreamsEntry.HasAuthors;
import com.atlassian.streams.api.StreamsEntry.HasId;
import com.atlassian.streams.api.StreamsEntry.HasPostedDate;
import com.atlassian.streams.api.StreamsEntry.HasRenderer;
import com.atlassian.streams.api.StreamsEntry.HasVerb;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.feed.ActivitySourceTimeOutFeedHeader;
import com.atlassian.streams.internal.feed.FeedHeader;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.joda.time.DateTimeZone.UTC;

public class StreamsTestData
{
    public static final URI BASE_URI = URI.create("http://localhost:3990/streams");
    public static final String AGGREGATED_TITLE = "Aggregated Title";
    public static final Uri AGGREGATED_URI = Uri.parse("http://foo.com/aggregated");
    public static final Uri FEED_URI = Uri.parse("http://foo.com/1");
    public static final Uri FEED_URI_2 = Uri.parse("http://foo.com/2");
    public static final String FEED_TITLE = "feed";
    public static final String FEED_SUBTITLE = "sub";
    public static final String FEED_TITLE_2 = "need";
    public static final String FEED_AUTHOR = "joeBagADonuts";
    public static final String FEED_AUTHOR_2 = "me";
    public static final int TIMEOUT = 1;
    public static final DateTime FEED_DATE = new DateTime(2011, 4, 1, 13, 14, 15, 3, DateTimeZone.UTC);
    public static final String FEED_DATE_STR = "2011-04-01T13:14:15.003Z";

    public static final String ENTRY_URI = "http://somewhere.com";
    public static final String ENTRY_APP_NAME = "test";
    public static final String ENTRY_APP_NAME_2 = "test2";
    public static final String ENTRY_OBJECT_KEY = "One";
    public static final String ENTRY_OBJECT_NAME = "Activity Object One";
    public static final String ENTRY_USER_NAME = "someone";
    public static final String ENTRY_USER_FULL_NAME = "Some One";
    public static final String ENTRY_TITLE = "someone did something";
    public static final String ENTRY_SUMMARY = "a summary of the entry";
    public static final String ENTRY_CONTENT = "some detailed content for the entry";
    public static final DateTime ENTRY_DATE = new DateTime(2011, 3, 31, 10, 11, 12, 4, DateTimeZone.UTC);
    public static final String ENTRY_DATE_STR = "2011-03-31T10:11:12.004Z";
    public static final String ENTRY_ALTERNATE_URL = "http://www.example.com/alternate/One";
    public static final String TYPES_BASE_URL = "http://example.com/types/";
    public static final String TYPE_TEST = "testtype";
    public static final String TYPE_TEST_URL = TYPES_BASE_URL + TYPE_TEST;
    public static final String VERBS_BASE_URL = "http://example.com/verbs/";
    public static final String VERB_TEST = "test";
    public static final String VERB_TEST_URL = VERBS_BASE_URL + VERB_TEST;
    public static final String VERB_CHILD = "child";
    public static final String VERB_CHILD_URL = VERBS_BASE_URL + VERB_CHILD;
    public static final String USER_PROFILE_URL = "http://localhost:3990/streams/browse/user/someone";
    public static final String USER_PHOTO_URL = "http://profilephotourl";
    public static final String USER_AVATAR_URL = "http://localhost:3990/streams/browse/avatar/someone";

    // TimeZone test data
    public static final DateTime NON_DST_DATE = new DateTime(2011, 1, 1, 1, 0, 0, 0, DateTimeZone.UTC);
    public static final DateTime DST_DATE = new DateTime(2011, 7, 1, 1, 0, 0, 0, DateTimeZone.UTC);
    public static final DateTimeZone LA_TIMEZONE = DateTimeZone.forID("America/Los_Angeles");
    public static final DateTimeZone NY_TIMEZONE = DateTimeZone.forID("America/New_York");
    public static final String LA_TIMEZONE_OFFSET = "-0800";
    public static final String LA_TIMEZONE_DST_OFFSET = "-0700";
    public static final String NY_TIMEZONE_OFFSET = "-0500";

    public static StreamsEntry.Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors> newEntryParams(DateTime date, StreamsEntry.ActivityObject object)
    {
        return StreamsEntry.params()
                .id(URI.create(ENTRY_URI))
                .postedDate(date)
                .alternateLinkUri(URI.create(ENTRY_URI))
                .applicationType(ENTRY_APP_NAME)
                .verb(testVerb())
                .addActivityObject(object)
                .authors(ImmutableNonEmptyList.of(new UserProfile.Builder(ENTRY_USER_NAME).fullName(ENTRY_USER_FULL_NAME).build()))
                .renderer(newRenderer(URI.create(ENTRY_URI), ENTRY_TITLE, ENTRY_SUMMARY));
    }
    
    public static StreamsEntry.Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors> newEntryParams()
    {
        return newEntryParams(new DateTime(UTC));
    }

    public static StreamsEntry.Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors> newEntryParams(DateTime date)
    {
        return newEntryParams(date, newActivityObject(ENTRY_OBJECT_KEY));
    }
    
    public static StreamsEntry.Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors> newEntryParamsWithoutObject()
    {
	return newEntryParamsWithoutObject(new DateTime(UTC));
    }

    public static StreamsEntry.Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors> newEntryParamsWithoutObject(DateTime date)
    {
        return StreamsEntry.params()
                .id(URI.create(ENTRY_URI))
                .postedDate(date)
                .alternateLinkUri(URI.create(ENTRY_URI))
                .applicationType(ENTRY_APP_NAME)
                .verb(testVerb())
                .authors(ImmutableNonEmptyList.of(new UserProfile.Builder(ENTRY_USER_NAME).fullName(ENTRY_USER_FULL_NAME).build()))
                .renderer(newRenderer(URI.create(ENTRY_URI), ENTRY_TITLE, ENTRY_SUMMARY));
    }
    
    public static Renderer newRenderer(final URI baseUri, String title, String content)
    {
        return newRenderer(baseUri, title, some(content), none(String.class));
    }

    public static Renderer newRenderer(final URI baseUri, String title, Option<String> content, Option<String> summary)
    {
        Renderer renderer = mock(Renderer.class);
        when(renderer.renderTitleAsHtml(any(StreamsEntry.class))).thenReturn(new Html(title));
        when(renderer.renderContentAsHtml(any(StreamsEntry.class))).thenReturn(content.map(html()));
        when(renderer.renderSummaryAsHtml(any(StreamsEntry.class))).thenReturn(summary.map(html()));
        return renderer;
    }

    public static StreamsEntry.ActivityObject newActivityObject(String name)
    {
        return newActivityObject(name, some(name), some(URI.create("http://www.example.com/alternate/" + name)));
    }

    public static StreamsEntry.ActivityObject newActivityObject(String name, Option<String> id, Option<URI> linkUri)
    {
        return new StreamsEntry.ActivityObject(StreamsEntry.ActivityObject.params()
                .id(id)
                .activityObjectType(testObjectType())
                .title(some("Activity Object " + name))
                .alternateLinkUri(linkUri));
    }

    private static final ActivityObjectTypes.TypeFactory typeFactory = ActivityObjectTypes.newTypeFactory(TYPES_BASE_URL);
    public static ActivityObjectType testObjectType()
    {
        return typeFactory.newType(TYPE_TEST);
    }

    private static final ActivityVerbs.VerbFactory verbFactory = ActivityVerbs.newVerbFactory(VERBS_BASE_URL);
    public static ActivityVerb testVerb()
    {
        return verbFactory.newVerb(VERB_TEST);
    }

    public static ActivityVerb childVerb()
    {
        return verbFactory.newVerb(VERB_CHILD, testVerb());
    }
    
    public static FeedHeader emptyFeedHeader()
    {
        return new DummyFeedHeader();
    }

    private static class DummyFeedHeader implements FeedHeader
    {
    }

    public static FeedHeader timeoutHeader(String applicationName)
    {
        return new ActivitySourceTimeOutFeedHeader(applicationName);
    }
}
