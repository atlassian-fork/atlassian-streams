package com.atlassian.streams.testing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * Slimline copy of it.com.atlassian.jira.plugin.ext.fisheye.controller.InstanceController.
 * Used to force reindex of FeCru's repos. and hence make sure all comments etc are loading
 * so that activity streams are what we expect.
 */
public class FeCruInstanceController
{

    private static final int SOCKET_TIMEOUT = 1000 * 60;
    private final int controlPort;
    private static final Logger log = LoggerFactory.getLogger(FeCruInstanceController.class);

    public FeCruInstanceController(int controlPort) {

        this.controlPort = controlPort;
    }

    public int getControlPort() {
        return controlPort;
    }

    public void reindexRepositories() throws IOException {
        executeFishEyeControlCmd("reindexsync");
    }

    public void executeFishEyeControlCmd(String cmd) throws IOException {
        Socket s = null;
        try {
            s = new Socket("localhost", getControlPort());
            s.setSoTimeout(SOCKET_TIMEOUT);
            OutputStream out = s.getOutputStream();
            cmd = "cenqua\r\n" + cmd + "\r\n";
            long start = System.currentTimeMillis();
            out.write(cmd.getBytes());
            out.flush();
            s.shutdownOutput();
            readToEnd(s.getInputStream());
            log.info(toString() + " reindex complete " + (System.currentTimeMillis() - start) + "ms");
        } catch (SocketTimeoutException e) {
            log.error("Socket timed out when executing command '" + cmd + "' (" + SOCKET_TIMEOUT + "ms)", e);
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }

    /**

     * FISH-233 read any further bytes sent by FishEye whilst shutting down (ensure we're not blocking the socket from closing)

     * @param is InputStream from Socket

     * @throws IOException if an I/O exception occurs whilst reading from stream

     */

    private void readToEnd(InputStream is) throws IOException {

        int c;
        StringBuilder out = new StringBuilder();

        while ((c = is.read()) != -1)  {
            out.append((char) c);
        }

        log.info("Read from socket: " + out.toString());
    }


}


