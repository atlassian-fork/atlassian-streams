package com.atlassian.streams.internal.feed;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.ActivityProvider.Error;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.testing.StreamsTestData.AGGREGATED_URI;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_APP_NAME;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_APP_NAME_2;
import static com.atlassian.streams.testing.StreamsTestData.FEED_AUTHOR;
import static com.atlassian.streams.testing.StreamsTestData.FEED_AUTHOR_2;
import static com.atlassian.streams.testing.StreamsTestData.FEED_TITLE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_TITLE_2;
import static com.atlassian.streams.testing.StreamsTestData.FEED_URI;
import static com.atlassian.streams.testing.StreamsTestData.FEED_URI_2;
import static com.atlassian.streams.testing.StreamsTestData.emptyFeedHeader;
import static com.atlassian.streams.testing.StreamsTestData.timeoutHeader;
import static com.atlassian.streams.testing.StreamsTestData.newEntryParams;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.joda.time.DateTimeZone.UTC;

@RunWith(MockitoJUnitRunner.class)
public class FeedAggregatorTest
{
    FeedAggregator aggregator;

    @Mock I18nResolver i18nResolver;

    @Before
    public void createFeedAggregator()
    {
        aggregator = new FeedAggregator();
    }

    @Test
    public void assertThatAggregatingASingleFeedReturnsAnIdenticalFeed()
    {
        FeedModel inputFeed = createSimpleFeed(AGGREGATED_URI, FEED_TITLE, FEED_AUTHOR, 1);
        
        FeedModel aggregatedFeed = getAggregated(inputFeed, 5);

        assertThat(aggregatedFeed, is(sameInstance(inputFeed)));
    }

    @Test
    public void assertThatAggregatedFeedReturnsAFeedWithLatestUpdatedDate()
    {
        DateTime date0 = new DateTime(UTC);
        DateTime date1 = date0.plusMinutes(30);

        FeedModel inputFeed1 = createSimpleFeed(FEED_URI, FEED_TITLE, FEED_AUTHOR, 1, date0, null, date0);
        FeedModel inputFeed2 = createSimpleFeed(FEED_URI_2, FEED_TITLE_2, FEED_AUTHOR_2, 1, date1, null, date1);

        FeedModel aggregatedFeed = getAggregated(inputFeed1, inputFeed2, 2);

        assertThat(aggregatedFeed.getUpdated(), is(equalTo(some(date1))));
    }

    @Test
    public void assertThatAggregatedFeedReturnsAFeedWithCorrectSelfUriEvenIfOnlyFeedHasDifferentSelfUri()
    {
        FeedModel inputFeed = createSimpleFeed(FEED_URI, FEED_TITLE, FEED_AUTHOR, 2);

        FeedModel aggregatedFeed = getAggregated(inputFeed, 5);

        assertThat(aggregatedFeed, not(sameInstance(inputFeed)));
        assertThat(aggregatedFeed.getUri(), is(equalTo(AGGREGATED_URI)));
        assertThat(Iterables.size(aggregatedFeed.getEntries()), is(equalTo(2)));
    }

    @Test
    public void assertThatAggregatedFeedReturnsAFeedWithAggregatedTitle()
    {
        FeedModel inputFeed1 = createSimpleFeed(FEED_URI, FEED_TITLE, FEED_AUTHOR, 2);
        FeedModel inputFeed2 = createSimpleFeed(FEED_URI_2, FEED_TITLE_2, FEED_AUTHOR_2, 3);

        FeedModel aggregatedFeed = getAggregated(inputFeed1, inputFeed2, 10);

        assertThat(aggregatedFeed, not(sameInstance(inputFeed1)));
        assertThat(aggregatedFeed, not(sameInstance(inputFeed2)));
        assertThat(aggregatedFeed.getUri(), is(equalTo(AGGREGATED_URI)));
    }

    @Test
    public void assertThatAggregatedFeedContainsAggregatedElements()
    {
        FeedModel inputFeed1 = createSimpleFeed(FEED_URI, FEED_TITLE, FEED_AUTHOR, 2);
        FeedModel inputFeed2 = createSimpleFeed(FEED_URI_2, FEED_TITLE_2, FEED_AUTHOR_2, 3);

        FeedModel aggregatedFeed = getAggregated(inputFeed1, inputFeed2, 10);

        assertThat(Iterables.size(aggregatedFeed.getEntries()), is(equalTo(5)));

        assertThat(aggregatedFeed.getEntries(), everyItem(hasSourceFeed()));
    }

    @Test
    public void assertThatAggregatedFeedContainsAggregatedElementsWithOrderAndLimit()
    {
        DateTime date0 = new DateTime(UTC);
        DateTime date1 = incDate(date0, 1);
        DateTime date2 = incDate(date0, 2);
        DateTime date3 = incDate(date0, 3);
        DateTime date4 = incDate(date0, 4);

        FeedModel inputFeed1 = createSimpleFeed(FEED_URI, FEED_TITLE, FEED_AUTHOR, 3, date0);
        FeedModel inputFeed2 = createSimpleFeed(FEED_URI_2, FEED_TITLE_2, FEED_AUTHOR_2, 5, date0);

        // inputFeed1's entries have these dates:  date0, date1, date2
        // inputFeed2's entries have these dates:  date0, date1, date2, date3, date4
        // the aggregated feed, after being sorted in reverse date order and truncated to 5 entries,
        // should have these dates:  date4, date3, date2, date2, date1

        FeedModel aggregatedFeed = getAggregated(inputFeed1, inputFeed2, 5);

        assertThat(Iterables.size(aggregatedFeed.getEntries()), is(equalTo(5)));

        FeedEntry[] entries = Iterables.toArray(aggregatedFeed.getEntries(), FeedEntry.class);
        assertThat(entries[0].getEntryDate(), equalTo(date4));
        assertThat(entries[1].getEntryDate(), equalTo(date3));
        assertThat(entries[2].getEntryDate(), equalTo(date2));
        assertThat(entries[3].getEntryDate(), equalTo(date2));
        assertThat(entries[4].getEntryDate(), equalTo(date1));
    }

    @Test
    public void assertThatAggregatedFeedContainsHeaders()
    {
        FeedHeader header1 = emptyFeedHeader();
        FeedHeader header2 = emptyFeedHeader();
        FeedModel inputFeed1 = createSimpleFeed(FEED_URI, FEED_TITLE, FEED_AUTHOR, 2, new DateTime(UTC), header1);
        FeedModel inputFeed2 = createSimpleFeed(FEED_URI_2, FEED_TITLE_2, FEED_AUTHOR_2, 3, new DateTime(UTC), header2);

        FeedModel aggregatedFeed = getAggregated(inputFeed1, inputFeed2, 10);

        assertThat(Iterables.size(aggregatedFeed.getEntries()), is(equalTo(5)));

        assertThat(aggregatedFeed.getHeaders(), contains(header1, header2));
    }

    @Test
    public void assertThatAggregatedFeedContainsTimedOutApplications()
    {
        FeedHeader header1 = timeoutHeader(ENTRY_APP_NAME);
        FeedHeader header2 = timeoutHeader(ENTRY_APP_NAME_2);
        FeedModel inputFeed1 = createSimpleFeed(FEED_URI, FEED_TITLE, FEED_AUTHOR, 2, new DateTime(UTC), header1);
        FeedModel inputFeed2 = createSimpleFeed(FEED_URI_2, FEED_TITLE_2, FEED_AUTHOR_2, 3, new DateTime(UTC), header2);

        FeedModel aggregatedFeed = getAggregated(inputFeed1, inputFeed2, 10);

        assertThat(Iterables.size(aggregatedFeed.getEntries()), is(equalTo(5)));

        assertThat(aggregatedFeed.getHeaders(), contains(header1, header2));
    }

    private FeedModel createSimpleFeed(Uri feedUri, String title, String author, int entryCount,
                                       DateTime startDate, FeedHeader header, DateTime updated)
    {
        FeedModel.Builder builder = FeedModel.builder(feedUri)
            .title(some(title));
        for (int i = 0; i < entryCount; i++)
        {
            StreamsEntry entry = new StreamsEntry(newEntryParams(startDate), i18nResolver);
            startDate = incDate(startDate, 1);
            builder.addEntries(ImmutableList.of(FeedEntry.fromStreamsEntry(entry)));
        }
        if (header != null)
        {
            builder.addHeaders(ImmutableList.of(header));
        }
        builder.updated(option(updated));
        return builder.build();
    }

    private DateTime incDate(DateTime date, int inc)
    {
        return date.plusSeconds(inc);
    }

    private FeedModel createSimpleFeed(Uri feedUri, String title, String author, int entryCount,
                                       DateTime startDate, FeedHeader header)
    {
        return createSimpleFeed(feedUri, title, author, entryCount, startDate, header, null);
    }

    private FeedModel createSimpleFeed(Uri feedUri, String title, String author, int entryCount, DateTime startDate)
    {
        return createSimpleFeed(feedUri, title, author, entryCount, startDate, null);
    }

    private FeedModel createSimpleFeed(Uri feedUri, String title, String author, int entryCount)
    {
        return createSimpleFeed(feedUri, title, author, entryCount, new DateTime(UTC));
    }

    private FeedModel getAggregated(FeedModel f1, int maxResults)
    {
        Either<Error, FeedModel> ef1 = right(f1);
        return aggregator.aggregate(ImmutableList.of(ef1), AGGREGATED_URI, maxResults, Option.none(String.class) );
    }

    private FeedModel getAggregated(FeedModel f1, FeedModel f2, int maxResults)
    {
        Either<Error, FeedModel> ef1 = right(f1);
        Either<Error, FeedModel> ef2 = right(f2);
        return aggregator.aggregate(ImmutableList.of(ef1, ef2), AGGREGATED_URI, maxResults, Option.none(String.class) );
    }

    public static Matcher<FeedEntry> hasSourceFeed()
    {
        return new HasSourceFeed();
    }

    private static final class HasSourceFeed extends TypeSafeDiagnosingMatcher<FeedEntry>
    {
        @Override
        protected boolean matchesSafely(FeedEntry item, Description mismatchDescription)
        {
            if (! item.getSourceFeed().isDefined())
            {
                mismatchDescription.appendText("no source feed");
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("has a source feed");
        }
    }

    public static Matcher<? super FeedEntry> hasEntryDate(DateTime date)
    {
        return new HasEntryDate(is(equalTo(date)));
    }

    private static final class HasEntryDate extends TypeSafeDiagnosingMatcher<FeedEntry>
    {
        private final Matcher<? super DateTime> matcher;

        public HasEntryDate(Matcher<? super DateTime> matcher)
        {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(FeedEntry item, Description mismatchDescription)
        {
            if (! matcher.matches(item.getEntryDate()))
            {
                mismatchDescription.appendText("entry date '" + item.getEntryDate() + "'");
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("entry date " + matcher);
        }
    }
}
