package com.atlassian.streams.internal;

import com.atlassian.applinks.api.ApplicationLink;
import com.google.common.base.Predicate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppLinksActivityProvidersTest
{
    @Mock
    private ApplicationLink optInLink;
    @Mock
    private ApplicationLink optOutLink;
    private Predicate<? super ApplicationLink> predicate = AppLinksActivityProviders.removeOptOuts();

    @Before
    public void setUp()
    {
        when(optOutLink.getProperty("IS_ACTIVITY_ITEM_PROVIDER")).thenReturn("false");
    }

    @Test
    public void assertThatApplicationLinkCanOptOutToActivity()
    {
        assertThat(predicate.apply(optOutLink), is(equalTo(false)));
    }

    @Test
    public void assertThatApplicationLinkIsOptInByDefaultToActivity()
    {
        assertThat(predicate.apply(optInLink), is(equalTo(true)));
    }
}

