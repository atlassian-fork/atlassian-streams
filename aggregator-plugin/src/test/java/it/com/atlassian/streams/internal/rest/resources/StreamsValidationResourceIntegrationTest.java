package it.com.atlassian.streams.internal.rest.resources;

import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.PreferenceMapBuilder.builder;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(StreamsUiTesterRunner.class)
public class StreamsValidationResourceIntegrationTest
{
    @Inject static RestTester rest;

    @Test
    public void assertThatResponseIsBadRequestIfStreamsTitleParameterIsNull()
    {
        ClientResponse response = rest.validatePreferences(builder().title(null).build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsTitleParameterIsBlank()
    {
        ClientResponse response = rest.validatePreferences(builder().title("").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfStreamsHasInvalidUsernameParameter()
    {
        // Following CSTRM-87, we don't validate user names any more, as this leaks account information
        ClientResponse response = rest.validatePreferences(builder().usernames("invalidusername").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsNull()
    {
        ClientResponse response = rest.validatePreferences(builder().maxResults(null).build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsEmpty()
    {
        ClientResponse response = rest.validatePreferences(builder().maxResults("").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsNotANumber()
    {
        ClientResponse response = rest.validatePreferences(builder().maxResults("ten").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsLessThanZero()
    {
        ClientResponse response = rest.validatePreferences(builder().maxResults("-10").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsZero()
    {
        ClientResponse response = rest.validatePreferences(builder().maxResults("0").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsMaxResultsParameterIsGreaterThan100()
    {
        ClientResponse response = rest.validatePreferences(builder().maxResults("101").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndKeysNotDefined()
    {
        ClientResponse response = rest.validatePreferences(builder().build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameDefined()
    {
        ClientResponse response = rest.validatePreferences(builder().usernames("admin").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }
}
