package it.com.atlassian.streams.confluence;

import java.io.IOException;
import java.net.URISyntaxException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.Confluence.MAX_DATE_PAGE;
import static com.atlassian.streams.testing.Confluence.MIN_DATE_PAGE;
import static com.atlassian.streams.testing.Confluence.STRM_1426_PAGE;
import static com.atlassian.streams.testing.Confluence.STRM_1486_BLOG;
import static com.atlassian.streams.testing.Confluence.STRM_769_ADD_PAGE;
import static com.atlassian.streams.testing.Confluence.STRM_769_EDIT_PAGE;
import static com.atlassian.streams.testing.Confluence.STRM_930_PAGE;
import static com.atlassian.streams.testing.Confluence.STRM_975_PAGE;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntryCount;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withSummary;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withUpdatedDate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/create-and-edit.zip")
public class ConfluenceStreamsTest
{
    private static final String STRM_769_PAGE_TITLE = "STRM-769";
    private static final String STRM_930_PAGE_TITLE = "STRM-930";
    private static final String STRM_975_PAGE_TITLE = "STRM-975";
    private static final String STRM_1426_PAGE_TITLE = "STRM-1426";
    private static final String STRM_1486_PAGE_TITLE = "STRM-1486";
    private static final String MIN_DATE_PAGE_TITLE = "My page for testing min date";
    private static final String MAX_DATE_PAGE_TITLE = "My page for testing max date";

    @Inject static FeedClient feedClient;

    /**
     * Tests STRM-769 where ALL activities from a page that was created/edited by
     * the filtered user appear in the feed, even if the revisions shown are not involved
     * with the filtered user.
     */
    @Test
    public void assertThatPagesCreatedByOtherUsersDoNotAppearInFeedForFilteredUserWhenFilteredUserHasEditedThePage()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), user(IS, "betty"),
                updateDate(BETWEEN, STRM_769_ADD_PAGE.minusSeconds(1), STRM_769_EDIT_PAGE.plusSeconds(1)));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
            hasEntry(withTitle(allOf(containsString("betty"),
                                     containsString("edited"),
                                     containsString(STRM_769_PAGE_TITLE)))),
            not(hasEntry(withTitle(allOf(containsString("admin"),
                                         containsString("added"),
                                         containsString(STRM_769_PAGE_TITLE))))))));
    }

    /**
     * Tests STRM-769 where ALL activities from a page that was created/edited by
     * the filtered user appear in the feed, even if the revisions shown are not involved
     * with the filtered user.
     */
    @Test
    public void assertThatPagesEditedByOtherUsersDoNotAppearInFeedForFilteredUserWhenFilteredUserHasCreatedThePage()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), user(IS, "admin"),
                updateDate(BETWEEN, STRM_769_ADD_PAGE.minusSeconds(1), STRM_769_EDIT_PAGE.plusSeconds(1)));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
            not(hasEntry(withTitle(containsString("betty")),
                         withTitle(containsString("edited")),
                         withTitle(containsString(STRM_769_PAGE_TITLE)))),
            hasEntry(withTitle(containsString("admin")),
                     withTitle(containsString("added")),
                     withTitle(containsString(STRM_769_PAGE_TITLE))))));
    }

    @Test
    public void assertThatPageCreatedDoesNotStripSingleQuotes()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), user(IS, "admin"),
                updateDate(BETWEEN, STRM_975_PAGE.minusSeconds(1), STRM_975_PAGE.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("admin"), containsString("added"), containsString(STRM_975_PAGE_TITLE))),
            withContent(containsString("We're creating this page"))));
    }

    @Test
    public void assertThatPageCreatedWithLongContentIsTruncated()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), user(IS, "admin"),
                updateDate(BETWEEN, STRM_1426_PAGE.minusSeconds(1), STRM_1426_PAGE.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("admin"), containsString("added"), containsString(STRM_1426_PAGE_TITLE))),
            withSummary(containsString("Read more"))));
    }

    @Test
    public void assertThatPageCreatedWithTruncatedContentHasLinkToCreatedPage()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), user(IS, "admin"),
                updateDate(BETWEEN, STRM_1426_PAGE.minusSeconds(1), STRM_1426_PAGE.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("admin"), containsString("added"), containsString(STRM_1426_PAGE_TITLE))),
            withSummary(allOf(containsString("a "),
                              containsString("href"),
                              containsString(feedClient.getBaseUrl()),
                              containsString(STRM_1426_PAGE_TITLE)))));
    }

    @Test
    public void assertThatBlogPostCreatedNowHasRecentTimestamp()
    {
        // STRM-1486, workaround for Confluence bug
        DateTime createdAfterDate = STRM_1486_BLOG.minusSeconds(1);

        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), user(IS, "admin"), maxResults(5));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("admin"), containsString("blogged"), containsString(STRM_1486_PAGE_TITLE))),
            withUpdatedDate(greaterThanOrEqualTo(createdAfterDate.toDate()))));
    }

    @Test
    @Ignore("STRM-930 bug still exists")
    public void assertThatPagesCreatedBeforeTimeFilterAppearInFeedButEditedAfterTimeFilterDoNotAppearInFeedForUpdateDateBeforeFilter()
    {
        DateTime inBetween = STRM_930_PAGE.plusSeconds(1);

        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), updateDate(BEFORE, inBetween), maxResults(2));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
            not(hasEntry(withTitle(containsString("admin")),
                         withTitle(containsString("edited")),
                         withTitle(containsString(STRM_930_PAGE_TITLE)))),
            hasEntry(withTitle(containsString("admin")),
                     withTitle(containsString("added")),
                     withTitle(containsString(STRM_930_PAGE_TITLE))))));
    }

    @Test
    public void assertThatPagesEditedAfterTimeFilterAppearInFeedButCreatedBeforeTimeFilterDoNotAppearInFeedForUpdateDateAfterFilter()
    {
        DateTime inBetween = STRM_930_PAGE.plusSeconds(1);

        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), updateDate(AFTER, inBetween), maxResults(5));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
            hasEntry(withTitle(containsString("admin")),
                     withTitle(containsString("edited")),
                     withTitle(containsString(STRM_930_PAGE_TITLE))),
            not(hasEntry(withTitle(containsString("admin")),
                         withTitle(containsString("added")),
                         withTitle(containsString(STRM_930_PAGE_TITLE)))))));
    }

    @Test
    public void assertThatFeedWithMinDateContainsEntriesAfterTheMinDate()
    {
        DateTime dateBeforeCreation = MIN_DATE_PAGE.minusSeconds(1);

        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), updateDate(AFTER, dateBeforeCreation), maxResults(2));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(containsString(MIN_DATE_PAGE_TITLE)),
            withContent(containsString("Test for min date"))));
    }

    @Test
    public void assertThatFeedWithMinDateDoesNotContainEntriesBeforeTheMinDate()
    {
        DateTime dateAfterCreation = MIN_DATE_PAGE.plusSeconds(1);

        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), updateDate(AFTER, dateAfterCreation), maxResults(2));
        assertThat(feed.getEntries(), not(hasEntry(
            withTitle(containsString(MIN_DATE_PAGE_TITLE)),
            withContent(containsString("Test for min date")))));
    }

    @Test
    public void assertThatFeedWithMaxDateContainsEntriesBeforeTheMaxDate()
    {
        DateTime dateAfterCreation = MAX_DATE_PAGE.plusSeconds(1);

        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), updateDate(BEFORE, dateAfterCreation), maxResults(2));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(containsString(MAX_DATE_PAGE_TITLE)),
            withContent(containsString("Test for max date"))));
    }

    @Test
    public void assertThatFeedWithMaxDateDoesNotContainEntriesAfterTheMaxDate()
    {
        DateTime dateBeforeCreation = MAX_DATE_PAGE.minusSeconds(1);

        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), updateDate(BEFORE, dateBeforeCreation), maxResults(2));
        assertThat(feed.getEntries(), not(hasEntry(withTitle(containsString(MAX_DATE_PAGE_TITLE)))));
    }

    @Test
    @Restore("confluence/backups/strm-1611.zip")
    public void assertThatHibernateLazyInitializationExceptionDoesNotOccur() throws IOException, URISyntaxException
    {
        //See STRM-1611 for more information.
        Feed feed = feedClient.getAs("admin", maxResults(150));
        assertThat(feed.getEntries(), hasEntryCount(equalTo(146)));
   }
}