package it.com.atlassian.streams.fisheye;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.FeCruInstanceController;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.PreferenceMapBuilder;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_1;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_2;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_4;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_5;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_6;
import static com.atlassian.streams.testing.FishEye.Data.CHANGESET_7;
import static com.atlassian.streams.testing.FishEye.Data.changeSet1;
import static com.atlassian.streams.testing.FishEye.Data.changeSet2;
import static com.atlassian.streams.testing.FishEye.Data.changeSet6;
import static com.atlassian.streams.testing.FishEye.Data.changeSet7;
import static com.atlassian.streams.testing.FishEye.fisheyeModule;
import static com.atlassian.streams.testing.PreferenceMapBuilder.builder;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasFisheyeActivityFilterOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasFisheyeFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasStandardFilters;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withFilterProviderName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionKey;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionName;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withOptionValueKeys;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasAuthor;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtomGeneratorElement;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withSummary;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(FECRU)
public class FishEyeActivityStreamTest
{
    @Inject static FeedClient feedClient;
    @Inject static RestTester restTester;

    public void assertThatFishEyeActivityStreamContainsTwoChangesets() throws Exception
    {
        // Fisheye by default has two changesets in its test data.  We'll check that these are right.
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local());

        assertThat(feed.getEntries(), hasEntries(changeSet2(), changeSet1()));
    }

    @Test
    public void assertThatAuthorsContainProfilePictureUris()
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), key(IS, "TST"));
        assertThat(feed.getEntries(), allEntries(hasAuthor(withLink(whereRel(equalTo("photo"))))));
    }

    @Test
    public void assertThatFishEyeActivityStreamResultsCanBeLimited() throws Exception
    {
        //make sure all repos are loaded and so we can get the final  entry.
        //Nasty hardcoding of the control port, but then localhost is hardcoded everywhere...
        FeCruInstanceController ic = new FeCruInstanceController(39901);
        ic.reindexRepositories();

        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(), maxResults(1));
        assertThat(feed.getEntries(), hasEntries(changeSet7()));
    }

    @Test
    public void assertThatActivityStreamContainsAtomGeneratorModule()
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local());
        assertThat(feed.getEntries(), allEntries(haveAtomGeneratorElement(containsString(feedClient.getBaseUrl()))));
    }

    @Test
    public void assertThatFeedUpdatedDateIsSameAsFirstEntryPublishedDate()
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local());
        assertThat(feed.getUpdated(), is(equalTo(feed.getEntries().get(0).getPublished())));
    }

    @Test
    public void assertThatAllActivityStreamEntriesContainsAtlassianApplicationElement()
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local());
        assertThat(feed.getEntries(), allEntries(haveAtlassianApplicationElement(containsString("com.atlassian.fisheye"))));
    }

    @Test
    public void assertThatFishEyeActivityStreamEntryContainsDetailedListInSummary() throws Exception
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(),
                updateDate(AFTER, CHANGESET_2.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_2.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(allOf(
                changeSet2(),
                withContent(allOf(
                        containsString("commit-list"),
                        containsString("+1"),
                        containsString("-0"),
                        containsString("Test.java"))))));
    }

    @Test
    @Ignore
    public void assertThatFishEyeActivityStreamEntryWithCommittedFilesMoreThan3ContainsTruncatedListInSummary() throws Exception
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(),
                updateDate(AFTER, CHANGESET_6.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_6.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(allOf(
                changeSet6(),
                withSummary(allOf(
                            allOf(
                                    containsString("commit-list"),
                                    containsString("file0"),
                                    containsString("file1"),
                                    containsString("file2"),
                                    containsString("file3"),
                                    containsString("Added 7 more files")),
                            not(allOf(
                                    containsString("file4"),
                                    containsString("file5"),
                                    containsString("file6"),
                                    containsString("file7"),
                                    containsString("file8"),
                                    containsString("file9"))))))));
    }

    @Test
    public void assertThatFishEyeActivityStreamEntryDoesNotContainEntriesWithZeroModifiedLinesForAddedFiles() throws Exception
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(),
                updateDate(AFTER, CHANGESET_4.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_4.plusSeconds(1)));

        assertThat(feed.getEntries(), not(hasEntries(
            withContent(allOf(containsString("+0"), containsString("-0"))))));
    }

    @Test
    public void assertThatFishEyeActivityStreamEntryDoesNotContainEntriesWithZeroModifiedLinesForRemovedFiles() throws Exception
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(),
                updateDate(AFTER, CHANGESET_5.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_5.plusSeconds(1)));

        assertThat(feed.getEntries(), not(hasEntries(
                withContent(allOf(containsString("+0"), containsString("-0"))))));
    }

    @Test
    public void assertThatFishEyeActivityStreamEntryContainsDetailedListInContent() throws Exception
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(),
                updateDate(BEFORE, CHANGESET_1.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(allOf(ImmutableList.<Matcher<? super Entry>>of(
                changeSet1(),
                withContent(allOf(
                    containsString("commit-list"),
                    containsString("TST/trunk/Test.java"),
                    containsString("+7")))))));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void assertThatFishEyeActivityStreamEntryWithCommittedFilesMoreThan3ContainsDetailedListInContent() throws Exception
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(),
                updateDate(AFTER, CHANGESET_6.minusSeconds(1)),
                updateDate(BEFORE, CHANGESET_6.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(allOf(
                changeSet6(),
                withContent(allOf(
                    containsString("commit-list"),
                    containsString("file1"),
                    containsString("file2"),
                    containsString("file3"),
                    containsString("file4"),
                    containsString("file5"),
                    containsString("file6"),
                    containsString("file7"),
                    containsString("file8"),
                    containsString("file9"),
                    containsString("file0"),
                    not(containsString("Added 7 more files")))))));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void assertThatFishEyeActivityStreamEntryWithHtmlIsEscaped() throws Exception
    {
        Feed feed = feedClient.getAs("admin", fisheyeModule(), local(),
                updateDate(AFTER, CHANGESET_7.minusHours(24)),
                updateDate(BEFORE, CHANGESET_7.plusHours(24)));

        assertThat(feed.getEntries(), hasEntries(allOf(
                changeSet7(),
                withContent(allOf(
                        containsString("commit-list"),
                        containsString("file10"))))));
    }

    @Test
    public void assertThatFisheyeFilterIsAvailable()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasFisheyeFilters());
    }

    @Test
    public void assertThatFisheyeFilterHasFishEyeAsDisplayName()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasFisheyeFilters(withFilterProviderName(equalTo("FishEye"))));
    }

    @Test
    public void assertThatFilterResourceHasKeyFilterNamedProductSpecific()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"), withOptionName("Project/Repository"))));
    }

    @Test
    public void assertThatFilterResourceHasFisheyeRepositoriesAndCrucibleProjectsAsKeyOptionValues()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(withOption(withOptionKey("key"), withOptionValueKeys("CR", "TST"))));
    }

    @Test
    public void assertThatFisheyeFilterHasActivityFilterOption()
    {
        StreamsConfigRepresentation representation = restTester.getStreamsConfigRepresentation();
        assertThat(representation, hasFisheyeActivityFilterOption());
    }

    @Test
    public void assertThatResponseIsBadRequestIfStreamsHasInvalidKeyParameter()
    {
        ClientResponse response = restTester.validatePreferences(builder().keys("INVALIDKEY").build());
        assertThat(response.getStatus(), is(equalTo(SC_BAD_REQUEST)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(PreferenceMapBuilder.builder().keys("TST").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }

    @Test
    public void assertThatResponseIsOkIfAllStreamsParametersAreValidWithUsernameAndKeysDefined()
    {
        ClientResponse response = restTester.validatePreferences(
            PreferenceMapBuilder.builder().usernames("admin").keys("TST").build());
        assertThat(response.getStatus(), is(equalTo(SC_OK)));
    }
}
