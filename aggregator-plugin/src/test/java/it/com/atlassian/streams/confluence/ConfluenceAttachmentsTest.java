package it.com.atlassian.streams.confluence;

import java.io.IOException;
import java.net.URISyntaxException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.Confluence.activities;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/multiple-page-attachment.zip")
public class ConfluenceAttachmentsTest
{
    @Inject static FeedClient feedClient;

    @Test
    public void assertThatAttachmentsWithoutCommentsDoNotHaveCommentBlockQuote()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), activities(IS, pair(file(), post())));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("attached"), containsString("Page with Attachments"))),
                withContent(not(containsString("blockquote")))));
    }

    @Test
    public void assertThatChangingProfilePictureAddsEntry() throws IOException, URISyntaxException
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(2));

        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(containsString("attached a file")),
            withContent(allOf(
                not(containsString("null")),
                containsString("avatar.png"))))));
    }

    @Test
    public void assertThatAttachingMultipleFilesDoesntAffectMaxResults() throws IOException, URISyntaxException
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), local(), maxResults(5));
        assertThat(feed.getEntries(), hasEntry(allOf(
                withTitle(allOf(
                        containsString("Page with Multiple Attachments"),
                        containsString("attached"),
                        containsString("files"))),
                withContent(allOf(ImmutableList.<Matcher<? super String>>of(
                    containsString("UiTestsTesterRunner.java"),
                    containsString("JiraIssuePage.java"),
                    containsString("UiTestGroups.java"),
                    containsString("DashboardToolsMenu.java"),
                    containsString("AddGadgetDialog.java"),
                    containsString("ExtendedDashboardPage.java"),
                    containsString("CreateDashboardPage.java"),
                    containsString("Gadget.java")))))));
        assertThat(feed.getEntries().size(), is(equalTo(5)));
    }
}
