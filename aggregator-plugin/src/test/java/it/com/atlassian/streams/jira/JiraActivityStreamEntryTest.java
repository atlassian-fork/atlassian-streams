package it.com.atlassian.streams.jira;

import java.io.IOException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.Jira.Data.ONE1_COMMENTED;
import static com.atlassian.streams.testing.Jira.Data.ONE1_CREATED;
import static com.atlassian.streams.testing.Jira.Data.commentedOne1;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.Jira.Data.createdOne2;
import static com.atlassian.streams.testing.Jira.Data.createdTwo1;
import static com.atlassian.streams.testing.Jira.JIRA_PROVIDER;
import static com.atlassian.streams.testing.Jira.activities;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasAuthor;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtomGeneratorElement;
import static com.atlassian.streams.testing.matchers.Matchers.haveLink;
import static com.atlassian.streams.testing.matchers.Matchers.whereHref;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withInReplyToElement;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withType;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/base.zip")
public class JiraActivityStreamEntryTest
{
    @Inject static FeedClient client;
    @Inject static RestTester restTester;


    @Test
    public void assertThatFeedUpdatedDateIsSameAsFirstEntryPublishedDate()
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER));
        assertThat(feed.getUpdated(), is(equalTo(feed.getEntries().get(0).getPublished())));
    }

    @Test
    public void assertThatFeedContainsAllEntries() throws Exception
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), commentedOne1(), createdTwo1(), createdOne1()));
    }

    @Test
    public void assertThatActivityStreamContainsAtomGeneratorModule()
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER));
        assertThat(feed.getEntries(), allEntries(haveAtomGeneratorElement(containsString(client.getBaseUrl()))));
    }

    @Test
    public void assertThatActivityStreamCommentEntryHasInReplyTo() throws IOException
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER), updateDate(BETWEEN, ONE1_COMMENTED.minusSeconds(1), ONE1_COMMENTED.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntries(withInReplyToElement(startsWith("urn:uuid"))));
    }

    @Test
    public void assertThatCreatedOne1EntryHasPostVerb() throws IOException
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER), updateDate(BETWEEN, ONE1_CREATED.minusSeconds(1), ONE1_CREATED.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntry(withVerbElement(equalTo(post().iri().toASCIIString()))));
    }

    @Test
    public void assertThatCreateOne1EntryHasIssueActivityObject() throws IOException
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER), updateDate(BETWEEN, ONE1_CREATED.minusSeconds(1), ONE1_CREATED.plusSeconds(1)));

        assertThat(feed.getEntries(),
            hasEntry(withActivityObjectElement(withType(equalTo(issue().iri().toASCIIString())))));
    }

    @Test
    public void assertThatCommentedOne1EntryHasPostVerb() throws IOException
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER), updateDate(BETWEEN, ONE1_COMMENTED.minusSeconds(1), ONE1_COMMENTED.plusSeconds(1)));

        assertThat(feed.getEntries(), hasEntry(withVerbElement(equalTo(post().iri().toASCIIString()))));
    }

    @Test
    public void assertThatCommentedOne1EntryHasCommentActivityObject() throws IOException
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER), updateDate(BETWEEN, ONE1_COMMENTED.minusSeconds(1), ONE1_COMMENTED.plusSeconds(1)));

        assertThat(feed.getEntries(),
            hasEntry(withActivityObjectElement(withType(equalTo(comment().iri().toASCIIString())))));
    }

    @Test
    public void assertThatAllActivityStreamEntriesContainsAtlassianApplicationElement()
    {
        Feed feed = client.get(provider(JIRA_PROVIDER));
        assertThat(feed.getEntries(), allEntries(haveAtlassianApplicationElement(containsString("com.atlassian.jira"))));
    }

    @Test
    public void assertThatAuthorsContainProfilePictureUris()
    {
        Feed feed = client.getAs("admin", provider(JIRA_PROVIDER));
        assertThat(feed.getEntries(), allEntries(hasAuthor(withLink(whereRel(equalTo("photo"))))));
    }

    @Test
    public void assertThatXmlEntitiesInAuthorNameAreProperlyEncoded()
    {
        String feed = client.getAs("admin", String.class, issueKey(IS, "ONE-2"), provider(JIRA_PROVIDER));
        assertThat(feed, containsString("<name>User &lt;></name>")); // abdera's XML escaping only does the less thans
    }

    @Test
    public void assertThatXmlAndHtmlEntitiesInSummaryElementAreProperlyEncoded()
    {
        String feed = client.getAs("admin", String.class, issueKey(IS, "ONE-2"), provider(JIRA_PROVIDER));
        // The summary element's contents need to be XML escaped since they are in an XML feed, just like the rest of
        // the feed element contents. But because the summary element contains HTML, the non-HTML portions of it such as
        // user full name need to be HTML-escaped as well. So we check for "double-escaping" here.
        assertThat(feed, containsString("?name=user\" class=\"activity-item-user activity-item-author\">User &amp;lt;&amp;gt;&lt;/a>"));
    }

    @Test
    public void assertThatCommentEntriesLinkToComment()
    {
        Feed feed = client.getAs("admin", activities(IS, pair(comment(), post())), provider(JIRA_PROVIDER));
        assertThat(feed.getEntries(),
                   allEntries(haveLink(whereHref(allOf(
                           containsString("focusedCommentId"),
                           containsString("&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-"))))));
    }
}
