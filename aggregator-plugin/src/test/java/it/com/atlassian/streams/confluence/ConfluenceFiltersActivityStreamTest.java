package it.com.atlassian.streams.confluence;

import java.io.IOException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.page;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.atlassian.streams.testing.Confluence.CONFLUENCE_PROVIDER;
import static com.atlassian.streams.testing.Confluence.MY_FIRST_BLOG;
import static com.atlassian.streams.testing.Confluence.activities;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntryCount;
import static com.atlassian.streams.testing.matchers.Matchers.hasTarget;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withId;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withType;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/base.zip")
public class ConfluenceFiltersActivityStreamTest
{
    @Inject static FeedClient feedClient;

    @Test
    public void assertThatFeedWithAuthorFilterOnlyContainsEntriesFromThatAuthor() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), user(IS, "admin"));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("admin"))));
    }

    @Test
    public void assertThatFeedWithNottedAuthorFilterDoesntContainEntriesFromThatAuthor() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), user(NOT, "admin"));
        assertThat(feed.getEntries(), not(allEntries(withTitle(containsString("admin")))));
    }

    @Test
    public void assertThatFilteringNottedAuthorsIsCompletedBeforePaging() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), user(NOT, "admin"), maxResults(1));
        assertThat(feed.getEntries(), hasEntryCount(equalTo(1)));
    }

    @Test
    public void assertThatFilteringIsAuthorIsCompletedBeforePaging() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), user(IS, "jnolen"), maxResults(1));
        assertThat(feed.getEntries(), hasEntryCount(equalTo(1)));
    }

    @Test
    public void assertThatFeedWithSpaceFilterOnlyContainsEntriesFromThatSpace() throws Exception
    {
        /* This search includes multiple versions of a "newspace" page, and only the most recent has links
           in the form of: http://localhost:3990/streams/pages/viewpage.action?pageId=1277982
           The others have: http://localhost:3990/streams/display/newspace/Home so we need to search for "New Space" too.
         */
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), key(IS, "newspace"));
        assertThat(feed.getEntries(), allEntries(anyOf(
                withTitle(containsString("newspace")),
                withContent(containsString("New Space")),
                hasTarget(withId(containsString("newspace"))))));
    }

    @Test
    public void assertThatFeedWithNottedSpaceFilterDoesntContainEntriesFromThatSpace() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), key(NOT, "newspace"));
        assertThat(feed.getEntries(), not(allEntries(anyOf(withTitle(containsString("newspace")), withContent(containsString("New Space"))))));
    }

    @Test
    public void assertThatFeedWithDateBeforeAndAfterParametersFindsEntriesInMiddle() throws IOException
    {
        //entry will exist twice due to confluence 4.0 migration's revision
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), updateDate(BEFORE, MY_FIRST_BLOG.plusSeconds(1)), updateDate(AFTER, MY_FIRST_BLOG.minusSeconds(1)));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("My First Blog"))));
    }

    @Test
    public void assertThatFeedWithDateRangeParametersFindsEntriesInMiddle() throws IOException
    {
        //entry will exist twice due to confluence 4.0 migration's revision
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), updateDate(BETWEEN, MY_FIRST_BLOG.minusSeconds(1), MY_FIRST_BLOG.plusSeconds(1)));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("My First Blog"))));
    }

    @Test
    public void assertThatFeedWithActivityFilterIsOnlyContainsEntriesMatchingThatActivity() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(page(), post())));
        assertThat(feed.getEntries(),
            allEntries(allOf(
                withVerbElement(equalTo(post().iri().toASCIIString())),
                withActivityObjectElement(withType(equalTo(page().iri().toASCIIString()))))));
    }

    @Test
    public void assertThatFeedWithFilterNotVerbParameterOnlyContainsEntriesNotMatchingThatVerb() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(NOT, pair(page(), post())));
        assertThat(feed.getEntries(),
            allEntries(not(allOf(
                withVerbElement(equalTo(post().iri().toASCIIString())),
                withActivityObjectElement(withType(equalTo(page().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFeedWithTwoActivityFiltersContainsEntriesMatchingEitherActivity() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(page(), post()), pair(page(), update())));
        assertThat(feed.getEntries(), allEntries(anyOf(
                allOf(
                    withVerbElement(equalTo(post().iri().toASCIIString())),
                    withActivityObjectElement(withType(equalTo(page().iri().toASCIIString())))),
                allOf(
                    withVerbElement(equalTo(update().iri().toASCIIString())),
                    withActivityObjectElement(withType(equalTo(page().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatFeedWithSpaceActivityFilterIsOnlyContainsSpacesAndNotPersonalSpaces() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(space(), post())));
        assertThat(feed.getEntries(),
            allEntries(allOf(
                withVerbElement(equalTo(post().iri().toASCIIString())),
                withActivityObjectElement(withType(equalTo(space().iri().toASCIIString()))))));
    }

    @Test
    public void assertThatFeedWithPersonalSpaceActivityFilterIsOnlyContainsPersonalSpacesAndNotOtherSpaces() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(personalSpace(), post())));
        assertThat(feed.getEntries(),
            allEntries(allOf(
                withVerbElement(equalTo(post().iri().toASCIIString())),
                withActivityObjectElement(withType(equalTo(personalSpace().iri().toASCIIString()))))));
    }

    @Test
    public void assertThatFeedWithActivityFilterIsSpaceCreatedOnlyContainsEntriesMatchingThatActivity() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(space(), post())));
        assertThat(feed.getEntries(), allEntries(withTitle(allOf(containsString("space"), anyOf(containsString("created"), containsString("added"))))));
    }

    @Test
    public void assertThatFeedWithActivityFilterIsSpaceEditedOnlyContainsEntriesMatchingThatActivity() throws Exception
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(space(), update())));
        assertThat(feed.getEntries(), allEntries(withTitle(allOf(containsString("space"), containsString("edited")))));
    }

    @Test
    public void assertThatFeedWithActivityFilterIsCommentPostedOnlyContainsEntriesMatchingThatActivity()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), activities(IS, pair(comment(), post())));
        assertThat(feed.getEntries(), allEntries(withTitle(containsString("commented on"))));
    }

    @Test
    public void assertThatFeedWithIssueKeyIncludesContentMatchingValue()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), issueKey(IS, "qqwweerrttyy"));
        assertThat(feed.getEntries(), hasEntry(withTitle(containsString("My First Blog"))));
    }

    @Test
    public void assertThatFeedWithMultipleIssueKeysIncludesContentMatchingAnyValue()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), issueKey(IS, "first", "qqwweerrttyy"));
        assertThat(feed.getEntries(), allEntries(anyOf(
                withTitle(containsString("My First Blog")),
                withTitle(containsString("Re-ordering pages")),
                withTitle(containsString("Creating pages and linking")))));
    }

    @Test
    public void assertThatFeedWithNotIssueKeyDoesNotIncludeContentMatchingThatValue()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), issueKey(NOT, "qqwweerrttyy"));
        assertThat(feed.getEntries(), not(hasEntry(withTitle(containsString("My First Blog")))));
    }

    @Test
    public void assertThatIssueKeyFilterAndUsernameFilterCanCoexist()
    {
        Feed feed = feedClient.getAs("admin", provider(CONFLUENCE_PROVIDER), issueKey(IS, "first"), user(IS, "admin"));
        assertThat(feed.getEntries(), allOf(
            hasEntry(withTitle(containsString("My First Blog"))),
            not(hasEntry(withTitle(containsString("Re-ordering pages")))),
            not(hasEntry(withTitle(containsString("Creating pages and linking"))))));
    }
}
