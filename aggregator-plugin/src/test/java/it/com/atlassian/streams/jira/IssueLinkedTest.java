package it.com.atlassian.streams.jira;

import java.io.IOException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.jira.JiraStreamsActivityProvider.ISSUE_VOTE_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.REPLY_TO_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.WATCH_LINK_REL;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withLink;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/issue-linked.xml")
public class IssueLinkedTest
{
    @Inject static FeedClient client;

    @Test
    public void assertThatLinkedIssuesHasAggregatedLinkTitle() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(
                containsString("admin"),
                containsString("linked 2 issues")))));
    }

    @Test
    public void assertThatLinkedIssuesHasBothIssueKeysInContent() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withContent(allOf(
                containsString("ONE-1"),
                containsString("is blocked by"),
                containsString("TWO-1")))));
    }

    @Test
    public void assertThatThreeLinkedIssuesHasCorrectTitle() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(
                containsString("admin"),
                containsString("linked 3 issues")))));
    }

    @Test
    public void assertThatThreeLinkedIssuesHasAllIssueKeysInContent() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withContent(allOf(
                containsString("ONE-3"),
                containsString("is duplicated by"),
                containsString("TWO-1"),
                containsString("and"),
                containsString("ONE-2")))));
    }

    @Test
    public void assertThatRemovedIssueLinkHasCorrectTitle() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(
                containsString("admin"),
                containsString("removed the Link between"),
                containsString("ONE-3"),
                containsString("and"),
                containsString("TWO-1")))));
    }

    @Test
    public void assertThatLinkedIssuesHasIssueSummary() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withContent(allOf(
                containsString("ONE-1 - Issue One"),
                containsString("is blocked by"),
                containsString("TWO-1 - Issue Two")))));
    }

    @Test
    public void assertThatLinkedIssuesWithCommentShowsComment() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withTitle(containsString("linked 2 issues")),
            withContent(allOf(containsString("ONE-3 - Wow, another bug!"),
                containsString("is blocked by"),
                containsString("TWO-1 - Issue Two"),
                containsString("oh bullocks!")))));
    }

    @Test
    public void assertThatLinkedIssuesDoNotHaveReplyToLink() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withContent(allOf(
                containsString("ONE-1 - Issue One"),
                containsString("is blocked by"),
                containsString("TWO-1 - Issue Two"))),
            not(withLink(whereRel(is(equalTo(REPLY_TO_LINK_REL)))))));
    }

    @Test
    public void assertThatLinkedIssuesDoNotHaveWatchLink() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withContent(allOf(
                containsString("ONE-1 - Issue One"),
                containsString("is blocked by"),
                containsString("TWO-1 - Issue Two"))),
            not(withLink(whereRel(is(equalTo(WATCH_LINK_REL)))))));
    }

    @Test
    public void assertThatLinkedIssuesDoNotHaveVoteLink() throws IOException
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(
            withContent(allOf(
                containsString("ONE-1 - Issue One"),
                containsString("is blocked by"),
                containsString("TWO-1 - Issue Two"))),
            not(withLink(whereRel(is(equalTo(ISSUE_VOTE_REL)))))));
    }
}
