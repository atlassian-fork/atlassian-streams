package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/summary-edited.xml")
public class SummaryEditedTest
{
    @Inject static FeedClient client;

    @Test
    public void assertThatFeedContainsFieldUpdateEntryWhenFieldsIsUpdated() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("changed the Summary of"),
                            containsString("ONE-1"),
                            containsString("to 'new summary'")))));
    }

    @Test
    public void assertThatCreatedEntryIssueSummaryTitleDoesNotChangeWhenSummaryHasBeenUpdated() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("created"),
                            containsString("ONE-1"),
                            containsString("Issue One")))));
    }

    @Test
    public void assertThatEditedSummaryEntryDoesNotShowSummaryTwice() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("changed the Summary of"),
                            containsString("ONE-1"),
                            containsString("to 'new summary'"))),
            not(withTitle(containsString("- new summary")))));
    }

    @Test
    public void assertThatOldIssueSummaryIsUsedInCommentEntryBeforeSummaryUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("commented"),
                            containsString("ONE-1"),
                            containsString("Issue One"))),
            withContent(containsString("This is a comment."))));
    }

    @Test
    public void assertThatLatestIssueSummaryIsUsedInCommentEntryAfterTheLastSummaryUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("commented"),
                            containsString("ONE-1"),
                            containsString("supercalifragilisticexpialidocious"))),
            withContent(containsString("this should be the last summary update"))));
    }

    @Test
    public void assertThatLatestIssueSummaryIsUsedInIssueTransitionEntryAfterTheLastSummaryUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("stopped progress"),
                            containsString("ONE-1"),
                            containsString("supercalifragilisticexpialidocious")))));
    }

    @Test
    public void assertThatLatestIssueSummaryIsUsedInIssueUpdateEntryAfterTheLastSummaryUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("changed the Issue Type"),
                            containsString("ONE-1"),
                            containsString("supercalifragilisticexpialidocious")))));
    }

    @Test
    public void assertThatNewIssueSummaryDoesNotChangeInCommentEntryAfterFirstSummaryUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("commented"),
                            containsString("ONE-1"),
                            containsString("new summary"))),
            withContent(containsString("why did you change the summary?"))));
    }

    @Test
    public void assertThatNewIssueSummaryDoesNotChangeInIssueTransitionEntryAfterFirstSummaryUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("started progress"),
                            containsString("ONE-1"),
                            containsString("new summary")))));
    }

    @Test
    public void assertThatNewIssueSummaryDoesNotChangeInIssueUpdateEntryAfterFirstSummaryUpdate() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("changed the Priority"),
                            containsString("ONE-1"),
                            containsString("new summary")))));
    }
}
