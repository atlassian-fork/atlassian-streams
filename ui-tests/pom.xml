<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.streams</groupId>
        <artifactId>streams-parent</artifactId>
        <version>8.2.1-SNAPSHOT</version>
    </parent>

    <artifactId>streams-ui-tests</artifactId>
    <packaging>atlassian-plugin</packaging>

    <name>Streams Ui Tests</name>

    <properties>
        <!-- make sure we set the order of the test groups to run with applinks last, otherwise it tends to run first
             which is silly cause it shouldn't pass if the others don't -->
        <testGroups>jira,confluence,multi</testGroups>
        <jira.data.base>${basedir}/../aggregator-plugin/src/test/resources/jira/test-resources/base</jira.data.base>
        <jira.data.applinks.oauth>${basedir}/../aggregator-plugin/src/test/resources/jira/test-resources/jira-applinks-3lo.zip</jira.data.applinks.oauth>
        <confluence.data.base>${basedir}/../aggregator-plugin/src/test/resources/confluence/test-resources/base.zip</confluence.data.base>
        <confluence.data.applinks.oauth>${basedir}/../aggregator-plugin/src/test/resources/confluence/test-resources/confluence-applinks-3lo.zip</confluence.data.applinks.oauth>
        <jira.testkit.plug.version>8.1.11</jira.testkit.plug.version>
        <sonar.coverage.jacoco.xmlReportPaths>${project.basedir}/../${jacoco.report.file}</sonar.coverage.jacoco.xmlReportPaths>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-api</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-spi</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-jira-plugin</artifactId>
            <type>atlassian-plugin</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-jira-inline-actions-plugin</artifactId>
            <type>atlassian-plugin</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-confluence-plugin</artifactId>
            <type>atlassian-plugin</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-confluence-inline-actions-plugin</artifactId>
            <type>atlassian-plugin</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-aggregator-plugin</artifactId>
            <type>atlassian-plugin</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-core-plugin</artifactId>
            <type>atlassian-plugin</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-inline-actions-plugin</artifactId>
            <type>atlassian-plugin</type>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.inject</groupId>
            <artifactId>guice</artifactId>
            <version>3.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- test dependencies -->
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-testing</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.integrationtesting</groupId>
            <artifactId>atlassian-integrationtesting-lib</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>net.sourceforge.htmlunit</groupId>
                    <artifactId>htmlunit</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>be.roam.hue</groupId>
                    <artifactId>hue</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>atlassian-jira-pageobjects</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.selenium</groupId>
            <artifactId>atlassian-webdriver-confluence</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.streams</groupId>
            <artifactId>streams-pageobjects</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.selenium</groupId>
            <artifactId>atlassian-pageobjects-api</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>amps-maven-plugin</artifactId>
                <version>8.1.0-5b23c07</version>
                <configuration>
                    <server>${server}</server>
                    <contextPath>${contextPath}</contextPath>
                    <products>
                        <product>
                            <id>jira</id>
                            <version>${jira.test.version}</version>
                            <httpPort>${httpPort}</httpPort>
                            <ajpPort>${jira.ajpPort}</ajpPort>
                            <output>${project.build.directory}/jira-${jira.test.version}.log</output>
                            <productDataPath>${jira.data.base}</productDataPath>
                            <jvmArgs>-Datlassian.darkfeature.jira.onboarding.feature.disabled=true</jvmArgs>
                            <!-- We need to use bundledArtifacts here to make AMPS replace the older
                                 streams-jira-plugin.  This also means that we have to reproduce all the bundled
                                 artifact entries from above because AMPS does not aggregate them. -->
                            <bundledArtifacts>
                                <bundledArtifact>
                                    <groupId>com.atlassian.jira.tests</groupId>
                                    <artifactId>jira-testkit-plugin</artifactId>
                                    <version>${jira.testkit.plug.version}</version>
                                </bundledArtifact>
                                <pluginArtifact>
                                    <groupId>com.atlassian.jira</groupId>
                                    <artifactId>jira-func-test-plugin</artifactId>
                                    <version>${jira.version}</version>
                                </pluginArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-aggregator-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-core-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-jira-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-api</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-spi</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.integrationtesting</groupId>
                                    <artifactId>atlassian-integrationtesting-plugin</artifactId>
                                    <version>${ait.version}</version>
                                </bundledArtifact>
                                <pluginArtifact>
                                    <groupId>com.atlassian.integrationtesting</groupId>
                                    <artifactId>atlassian-integrationtesting-jira-6.3-plugin</artifactId>
                                    <version>${ait.version}</version>
                                </pluginArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-js-tests-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-jira-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                            </bundledArtifacts>
                        </product>
                        <product>
                            <id>jira</id>
                            <instanceId>jira-applinks-oauth</instanceId>
                            <version>${jira.test.version}</version>
                            <httpPort>${httpPort.jira.applinks}</httpPort>
                            <ajpPort>${jira.ajpPort}</ajpPort>
                            <output>${project.build.directory}/jira-${jira.test.version}.log</output>
                            <productDataPath>${jira.data.applinks.oauth}</productDataPath>
                            <!-- We need to use bundledArtifacts here to make AMPS replace the older
                                 streams-jira-plugin.  This also means that we have to reproduce all the bundled
                                 artifact entries from above because AMPS does not aggregate them. -->
                            <bundledArtifacts>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-aggregator-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-core-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-jira-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-api</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-spi</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.integrationtesting</groupId>
                                    <artifactId>atlassian-integrationtesting-plugin</artifactId>
                                    <version>${ait.version}</version>
                                </bundledArtifact>
                                <pluginArtifact>
                                    <groupId>com.atlassian.integrationtesting</groupId>
                                    <artifactId>atlassian-integrationtesting-jira-6.3-plugin</artifactId>
                                    <version>${ait.version}</version>
                                </pluginArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-js-tests-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-jira-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                            </bundledArtifacts>
                        </product>
                        <product>
                            <id>confluence</id>
                            <version>${confluence.test.version}</version>
                            <httpPort>${httpPort}</httpPort>
                            <ajpPort>${confluence.ajpPort}</ajpPort>
                            <output>${project.build.directory}/confluence-${confluence.test.version}.log</output>
                            <productDataPath>${confluence.data.base}</productDataPath>
                            <jvmArgs>${confluence.jvm.args}</jvmArgs>
                            <!-- We need to use bundledArtifacts here to make AMPS replace the older
                                 streams-confluence-plugin.  This also means that we have to reproduce all the bundled
                                 artifact entries from above because AMPS does not aggregate them. -->
                            <bundledArtifacts>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-aggregator-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-core-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-confluence-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-api</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-spi</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.integrationtesting</groupId>
                                    <artifactId>atlassian-integrationtesting-plugin</artifactId>
                                    <version>${ait.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-js-tests-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-confluence-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                            </bundledArtifacts>
                        </product>
                        <product>
                            <id>confluence</id>
                            <instanceId>confluence-applinks-oauth</instanceId>
                            <version>${confluence.test.version}</version>
                            <httpPort>${httpPort.confluence.applinks}</httpPort>
                            <ajpPort>${confluence.ajpPort}</ajpPort>
                            <output>${project.build.directory}/confluence-${confluence.test.version}.log</output>
                            <productDataPath>${confluence.data.applinks.oauth}</productDataPath>
                            <jvmArgs>${confluence.jvm.args}</jvmArgs>
                            <!-- We need to use bundledArtifacts here to make AMPS replace the older
                                 streams-confluence-plugin.  This also means that we have to reproduce all the bundled
                                 artifact entries from above because AMPS does not aggregate them. -->
                            <bundledArtifacts>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-aggregator-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-core-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-confluence-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-api</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-spi</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.integrationtesting</groupId>
                                    <artifactId>atlassian-integrationtesting-plugin</artifactId>
                                    <version>${ait.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-js-tests-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-confluence-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                                <bundledArtifact>
                                    <groupId>com.atlassian.streams</groupId>
                                    <artifactId>streams-inline-actions-plugin</artifactId>
                                    <version>${project.version}</version>
                                </bundledArtifact>
                            </bundledArtifacts>
                        </product>
                        <product>
                            <id>bamboo</id>
                            <instanceId>bamboo-applinks-oauth</instanceId>
                            <version>${bamboo.test.version}</version>
                            <httpPort>${httpPort.bamboo.applinks}</httpPort>
                            <ajpPort>${bamboo.ajpPort}</ajpPort>
                            <productDataPath>${basedir}/../aggregator-plugin/src/test/resources/bamboo/test-resources/bamboo-applinks-3lo.zip</productDataPath>
                            <output>${project.build.directory}/bamboo-applinks-oauth-${bamboo.test.version}.log</output>
                            <systemPropertyVariables>
                                <plugin.root.directories>${basedir}/../streams-api,${basedir}/../spi</plugin.root.directories>
                                <plugin.resource.directories>${basedir}/../streams-thirdparty-plugin/src/main/resources,${basedir}/../core-plugin/src/main/resources,${basedir}/src/main/resources,${basedir}/../streams-bamboo-plugin/src/main/resources</plugin.resource.directories>
                                <atlassian.dev.mode>true</atlassian.dev.mode>
                                <java.awt.headless>true</java.awt.headless>
                            </systemPropertyVariables>
                            <!-- TODO: Re-enable these setting when bamboo has upgraded to a version supporting Platform 5.0.x-->
<!--                            <libArtifacts>-->
<!--                                <libArtifact>-->
<!--                                    <groupId>org.apache.commons</groupId>-->
<!--                                    <artifactId>commons-lang3</artifactId>-->
<!--                                </libArtifact>-->
<!--                            </libArtifacts>-->
<!--                            <bundledArtifacts>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>com.atlassian.streams</groupId>-->
<!--                                    <artifactId>streams-bamboo-plugin</artifactId>-->
<!--                                    <version>${project.version}</version>-->
<!--                                </bundledArtifact>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>${project.groupId}</groupId>-->
<!--                                    <artifactId>streams-api</artifactId>-->
<!--                                    <version>${project.version}</version>-->
<!--                                </bundledArtifact>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>${project.groupId}</groupId>-->
<!--                                    <artifactId>streams-spi</artifactId>-->
<!--                                    <version>${project.version}</version>-->
<!--                                </bundledArtifact>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>${project.groupId}</groupId>-->
<!--                                    <artifactId>streams-core-plugin</artifactId>-->
<!--                                    <version>${project.version}</version>-->
<!--                                </bundledArtifact>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>com.atlassian.integrationtesting</groupId>-->
<!--                                    <artifactId>atlassian-integrationtesting-plugin</artifactId>-->
<!--                                    <version>${ait.version}</version>-->
<!--                                </bundledArtifact>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>${project.groupId}</groupId>-->
<!--                                    <artifactId>streams-js-tests-plugin</artifactId>-->
<!--                                    <version>${project.version}</version>-->
<!--                                </bundledArtifact>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>io.atlassian.util.concurrent</groupId>-->
<!--                                    <artifactId>atlassian-util-concurrent</artifactId>-->
<!--                                </bundledArtifact>-->
<!--                                <bundledArtifact>-->
<!--                                    <groupId>com.atlassian.streams</groupId>-->
<!--                                    <artifactId>streams-thirdparty-plugin</artifactId>-->
<!--                                    <version>${project.version}</version>-->
<!--                                </bundledArtifact>-->
<!--                            </bundledArtifacts>-->
                        </product>
                        <product>
                            <!-- Use the refapp since it's the fastest to start up. -->
                            <id>refapp</id>
                        </product>
                    </products>
                    <testGroups>
                        <testGroup>
                            <id>bamboo</id>
                            <productIds>
                                <productId>refapp</productId>
                            </productIds>
                            <excludes>
                                <exclude>**/*</exclude>
                            </excludes>
                        </testGroup>
                        <testGroup>
                            <id>confluence</id>
                            <productIds>
                                <productId>confluence</productId>
                            </productIds>
                            <includes>
                                <include>it/**</include>
                            </includes>
                        </testGroup>
                        <testGroup>
                            <id>fecru</id>
                            <productIds>
                                <productId>refapp</productId>
                            </productIds>
                            <excludes>
                                <exclude>**/*</exclude>
                            </excludes>
                        </testGroup>
                        <testGroup>
                            <id>jira</id>
                            <productIds>
                                <productId>jira</productId>
                            </productIds>
                            <includes>
                                <include>it/**</include>
                            </includes>
                        </testGroup>
                        <testGroup>
                            <id>refapp</id>
                            <productIds>
                                <productId>refapp</productId>
                            </productIds>
                            <excludes>
                                <exclude>**/*</exclude>
                            </excludes>
                        </testGroup>
                        <testGroup>
                            <id>applinks</id>
                            <productIds>
                                <productId>refapp</productId>
                            </productIds>
                            <excludes>
                                <exclude>**/*</exclude>
                            </excludes>
                        </testGroup>
                        <testGroup>
                            <id>multi</id>
                            <productIds>
                                <productId>jira-applinks-oauth</productId>
                                <productId>confluence-applinks-oauth</productId>
                                <productId>bamboo-applinks-oauth</productId>
                            </productIds>
                            <includes>
                                <include>it/**</include>
                            </includes>
                        </testGroup>
                        <!--  dummy test group to allow for successful StAC builds -->
                        <testGroup>
                            <id>applinks-streams3</id>
                            <productIds>
                                <productId>refapp</productId>
                            </productIds>
                            <excludes>
                                <exclude>**/*</exclude>
                            </excludes>
                        </testGroup>
                    </testGroups>
                    <systemPropertyVariables>
                        <xvfb.enable>${xvfb.enable}</xvfb.enable>
                        <webdriver.browser>${webdriver.browser}</webdriver.browser>
                    </systemPropertyVariables>
                    <skipTests>${skip.ui.tests}</skipTests>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
