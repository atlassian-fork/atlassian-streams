package it.com.atlassian.streams.uitests;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.streams.pageobjects.ActivityItem;
import com.atlassian.streams.pageobjects.ActivityItemInlineAction;
import com.atlassian.streams.pageobjects.ActivityStreamGadget;
import com.atlassian.streams.testing.UiTestsTesterRunner;
import com.atlassian.streams.testing.pageobjects.confluence.ConfluencePage;
import com.atlassian.streams.testing.pageobjects.confluence.InsertMacroDialog;
import com.atlassian.streams.testing.pageobjects.confluence.StreamsConfluenceTestedProduct;
import com.atlassian.webdriver.confluence.ConfluenceTestedProduct;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.pageobjects.Predicates.summaryContains;
import static com.atlassian.streams.testing.UiTestGroups.CONFLUENCE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(UiTestsTesterRunner.class)
@TestGroups(CONFLUENCE)
@Ignore //this has been deprecated in confluence, hope they'll change their mind.
public class ConfluenceInlineActionsTest
{
    private static final String STREAM_PAGE_URL = "/display/~admin/Home";
    private static final String WATCH = "Watch";

    private static ConfluenceTestedProduct product = (ConfluenceTestedProduct)TestedProductFactory.create(ConfluenceTestedProduct.class.getCanonicalName());

    private static StreamsConfluenceTestedProduct streamsProduct = new StreamsConfluenceTestedProduct(product);

    private ConfluencePage pageUnderTest;
    private ActivityStreamGadget gadget;

    @BeforeClass
    public static void restoreAndAddStreamToPage()
    {
        streamsProduct.restore("confluence/backups/base.zip");

        ConfluencePage page = product.visit(ConfluencePage.class, URI.create(STREAM_PAGE_URL));
        InsertMacroDialog macroDialog = page.openEditPage().openMacroDialog();
        macroDialog.add("Activity Stream")
            .getGadget()
            .viewAs(ActivityStreamGadget.class)
            .openConfiguration()
            .setNumEntries(99)
            .save()
            .close();
        macroDialog.finished()
            .save();
    }

    @Before
    public void reloadStreamPage()
    {
        pageUnderTest = product.visit(ConfluencePage.class, URI.create(STREAM_PAGE_URL));
        gadget = pageUnderTest.getGadgetWithTitle("Activity Stream").viewAs(ActivityStreamGadget.class);
    }

    @Test
    public void assertThatWatchInlineActionIsAvailableForPages()
    {
        assertThat(getEditedPage().getInlineAction(WATCH), is(not(none(ActivityItemInlineAction.class))));
    }

    @Test
    public void assertThatWatchInlineActionIsAvailableForSpaces()
    {
        assertThat(getAddedSpace().getInlineAction(WATCH), is(not(none(ActivityItemInlineAction.class))));
    }

    @Test
    public void assertThatWatchInlineActionIsAvailableForPersonalSpaces()
    {
        assertThat(getAddedPersonalSpace().getInlineAction(WATCH), is(not(none(ActivityItemInlineAction.class))));
    }

    @Test
    public void assertThatWatchInlineActionIsNotAvailableIfAlreadyWatchingPage()
    {
        try
        {
            getEditedPage().getInlineAction(WATCH).get().clickAndWaitForStatusBar();
            reloadStreamPage();

            assertThat(getEditedPage().getInlineAction(WATCH), is(none(ActivityItemInlineAction.class)));
        }
        finally
        {
            reloadStreamPage();
            getEditedPage().followAlternateLink(ConfluencePage.class).unwatch();
        }
    }

    @Test
    public void assertThatWatchInlineActionIsNotAvailableIfAlreadyWatchingSpace()
    {
        try
        {
            getAddedSpace().getInlineAction(WATCH).get().clickAndWaitForStatusBar();
            reloadStreamPage();

            assertThat(getAddedSpace().getInlineAction(WATCH), is(none(ActivityItemInlineAction.class)));
        }
        finally
        {
            reloadStreamPage();
            getAddedSpace().followAlternateLink(ConfluencePage.class).unwatchSpace();
        }
    }

    @Test
    public void assertThatWatchInlineActionRegistersUserAsPageWatcher()
    {
        try
        {
            ActivityItem item = getEditedPage();
            item.getInlineAction(WATCH).get().clickAndWaitForStatusBar();
            assertThat(item.followAlternateLink(ConfluencePage.class).isWatching(), is(true));
        }
        finally
        {
            reloadStreamPage();
            getEditedPage().followAlternateLink(ConfluencePage.class).unwatch();
        }
    }

    @Test
    public void assertThatWatchInlineActionRegistersUserAsSpaceWatcher()
    {
        try
        {
            ActivityItem item = getAddedSpace();
            item.getInlineAction(WATCH).get().clickAndWaitForStatusBar();
            assertThat(item.followAlternateLink(ConfluencePage.class).isWatchingSpace(), is(true));
        }
        finally
        {
            reloadStreamPage();
            getAddedSpace().followAlternateLink(ConfluencePage.class).unwatchSpace();
        }
    }

    // https://jira.atlassian.com/browse/CONF-22558 STRM's inline action only checks the most recent revision for page
    // watch status, and as a result, because of the linked bug, we need to watch/unwatch the latest revision.
    private ActivityItem getEditedPage()
    {
        return gadget.getActivityItemWhere(summaryContains("edited", "Page with Attachments")).get();
    }

    private ActivityItem getAddedSpace()
    {
        return gadget.getActivityItemWhere(summaryContains("added space", "My New Space")).get();
    }

    private ActivityItem getAddedPersonalSpace()
    {
        return gadget.getActivityItemWhere(summaryContains("created their", "personal space")).get();
    }
}
