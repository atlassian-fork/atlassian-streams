package it.com.atlassian.streams.uitests;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.pageobjects.ActivityItem;
import com.atlassian.streams.pageobjects.ActivityItemInlineAction;
import com.atlassian.streams.pageobjects.ActivityStreamGadget;
import com.atlassian.streams.pageobjects.InlineCommentForm;
import com.atlassian.streams.testing.UiTestsTesterRunner;
import com.atlassian.streams.testing.pageobjects.jira.ExtendedDashboardPage;
import com.atlassian.streams.testing.pageobjects.jira.JiraIssuePage;
import com.atlassian.streams.testing.pageobjects.jira.StreamsJiraRestorer;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.pageobjects.Matchers.withSummary;
import static com.atlassian.streams.pageobjects.Predicates.summaryContains;
import static com.atlassian.streams.testing.UiTestGroups.JIRA;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@TestGroups(JIRA)
@RunWith(UiTestsTesterRunner.class)
public class JiraInlineActionsTest
{
    private static final String COMMENT_MESSAGE = "This is my test comment.";
    private static final String COMMENT = "Comment";
    private static final String VOTE = "Vote";
    private static final String WATCH = "Watch";
    private static final String GADGET_TITLE = "Activity Stream";

    private static JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class);
    private static StreamsJiraRestorer restorer = new StreamsJiraRestorer(product);

    private ActivityStreamGadget gadget;

    @Rule
    public WebDriverScreenshotRule webDriverScreenshotRule = new WebDriverScreenshotRule();

    @BeforeClass
    public static void restoreDataAndLogIn()
    {
        restorer.restore("jira/backups/base.zip");
        product.gotoLoginPage().loginAsSysAdmin(DashboardPage.class);
    }

    @Before
    public void createNewDashboard()
    {
        String dashboardName = "Test Dashboard - " + RandomUtils.nextInt();
        gadget = product.visit(ExtendedDashboardPage.class)
                .navigateToDashBoardCreationPage()
                .setName(dashboardName)
                .addDashboard()
                .openAddGadgetDialog()
                .loadAllGadgets()
                .addGadget(GADGET_TITLE)
                .finished()
                .getGadgetWithTitle(GADGET_TITLE)
                .viewAs(ActivityStreamGadget.class)
                .openConfiguration()
                .setNumEntries(99)
                .save();

        refreshGadget();
    }
    
    @After
    public void clearVotesAndWatches()
    {
        refreshGadget();

        JiraIssuePage jiraIssuePage = getCreatedOne2().followAlternateLink(JiraIssuePage.class);

        if (jiraIssuePage.isWatching()) {
            jiraIssuePage.toggleWatching();
        }

        if (jiraIssuePage.hasVotedForIssue()) {
            jiraIssuePage.toggleVote();
        }
    }
    

    @Test
    public void assertThatWatchInlineActionIsAvailableForIssues()
    {
        assertThat(getCreatedOne2().getInlineAction(WATCH), is(not(none(ActivityItemInlineAction.class))));
    }

    @Test
    public void assertThatWatchInlineActionIsNotAvailableIfAlreadyWatching()
    {
            getCreatedOne2().getInlineAction(WATCH).get().clickAndWaitForStatusBar();
            refreshGadget();

            assertThat(getCreatedOne2().getInlineAction(WATCH), is(none(ActivityItemInlineAction.class)));
    }

    @Test
    public void assertThatWatchInlineActionRegistersUserAsAWatcher()
    {
            ActivityItem item = getCreatedOne2();
            item.getInlineAction(WATCH).get().clickAndWaitForStatusBar();
            assertThat(item.followAlternateLink(JiraIssuePage.class).getWatcherCount(), is(equalTo("1")));
    }

    @Test
    public void assertThatVoteInlineActionIsAvailableForIssues()
    {
        assertThat(getCreatedOne2().getInlineAction(VOTE), is(not(none(ActivityItemInlineAction.class))));
    }

    @Test
    public void assertThatVoteInlineActionIsNotAvailableForIssuesReported()
    {
        Option<ActivityItem> activityItem = gadget.getActivityItemWhere(summaryContains("admin", "created"));

        assertThat(activityItem.get().getInlineAction(VOTE), is(none(ActivityItemInlineAction.class)));
    }

    @Test
    @Ignore("STRM-836 this broke when bumping to jira 4.3-m5")
    public void assertThatVoteInlineActionIsNotAvailableIfAlreadyVotedOn()
    {
            getCreatedOne2().getInlineAction(VOTE).get().clickAndWaitForStatusBar();
            refreshGadget();

            assertThat(getCreatedOne2().getInlineAction(VOTE), is(none(ActivityItemInlineAction.class)));
    }

    @Test
    public void assertThatVoteInlineActionRegistersUserAsAVoter()
    {
            ActivityItem item = getCreatedOne2();

            item.getInlineAction(VOTE).get().clickAndWaitForStatusBar();
            assertThat(item.followAlternateLink(JiraIssuePage.class).getVoterCount(), is(equalTo("1")));
    }

    @Test
    public void assertThatCommentInlineActionIsAvailableForIssues()
    {
        assertThat(getCreatedOne2().getInlineAction(COMMENT), is(not(none(ActivityItemInlineAction.class))));
    }

    @Test
    public void assertThatCommentInlineActionAddsAComment()
    {
        final String message = COMMENT_MESSAGE + " " + System.currentTimeMillis();

        loadCommentForm(getCreatedOne2()).enterText(message).submit();
        refreshGadget();

        Matcher<Iterable<? super ActivityItem>> hasCommentOnOne2 = hasItem(withSummary(allOf(containsString("ONE-2"), containsString("commented"))));
        assertThat(gadget.getActivityItems(), hasCommentOnOne2);
    }

    @Test
    public void assertThatCommentInlineActionCannotBeSpammed()
    {
        final String message = COMMENT_MESSAGE + " " + System.currentTimeMillis();

        InlineCommentForm form = loadCommentForm(getCreatedOne2());
        form.enterText(message).submit();

        try
        {
            form.submit();
        }
        catch (Exception e)
        {
            // exception may occur as the form is hidden after the first click.
        }

        refreshGadget();

        assertThat(size(gadget.getActivityItemWhere(summaryContains("commented on", "ONE-2", message))), is(equalTo(1)));
    }

    private InlineCommentForm loadCommentForm(ActivityItem item)
    {
        ActivityItemInlineAction action = item.getInlineAction("Comment").get();
        return action.clickOpen(InlineCommentForm.class);
    }

    private ActivityItem getCreatedOne2()
    {
        return gadget.getActivityItemWhere(summaryContains("created", "ONE-2")).get();
    }

    private void refreshGadget()
    {
        gadget = product.visit(ExtendedDashboardPage.class)
            .getGadgetWithTitle(GADGET_TITLE)
            .viewAs(ActivityStreamGadget.class)
            .openConfiguration()
            .save();
    }
}
