package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.webdriver.utils.element.ElementConditions.isPresent;

public class JiraProfileActivityStream {

    @Inject
    private AtlassianWebDriver driver;
    @Inject
    private WebDriverPoller poller;

    private final String AUTH_MESSAGES_CSS_CLASS = "applinks-auth-messages";
    private final String id;


    public JiraProfileActivityStream(String id) {
        this.id = id;
    }

    public String getAuthenticationMessage()
    {
        return getAuthenticationMessageContainer().getText();
    }

    public List<WebElement> getAllAuthenticationMessages() {
        return getAuthenticationMessageContainer().findElements(By.tagName("li"));
    }

    public String getAuthenticationMessageAtIndex(int index) {
        return getAllAuthenticationMessages().get(index).getText();
    }

    private WebElement getAuthenticationMessageContainer()
    {
        poller.waitUntil(isPresent(By.id(id)));
        WebElement container = driver.findElement(By.id(id));
        poller.waitUntil(isPresent(By.className(AUTH_MESSAGES_CSS_CLASS)));
        return container.findElement(By.className(AUTH_MESSAGES_CSS_CLASS));
    }
}
