package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.pageobjects.Page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WebSudoAuthenticatePage implements Page
{
    @FindBy(id = "login-form-authenticatePassword")
    private WebElement password;
    
    @FindBy(id = "login-form-submit")
    private WebElement authenticate;
    
    public String getUrl()
    {
        return "/secure/admin/WebSudoAuthenticate!default.jspa";
    }

    public void loginAsSysAdmin()
    {
        password.sendKeys("admin");
        authenticate.click();
    }
}
