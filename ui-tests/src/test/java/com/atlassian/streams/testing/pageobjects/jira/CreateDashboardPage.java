package com.atlassian.streams.testing.pageobjects.jira;

import javax.inject.Inject;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.mockito.InjectMocks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateDashboardPage implements Page
{
    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder pageBinder;
    private @Inject PageElementFinder elementFinder;

    @FindBy(name = "portalPageName")
    private WebElement name;

    @FindBy(name = "add_submit")
    private WebElement add;
    private String dashboardName;
    @Inject
    Backdoor backdoor;

    public String getUrl()
    {
        return "secure/AddPortalPage!default.jspa";
    }

    @WaitUntil
    public void waitUntilLoaded()
    {
        driver.waitUntilElementIsVisible(By.name("portalPageName"));
    }

    public CreateDashboardPage setName(String dashboardName)
    {
        this.dashboardName = dashboardName;
        name.sendKeys(dashboardName);
        return this;
    }

    public ExtendedDashboardPage addDashboard()
    {
        backdoor.flags().clearFlags();;
        add.click();
        elementFinder.find(By.id("home_link")).click();
        elementFinder.find(By.linkText(dashboardName)).click();
        return pageBinder.bind(ExtendedDashboardPage.class);
    }
}
