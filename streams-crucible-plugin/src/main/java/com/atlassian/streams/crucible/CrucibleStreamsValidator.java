package com.atlassian.streams.crucible;

import com.atlassian.crucible.spi.services.ProjectService;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.streams.spi.StreamsValidator;

public class CrucibleStreamsValidator implements StreamsValidator
{
    private final ProjectService projectService;

    public CrucibleStreamsValidator()
    {
        this(ComponentLocator.getComponent(ProjectService.class));
    }

    public CrucibleStreamsValidator(ProjectService projectService)
    {
        this.projectService = projectService;
    }

    public boolean isValidKey(String key)
    {
        return projectService.getProject(key) != null;
    }
}
