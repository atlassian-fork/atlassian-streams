#!/bin/sh

#./deploy_jira <milestone> <artifact_dir>

set -e

app="JIRA"
milestone=$1
artifact_dir=$2

deploy_dir=/opt/j2ee/domains/atlassian.com/idog/jira-home/plugins/installed-plugins
webinf_deploy_dir=/opt/j2ee/domains/atlassian.com/idog/tomcat/webapps/jira/WEB-INF/lib

webinf_plugins="atlassian-jira-rpc-plugin:5.0-alpha3    \
                atlassian-util-concurrent:2.3.0-beta2"

streams_plugins="streams-aggregator-plugin          \
         streams-api                        \
         streams-core-plugin                \
         streams-jira-plugin                \
         streams-spi                        \
         streams-confluence-inline-actions-plugin \
         streams-inline-actions-plugin      \
         streams-jira-inline-actions-plugin \
         streams-fecru-inline-actions-plugin \
         streams-bamboo-inline-actions-plugin \
         streams-thirdparty-plugin"

plugins="atlassian-universal-plugin-manager-plugin:1.6-m2   \
         activeobjects-plugin:0.15.9                        \
         activeobjects-jira-spi:0.15.9"

echo "-------------"\

echo -e "\nRemoving old version of STRMs in $app"
for plugin in $streams_plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $deploy_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying STRM $milestone to $app"
for plugin in $streams_plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $deploy_dir"
      cp $plugin_file $deploy_dir
  fi
done

echo -e "\nRemoving old version of plugins in $app"
for details in $plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $deploy_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying plugins to $app"
for details in $plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $deploy_dir"
      cp $plugin_file $deploy_dir
  fi
done

#echo -e "\nRemoving old version of required bundled plugins in $app"
#for details in $bundled_plugins
#do
#  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
#  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
#  plugin_file=$artifact_dir/$plugin-$version.jar
#  if [ -f $plugin_file ]
#    then
#      old_plugin=`find $deploy_dir -type f -name "$plugin*"`
#      if [ $old_plugin ]
#        then
#          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
#          rm $old_plugin
#      fi
#  fi
#done
#
#echo -e "\nCopying required bundled plugins to $app"
#for details in $bundled_plugins
#do
#  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
#  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
#  plugin_file=$artifact_dir/$plugin-$version.jar
#  if [ -f $plugin_file ]
#    then
#      echo "copying $plugin_file to $deploy_dir"
#      cp $plugin_file $deploy_dir
#  fi
#done

echo -e "\nRemoving old version of required WEB-INF/lib plugins in $app"
for details in $webinf_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $webinf_deploy_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying required WEB-INF/lib plugins to $app"
for details in $webinf_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $webinf_deploy_dir"
      cp $plugin_file $webinf_deploy_dir
  fi
done

# Set file permissions
chown -R j2ee_idog.atlassian.com $deploy_dir* && chgrp -R j2ee_idog.atlassian.com $deploy_dir*

echo -e "\nDone deploying STRM $milestone to $app"
echo -e "-------------\n"


