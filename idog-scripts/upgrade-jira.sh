#!/bin/sh

set -e

product=jira

# $0 <filename>
filename=$1

if [ $# -eq 0 ]
  then
    echo -e "Usage:\n  ./upgrade-$product.sh <war-file>"
    exit 1
fi

# check if file exists, if not, exit immediately
if [ ! -f $filename ]
  then
    echo -e "[ERROR] File does not exist: $filename"
    exit 1
fi

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts
tomcat_dir=/opt/j2ee/domains/atlassian.com/idog/tomcat/webapps

date=`date +%s`
backup_dir=$script_dir/backup/$product-$date

echo -e "\n----------------------------\nUpgrading $product\n----------------------------\n"

# Create backup directory
echo -e "Creating backup directory $backup_dir..."
mkdir -p $backup_dir

# Backup current product
echo -e "Copying $tomcat_dir/$product.war..."
cp $tomcat_dir/$product.war $backup_dir
echo -e "Copying $tomcat_dir/$product directory..."
cp -R $tomcat_dir/$product $backup_dir

# Delete old product
echo -e "\nDeleting old $product product..."
rm -rf $tomcat_dir/$product.war
rm -rf $tomcat_dir/$product

# Copy new war to tomcat webapp directory
echo -e "\nCopying new $product version to $tomcat_dir/$product.war..."
cp $filename $tomcat_dir/$product.war
echo -e "\nExtracting $product.war...\n"
unzip $tomcat_dir/$product.war -d $tomcat_dir/$product

# Change the jira.home directory
echo -e "\nChanging jira.home directory"
sed 's/^jira\.home\s*=\s*$/jira.home = \/opt\/j2ee\/domains\/atlassian\.com\/idog\/jira-home\//g' $tomcat_dir/$product/WEB-INF/classes/jira-application.properties > temp-jira-application-properties
mv temp-jira-application-properties $tomcat_dir/$product/WEB-INF/classes/jira-application.properties

# Set file permissions
chown -R j2ee_idog.atlassian.com $tomcat_dir/$product* && chgrp -R j2ee_idog.atlassian.com $tomcat_dir/$product*

# Done
echo -e "\n----------------------------\nDone upgrading $product\nStart your server and make sure $product is properly configured\n----------------------------\n"

