#!/bin/sh

#./deploy_bamboo <milestone> <artifact_dir>

app="Bamboo"
milestone=$1
artifact_dir=$2

deploy_dir=/opt/j2ee/domains/atlassian.com/idog/bamboo-home/plugins
webinf_lib_dir=/opt/j2ee/domains/atlassian.com/idog/tomcat-bamboo/webapps/bamboo/WEB-INF/lib

bundled_plugins="atlassian-util-concurrent:2.3.0-beta2            \
                 atlassian-universal-plugin-manager-plugin:1.6-m2 \
                 atlassian-trusted-apps-plugin:3.0.0-plugin2      \
                 activeobjects-plugin:0.15.9                      \
                 activeobjects-bamboo-spi:0.15.9"

webinf_lib_plugins="atlassian-trusted-apps-api:3.0.0-plugin2 "

plugins="streams-aggregator-plugin            \
         streams-api                          \
         streams-core-plugin                  \
         streams-bamboo-plugin                \
         streams-spi                          \
         streams-inline-actions-plugin        \
         streams-bamboo-inline-actions-plugin \
         streams-thirdparty-plugin"

echo "-------------"

echo -e "\nRemoving old version of STRMs in $app"
for plugin in $plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $deploy_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying STRM $milestone to $app"
for plugin in $plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $deploy_dir"
      cp $plugin_file $deploy_dir
  fi
done

echo -e "\nRemoving old version of required bundled plugins in $app"
for details in $bundled_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $deploy_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying required bundled plugins to $app"
for details in $bundled_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $deploy_dir"
      cp $plugin_file $deploy_dir
      #Set file permissions
  fi
done

echo -e "\nRemoving old version of required WEB-INF/lib plugins in $app"
for details in $webinf_lib_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $webinf_lib_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying required WEB-INF/lib plugins to $app"
for details in $webinf_lib_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $webinf_lib_dir"
      cp $plugin_file $webinf_lib_dir
      #Set file permissions
  fi
done

#Set file permissions
chown -R j2ee_idog.atlassian.com $deploy_dir* && chgrp -R j2ee_idog.atlassian.com $deploy_dir*
chown -R j2ee_idog.atlassian.com $webinf_lib_dir* && chgrp -R j2ee_idog.atlassian.com $webinf_lib_dir*

echo -e "\nDone deploying STRM $milestone to $app"
echo -e "-------------\n"
                                                                                                                                                                                                       
