#!/bin/sh

# $0 <productId>
# values:
#    1 - Bamboo
#    2 - Confluence
#    3 - JIRA
#    4 - Fe/Cru
#
# $1 <version>
productId=$1
version=$2

atlassian_products[1]="com.atlassian.bamboo:atlassian-bamboo-web-app:war"
atlassian_products[2]="com.atlassian.confluence:confluence-webapp:war"
atlassian_products[3]="com.atlassian.crucible:atlassian-crucible:zip"
atlassian_products[4]="com.atlassian.jira:atlassian-jira-webapp:war"

base_url="https://maven.atlassian.com/service/local/repo_groups/internal/content"

PID=$$ # get PID of current process
LogUID=`cat /proc/"$PID"/loginuid` # get loginuid of current process
username=`getent passwd "$LogUID" | awk -F ":" '{print $1}'` # translate loginuid to username

path=`echo ${atlassian_products[$productId]} | awk '{ print substr($0, 0, index($0, ":")-1 ) }' | sed 's/\./\//g'`
artifact=`echo ${atlassian_products[$productId]} | awk '{ print substr($0, index($0, ":")+1, length($0)) }' | awk '{ print substr($0, 0, index($0, ":")-1) }'`
ext=`echo ${atlassian_products[$productId]} | awk '{ print substr($0, index($0, ":")+1, length($0)) }' | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`

echo -e "\n---------------"
echo -e "Starting download for $artifact-$version.$ext\n"

curl --user $username --remote-name $base_url/$path/$artifact/$version/$artifact-$version.$ext

echo -e "\nDone downloading $artifact-$version.$ext"
echo -e "---------------\n"

