#!/bin/sh

set -e

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts
plugins_dir=/opt/j2ee/domains/atlassian.com/idog/scripts/plugins
MAVEN_BIN=/opt/j2ee/domains/atlassian.com/idog/Applications/apache-maven-2.1.0/bin
fecru_bin=/opt/j2ee/domains/atlassian.com/idog/fecru/bin
bamboo_bin=/opt/j2ee/domains/atlassian.com/idog/tomcat-bamboo/bin
build_dir=$script_dir/build-dir
temp_dir=$script_dir/temp/deploy_build
LOCKFILE=$script_dir/script.lock

# SNAPSHOT build. Change this after releasing a new version of STRMs
SNAPSHOT_BUILD=5.0-SNAPSHOT

if [ -e ${LOCKFILE} ] && kill -0 `cat ${LOCKFILE}`; then
    echo -e "\nERROR: Script lock file found.. Is the script already running? If not, please delete ${LOCKFILE}\n"
    exit
fi

# make sure the lockfile is removed when we exit and then claim it
trap "rm -f ${LOCKFILE}; exit" INT TERM EXIT
echo $$ > ${LOCKFILE}

# create temp and build directory
mkdir -p $build_dir
mkdir -p $temp_dir

# Fresh checkout of STRMs project
echo -e "\nChecking out STRMs project"
svn checkout https://studio.atlassian.com/svn/STRM/trunk $build_dir/STRM

old_dir=`pwd`

cd $build_dir/STRM

# Build STRMs project
echo -e "\nBuilding STRMs"
$MAVEN_BIN/mvn clean install -DskipTests

# Copy built jars to temp directory
echo -e "\n------------------Copying jars to temp directory $temp_dir---------------------"
for file in `find . -wholename "**/target/streams*.jar"`
do
  echo -e "Copying $file"
  cp $file $temp_dir/
done

cd $old_dir

# download plugin files
# $script_dir/get-plugins.sh

# move downloaded files to deploy directory
cp $plugins_dir/*.jar $temp_dir

# Shutdown Confluence and JIRA Tomcat server
echo -e "\n-----------------\nStopping JIRA/Confluence Tomcat server\n---------------------"
svc -d /service/j2ee_idog.atlassian.com/

# Shutdown Bamboo Tomcat server
echo -e "\n-----------------\nStopping Bamboo server\n---------------------"
$bamboo_bin/shutdown.sh

# If Fe/Cru server is up, shut it down, skip otherwise

# Shutdown Fe/Cru server - we don't care if we fail stopping the fecru server.
echo -e "\n-----------------\nStopping Fe/Cru server\n---------------------"
$fecru_bin/stop.sh || true

# Deploy local jars from temp directory
echo -e "\n-----------------\nDeploying STRM-$SNAPSHOT_BUILD\n---------------------"
$script_dir/deploy_local.sh $SNAPSHOT_BUILD $temp_dir

# Start JIRA and Confluence Tomcat server
echo -e "\n-----------------\nStarting JIRA/Confluence Tomcat server\n---------------------"
svc -u /service/j2ee_idog.atlassian.com/

# Start Bamboo Tomcat server
echo -e "\n-----------------\nStarting Bamboo server\n---------------------"
$bamboo_bin/startup.sh

# Start Fe/Cru server
echo -e "\n-----------------\nStarting Fe/Cru server\n---------------------"
$fecru_bin/start.sh

# Delete temp and build directory
echo -e "------------------\nDeleting temp directory $temp_dir\n--------------------"
rm -rf $temp_dir
echo -e "------------------\nDeleting build directory $build_dir/STRM\n--------------------"
rm -rf $build_dir

# Remove script lock
rm -f ${LOCKFILE}

# Done
echo -e "\n------------------\nSuccessfully finished deploying latest STRM SNAPSHOT build to idog. Please wait for your apps to start up\n--------------------"




