#!/bin/sh

#./deploy_fecru <milestone> <artifact_dir>

app="Fe/Cru"
milestone=$1
artifact_dir=$2

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts
deploy_dir=/opt/j2ee/domains/atlassian.com/idog/fisheye-home/var/plugins/user
bundled_zip=/opt/j2ee/domains/atlassian.com/idog/fecru/plugins/bundled-plugins.zip
temp_dir=$script_dir/temp/fecru_bund


bundled_plugins="atlassian-util-concurrent:2.3.0-beta2                   \
                 atlassian-universal-plugin-manager-plugin:1.6-m2        \
                 activeobjects-plugin:0.15.9"

bundled_strm_plugins="streams-api                                        \
                      streams-spi                                        \
                      streams-aggregator-plugin                          \
                      streams-core-plugin                                \
                      streams-fisheye-plugin                             \
                      streams-crucible-plugin                            \
                      streams-fecru-activity-item-provider-plugin        \
                      streams-thirdparty-plugin"

# As of FishEye 2.7, all Streams plugins are currently bundled.
plugins=""

echo "-------------"

echo -e "Unzipping bundled-plugins.zip.. to $temp_dir"

unzip $bundled_zip -d $temp_dir

echo -e "\nRemoving old version of bundled STRMs in $app"
for plugin in $bundled_strm_plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $temp_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying STRM $milestone to $app bundled plugins"
for plugin in $bundled_strm_plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $temp_dir"
      cp $plugin_file $temp_dir
  fi
done

echo -e "\nRemoving old version of required bundled plugins in $app\n"
for details in $bundled_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $temp_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying required bundled plugins to $app"
for details in $bundled_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $temp_dir"
      cp $plugin_file $temp_dir
  fi
done

# Set file permissions
chown -R j2ee_idog.atlassian.com $temp_dir* && chgrp -R j2ee_idog.atlassian.com $temp_dir*

# Recreating bundled_plugins.zip
echo -e "\nZipping $app bundled plugins"

old_dir=`pwd`

cd $temp_dir
zip bundled-plugins.zip *
cp bundled-plugins.zip $bundled_zip

# Set file permissions
chown -R j2ee_idog.atlassian.com $bundled_zip* && chgrp -R j2ee_idog.atlassian.com $bundled_zip*

cd $old_dir

rm -rf $temp_dir

echo -e "\nRemoving old version of STRMs in $app"
for plugin in $plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $deploy_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying STRM $milestone to $app"
for plugin in $plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $deploy_dir"
      cp $plugin_file $deploy_dir
  fi
done

# Set file permissions
chown -R j2ee_idog.atlassian.com $deploy_dir* && chgrp -R j2ee_idog.atlassian.com $deploy_dir*

echo -e "\nDone deploying STRM $milestone to Fe/Cru"
echo -e "-------------\n"

