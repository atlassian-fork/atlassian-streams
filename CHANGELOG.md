# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [8.2.1] - Unreleased

### Added
- [BSP-997] Can measure coverage of unit tests.

### Fixed
- [BSP-1085] Unit tests of `aggregator-plugin` module were not running.

## [8.2.0] - 2019-12-06

### Fixed
- [BSP-658]: Fixed possible DoS in 3rd party applications query.

### Changed
- [BSP-658]: Changed querying for 3rd party applications to group-by query.

### Added
- [BSP-658]: Added dependency on ao-plugin 3.1.7+

## [8.1.6] - Unreleased

### Updates
- [MNSTR-3001]: added Jira username anonymization handler (GDPR SPI implementation). Restricted only to Jira >= 8.3.0.

## [8.1.0] - Unreleased

### Updates
- [MNSTR-2284]: added dependency on ajs message on js file calling "AJS.$(this).closeMessage();"
- [MNSTR-1780]: aggregator plugin works with jQuery v2.2.4 that is shipped as a global variable with AUI8
- [MNSTR-1780]: removed jquery.ui.core.js (v1.8.6)
- [MNSTR-1780]: updated jquery.ui.datepicker.js to v1.12.1, along with css
- [MNSTR-1780]: aligned aui-message "message type" classes with latest aui (eg. error->aui-message-error) 

## [6.4.6] - 2018.09.10

### Fixed
- [MNSTR-1893]: Minor change: removed unnecessary checks for IE older than IE11 that would not work with new jQuery version

[6.4.6]: https://bitbucket.org/atlassian/atlassian-streams/commits/tag/streams-parent-6.4.6
[MNSTR-1893]: https://bulldog.internal.atlassian.com/browse/MNSTR-1893

## [6.4.4] - Unreleased

### Fixed
- [STRM-2357]: Removed unused spring mvc imports in `streams-thirdparty-plugin`

[6.4.4]: https://bitbucket.org/atlassian/atlassian-streams/commits/tag/streams-parent-6.4.4 
[STRM-2357]: https://ecosystem.atlassian.net/browse/STRM-2357

## [6.4.3] - 2018-06-04

### Fixed
- [MNSTR-1771]: Changed 'JIRA' to 'Jira' in messages displayed by `streams-jira-plugin`

[6.4.3]: https://bitbucket.org/atlassian/atlassian-streams/commits/tag/streams-parent-6.4.3
[MNSTR-1771]: https://bulldog.internal.atlassian.com/browse/MNSTR-1771
