package com.atlassian.streams.api;

import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;

import com.google.common.collect.Multimap;

import java.net.URI;

/**
 * A request for activity
 */
public interface ActivityRequest
{
    String MAX_RESULTS = "maxResults";
    String PROVIDERS_KEY = "providers";
    String USE_ACCEPT_LANG_KEY = "use-accept-lang";
    String TIMEOUT = "timeout";

    /**
     * The maximum number of results to return
     *
     * @return The maximum number of results
     */
    int getMaxResults();

    /**
     * The maximum number of milliseconds to allow for the request
     *
     * @return The maximum number of milliseconds to allow for the request
     */
    int getTimeout();

    /**
     * The base URI of the request
     * @return the Base URI of the request
     */
    Uri getUri();

    /**
     * The context base URI of the application, as seen by the request. This is
     * so that we can give an absolute link for applications which are exposed
     * through several URLs.
     * @return the Context Base URI of the application for the request.
     */
    URI getContextUri();

    Multimap<String, Pair<Operator, Iterable<String>>> getStandardFilters();

    Multimap<String, Pair<Operator, Iterable<String>>> getProviderFilters();

    /**
     * Get the value of the {@code Accept-Language} HTTP header.
     *
     *  @return the value of the {@code Accept-Language} HTTP header, or {@code null} if not set.
     */
    String getRequestLanguages();
}
