package com.atlassian.streams.thirdparty;

import java.net.URI;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Link;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.Image;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.google.common.base.Preconditions.checkNotNull;

public class ThirdPartyStreamsEntryBuilder
{
    public static final String GENERATOR_KEY_SEPARATOR = "@";
    public static final String WEB_RESOURCES = "com.atlassian.streams.streams-thirdparty-plugin:thirdparty-web-resources";
    public static final URI THIRD_PARTY_APPLICATION_TYPE = URI.create("com.atlassian.streams.thirdparty");
    public static final String DEFAULT_ACTIVITY_ICON = "images/strea-ms-logo-blue.png";

    private final StreamsI18nResolver i18nResolver;
    private final WebResourceManager webResourceManager;

    public ThirdPartyStreamsEntryBuilder(StreamsI18nResolver i18nResolver,
                                         WebResourceManager webResourceManager)
    {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.webResourceManager = checkNotNull(webResourceManager, "webResourceManager");
    }

    public StreamsEntry buildStreamsEntry(Activity activity)
    {
        return new StreamsEntry(StreamsEntry.params()
                                    .id(getActivityId(activity))
                                    .postedDate(activity.getPostedDate())
                                    .applicationType(activity.getApplication().getId().toASCIIString())
                                    .alternateLinkUri(getLinkUri(activity))
                                    .categories(getActivityCategories(activity))
                                    .addActivityObjects(getActivityObjects(activity))
                                    .verb(getActivityVerb(activity))
                                    .target(getActivityTarget(activity))
                                    .addLinks(getLinks(activity))
                                    .renderer(getRenderer(activity))
                                    .authors(ImmutableNonEmptyList.of(activity.getUser())),
                                i18nResolver);
    }

    static URI getActivityId(Activity activity)
    {
        for (URI uri: activity.getId())
        {
            return uri;
        }
        return URI.create("");
    }

    static String getProviderIdAndName(Application application)
    {
        return application.getId().toASCIIString() + GENERATOR_KEY_SEPARATOR
            + application.getDisplayName();
    }

    static URI getLinkUri(Activity activity)
    {
        for (URI uri: activity.getUrl())
        {
            return uri;
        }
        return URI.create("");
    }

    private Supplier<URI> defaultActivityIconUri = new Supplier<URI>()
    {
        public URI get()
        {
            return URI.create(webResourceManager.getStaticPluginResource(WEB_RESOURCES,
                                                                         DEFAULT_ACTIVITY_ICON,
                                                                         UrlMode.ABSOLUTE));
        }
    };

    Iterable<String> getActivityCategories(Activity activity)
    {
        return ImmutableList.of();
    }

    Iterable<com.atlassian.streams.api.StreamsEntry.ActivityObject> getActivityObjects(Activity activity)
    {
        for (ActivityObject object : activity.getObject())
        {
            return ImmutableList.of(buildActivityObject(object));
        }
        return ImmutableList.of();

    }

    com.atlassian.streams.api.StreamsEntry.ActivityObject buildActivityObject(ActivityObject object)
    {
        return new com.atlassian.streams.api.StreamsEntry.ActivityObject(com.atlassian.streams.api.StreamsEntry.ActivityObject.params()
                                                                             .id(object.getId().map(toASCIIString))
                                                                             .activityObjectType(object.getType().map(toSimpleActivityObjectType))
                                                                             .title(object.getDisplayName())
                                                                             .alternateLinkUri(object.getUrl()));
    }

    private ActivityVerb getActivityVerb(Activity activity)
    {
        for (URI verb: activity.getVerb())
        {
            return new SimpleActivityVerb(verb);
        }
        return post();
    }

    private Option<com.atlassian.streams.api.StreamsEntry.ActivityObject> getActivityTarget(Activity activity)
    {
        for (ActivityObject object : activity.getTarget())
        {
            return some(buildActivityObject(object));
        }
        return none();
    }

    private Iterable<Link> getLinks(Activity activity)
    {
        for (Image icon : activity.getIcon())
        {
            return ImmutableList.of(new Link(icon.getUrl(), ICON_LINK_REL, some(activity.getApplication().getDisplayName())));
        }
        return ImmutableList.of(new Link(defaultActivityIconUri.get(), ICON_LINK_REL, some(activity.getApplication().getDisplayName())));
    }

    private StreamsEntry.Renderer getRenderer(Activity activity)
    {
        return new ActivityRenderer(activity);
    }

    private class ActivityRenderer implements StreamsEntry.Renderer
    {
        private final Activity activity;

        public ActivityRenderer(Activity activity)
        {
            this.activity = activity;
        }

        @Override
        public Html renderTitleAsHtml(StreamsEntry entry)
        {
            return activity.getTitle().getOrElse(html(""));
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry)
        {
            // TODO: use entity.getObject().getSummary(), or compute our own summary from the content?
            return none();
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry)
        {
            return activity.getContent();
        }
    }

    private static String getKeyFromIRIOrSimpleName(URI iriOrSimpleName)
    {
        String iriString = iriOrSimpleName.toASCIIString();
        if (iriString.indexOf("/") >= 0)
        {
            return iriString.substring(iriString.lastIndexOf('/') + 1);
        }
        return iriString;
    }

    private static class SimpleActivityTag<TParent>
    {
        private URI uri;
        private String key;

        public SimpleActivityTag(URI iri)
        {
            this.uri = iri;
            this.key = getKeyFromIRIOrSimpleName(iri);
        }

        public URI iri()
        {
            return uri;
        }

        public String key()
        {
            return key;
        }

        public Option<TParent> parent()
        {
            return none();
        }

        public boolean equals(Object other)
        {
            return (getClass().isAssignableFrom(other.getClass())) &&
                   ((SimpleActivityTag<?>) other).uri.equals(uri);
        }

        @Override
        public int hashCode()
        {
            return uri.hashCode();
        }
    }

    private static final class SimpleActivityObjectType extends SimpleActivityTag<ActivityObjectType>
        implements ActivityObjectType
    {
        public SimpleActivityObjectType(URI iri)
        {
            super(iri);
        }
    }

    private static final class SimpleActivityVerb extends SimpleActivityTag<ActivityVerb>
        implements ActivityVerb
    {
        public SimpleActivityVerb(URI iri)
        {
            super(iri);
        }
    }

    private static final Function<URI, ActivityObjectType> toSimpleActivityObjectType =
        new Function<URI, ActivityObjectType>()
        {
            public ActivityObjectType apply(URI from)
            {
                return new SimpleActivityObjectType(from);
            }
        };

    private static final Function<URI, String> toASCIIString = new Function<URI, String>()
    {
        public String apply(URI from)
        {
            return from.toASCIIString();
        }
    };
}
