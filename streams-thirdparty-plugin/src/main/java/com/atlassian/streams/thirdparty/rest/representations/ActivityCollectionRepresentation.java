package com.atlassian.streams.thirdparty.rest.representations;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * JSON representation class used by the Activity REST API.  Represents the result of an
 * activity list query ({@code GET /rest/activities/1.0/}).
 */
public class ActivityCollectionRepresentation
{
    @JsonProperty Collection<ActivityRepresentation> items;
    @JsonProperty private final Map<String, URI> links;

    @JsonCreator
    public ActivityCollectionRepresentation(@JsonProperty("items") Collection<ActivityRepresentation> items,
                                            @JsonProperty("links") Map<String, URI> links)
    {
        this.items = new ArrayList<ActivityRepresentation>(items);
        this.links = new HashMap<String, URI>(links);
    }
    
    public Iterable<ActivityRepresentation> getItems()
    {
        return Collections.unmodifiableCollection(items);
    }
    
    public Map<String, URI> getLinks()
    {
        return Collections.unmodifiableMap(links);
    }
}
