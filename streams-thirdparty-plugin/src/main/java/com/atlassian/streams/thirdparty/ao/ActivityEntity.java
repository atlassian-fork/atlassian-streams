package com.atlassian.streams.thirdparty.ao;

import java.net.URI;
import java.util.Date;

// If you're having troubles compiling in IDEA because it can't find these classes,
// Try activating the "ide" profile. See the pom for the thirdparty-plugin for details.
import net.java.ao.Preload;
import net.java.ao.RawEntity;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;
import net.java.ao.schema.StringLength;

@Preload ()
public interface ActivityEntity extends RawEntity<Long>
{
    @AutoIncrement
    @NotNull
    @PrimaryKey("activity_id")
    long getActivityId();

    URI getId();
    void setId(URI id);

    ActorEntity getActor();
    void setActor(ActorEntity actor);

    String getUsername();
    void setUsername(String username);

    String getPoster();
    void setPoster(String username);
    
    @StringLength(StringLength.UNLIMITED)
    String getContent();
    void setContent(String content);

    URI getGeneratorId();
    void setGeneratorId(URI generatorId);
    
    String getGeneratorDisplayName();
    void setGeneratorDisplayName(String generatorDisplayName);

    MediaLinkEntity getIcon();
    void setIcon(MediaLinkEntity icon);

    ObjectEntity getObject();
    void setObject(ObjectEntity object);

    Date getPublished();
    void setPublished(Date published);

    TargetEntity getTarget();
    void setTarget(TargetEntity target);

    String getTitle();
    void setTitle(String title);

    URI getUrl();
    void setUrl(URI url);

    URI getVerb();
    void setVerb(URI verb);
    
    String getProjectKey();
    void setProjectKey(String projectKey);
    
    String getIssueKey();
    void setIssueKey(String issueKey);
}
