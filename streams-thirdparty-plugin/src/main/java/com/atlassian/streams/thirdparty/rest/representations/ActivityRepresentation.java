package com.atlassian.streams.thirdparty.rest.representations;

import java.net.URI;
import java.util.Date;
import java.util.Map;

import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.ValidationErrors;

import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.Html.htmlToString;
import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.thirdparty.api.ValidationErrors.validationError;
import static com.google.common.base.Functions.toStringFunction;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * JSON representation class used by the Activity REST API.
 */
public class ActivityRepresentation
{
    @JsonProperty ActivityObjectRepresentation actor;
    @JsonProperty String content;
    @JsonProperty ActivityObjectRepresentation generator;
    @JsonProperty MediaLinkRepresentation icon;
    @JsonProperty String id;
    @JsonProperty ActivityObjectRepresentation object;
    @JsonProperty Date published;
    @JsonProperty ActivityObjectRepresentation provider;
    @JsonProperty ActivityObjectRepresentation target;
    @JsonProperty String title;
    @JsonProperty Date updated;
    @JsonProperty String url;
    @JsonProperty String verb;
    @JsonProperty Map<String, URI> links;

    /**
     * Constructor used for Jackson deserialization.  Use {@link #builder()} for all other purposes.
     */
    @JsonCreator
    public ActivityRepresentation(@JsonProperty("actor") ActivityObjectRepresentation actor,
                                  @JsonProperty("content") String content,
                                  @JsonProperty("generator") ActivityObjectRepresentation generator,
                                  @JsonProperty("icon") MediaLinkRepresentation icon,
                                  @JsonProperty("id") String id,
                                  @JsonProperty("object") ActivityObjectRepresentation object,
                                  @JsonProperty("published") Date published,
                                  @JsonProperty("provider") ActivityObjectRepresentation provider,
                                  @JsonProperty("target") ActivityObjectRepresentation target,
                                  @JsonProperty("title") String title,
                                  @JsonProperty("date") Date updated,
                                  @JsonProperty("url") String url,
                                  @JsonProperty("verb") String verb,
                                  @JsonProperty("links") Map<String, URI> links,
                                  @JsonProperty("application") ActivityObjectRepresentation application,
                                  @JsonProperty("user") ActivityObjectRepresentation user)
    {
        this.actor = actor == null ? user : actor;
        this.content = content;
        this.generator = generator == null ? application : generator;
        this.icon = icon;
        this.id = id;
        this.object = object;
        this.published = published;
        this.provider = provider;
        this.target = target;
        this.title = title;
        this.updated = updated;
        this.url = url;
        this.verb = verb;
        this.links = links == null ? ImmutableMap.<String, URI>of() : ImmutableMap.copyOf(links);
    }
    
    /**
     * Returns a {@link Builder} for creating a new instance of this class.
     */
    public static Builder builder(ActivityObjectRepresentation actor, ActivityObjectRepresentation generator)
    {
        return new Builder(actor, generator);
    }
    
    private ActivityRepresentation(Builder builder)
    {
        this.actor = builder.actor;
        this.content = builder.content.map(htmlToString()).getOrElse((String) null);
        this.generator = builder.generator;
        this.icon = builder.icon.getOrElse((MediaLinkRepresentation) null);
        this.id = builder.id.getOrElse((String) null);
        this.object = builder.object.getOrElse((ActivityObjectRepresentation) null);
        this.published = builder.published.getOrElse((Date) null);
        this.target = builder.target.getOrElse((ActivityObjectRepresentation) null);
        this.title = builder.title.map(htmlToString()).getOrElse((String) null);
        this.updated = builder.updated.getOrElse((Date) null);
        this.url = builder.url.getOrElse((String) null);
        this.verb = builder.verb.getOrElse((String) null);
        this.links = ImmutableMap.copyOf(builder.links.getOrElse(ImmutableMap.<String, URI>of()));
    }

    /**
     * Convert to an activity using the supplied user profile as the actor.
     */
    public Either<ValidationErrors, Activity> toActivity(com.atlassian.sal.api.user.UserProfile userProfile)
    {
        Either<ValidationErrors, Application> applicationOrError;
        DateTime postedDate;
        Either<ValidationErrors, UserProfile> userOrError;
        
        if (getGenerator() == null)
        {
            applicationOrError = left(validationError("activity must have generator"));
        }
        else
        {
            applicationOrError = Application.application(option(getGenerator().getDisplayName()),
                                                         option(getGenerator().getId()));
        }

        if (getPublished() == null)
        {
            postedDate = new DateTime();
        }
        else
        {
            postedDate = new DateTime(getPublished());
        }
        
        if (userProfile == null)
        {
            userOrError = left(validationError("activity must have actor"));
        }
        else
        {
            final MediaLinkRepresentation profilePicture = MediaLinkRepresentation.builder
                    (userProfile.getProfilePictureUri()).build();
            final ActivityObjectRepresentation rep = ActivityObjectRepresentation.builder()
                    .idString(option(userProfile.getUsername()))
                    .displayName(option(userProfile.getFullName()))
                    .image(option(profilePicture))
                    .build();
            userOrError = rep.toUserProfile();
        }

        Activity.Builder builder = new Activity.Builder(applicationOrError,
                                                        Either.<ValidationErrors, DateTime>right(postedDate),
                                                        userOrError);
        
        builder.content(option(getContent()).map(html()));
        builder.idString(option(getId()));
        if (getIcon() != null)
        {
            builder.icon(getIcon().toImage());
        }
        if (getObject() != null)
        {
            builder.object(getObject().toActivityObject());
        }
        if (getTarget() != null)
        {
            builder.target(getTarget().toActivityObject());
        }
        builder.title(option(getTitle()).map(html()));
        builder.urlString(option(getUrl()));
        builder.verbString(option(getVerb()));

        return builder.build();
    }

    public ActivityObjectRepresentation getActor()
    {
        return actor;
    }

    public String getContent()
    {
        return content;
    }

    public ActivityObjectRepresentation getGenerator()
    {
        return generator;
    }

    public MediaLinkRepresentation getIcon()
    {
        return icon;
    }

    public String getId()
    {
        return id;
    }

    public ActivityObjectRepresentation getObject()
    {
        return object;
    }

    public Date getPublished()
    {
        return published;
    }

    public ActivityObjectRepresentation getProvider()
    {
        return provider;
    }

    public ActivityObjectRepresentation getTarget()
    {
        return target;
    }

    public String getTitle()
    {
        return title;
    }

    public Date getUpdated()
    {
        return updated;
    }

    public String getUrl()
    {
        return url;
    }

    public String getVerb()
    {
        return verb;
    }
    
    public Map<String, URI> getLinks()
    {
        return links;
    }
    
    public static class Builder
    {
        private ActivityObjectRepresentation actor;
        private Option<Html> content = none();
        private ActivityObjectRepresentation generator;
        private Option<MediaLinkRepresentation> icon = none();
        private Option<String> id = none();
        private Option<ActivityObjectRepresentation> object = none();
        private Option<Date> published = none();
        private Option<ActivityObjectRepresentation> target = none();
        private Option<Html> title = none();
        private Option<Date> updated = none();
        private Option<String> url = none();
        private Option<String> verb = none();
        private Option<Map<String, URI>> links = none();
        
        public Builder(ActivityObjectRepresentation actor, ActivityObjectRepresentation generator)
        {
            this.actor = checkNotNull(actor, "actor");
            this.generator = checkNotNull(generator, "generator");
        }
        
        public ActivityRepresentation build()
        {
            return new ActivityRepresentation(this);
        }
        
        public Builder content(Option<Html> content)
        {
            this.content = checkNotNull(content, "content");
            return this;
        }
        
        public Builder icon(Option<MediaLinkRepresentation> icon)
        {
            this.icon = checkNotNull(icon, "icon");
            return this;
        }

        public Builder id(Option<URI> id)
        {
            this.id = checkNotNull(id, "id").map(toStringFunction());
            return this;
        }
        
        public Builder idString(Option<String> id)
        {
            this.id = checkNotNull(id, "id");
            return this;
        }
        
        public Builder object(Option<ActivityObjectRepresentation> object)
        {
            this.object = checkNotNull(object, "object");
            return this;
        }

        public Builder published(Option<Date> published)
        {
            this.published = checkNotNull(published, "published");
            return this;
        }

        public Builder target(Option<ActivityObjectRepresentation> target)
        {
            this.target = checkNotNull(target, "target");
            return this;
        }
        
        public Builder title(Option<Html> title)
        {
            this.title = checkNotNull(title, "title");
            return this;
        }
        
        public Builder updated(Option<Date> updated)
        {
            this.updated = checkNotNull(updated, "updated");
            return this;
        }

        public Builder url(Option<URI> url)
        {
            this.url = checkNotNull(url, "url").map(toStringFunction());
            return this;
        }
        
        public Builder urlString(Option<String> url)
        {
            this.url = checkNotNull(url, "url");
            return this;
        }

        public Builder verb(Option<URI> verb)
        {
            this.verb = checkNotNull(verb, "verb").map(toStringFunction());
            return this;
        }
        
        public Builder verbString(Option<String> verb)
        {
            this.verb = checkNotNull(verb, "verb");
            return this;
        }
        
        public Builder links(Option<Map<String, URI>> links)
        {
            this.links = checkNotNull(links, "links");
            return this;
        }
    }
}
