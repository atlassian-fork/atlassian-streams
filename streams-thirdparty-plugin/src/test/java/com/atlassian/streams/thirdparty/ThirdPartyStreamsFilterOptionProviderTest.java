package com.atlassian.streams.thirdparty;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.api.ActivityService;

import com.google.common.collect.Iterables;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_GENERATOR_ID;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_GENERATOR_NAME;
import static com.atlassian.streams.thirdparty.ThirdPartyStreamsFilterOptionProvider.PROVIDER_NAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ThirdPartyStreamsFilterOptionProviderTest
{
    private static final String PROVIDER_NAME_KEY = "streams.filter.thirdparty.provider.name";
    private static final String PROVIDER_NAME_DISPLAY = "Provider";
    private static final String PROVIDER_NAME_HELP_KEY = "streams.filter.help.thirdparty.provider.name";
    
    @Mock
    private StreamsI18nResolver i18nResolver;
    @Mock
    private ActivityService service;
    @Mock
    private TransactionTemplate transactionTemplate;

    private Map<String, String> generators;

    private ThirdPartyStreamsFilterOptionProvider provider;

    @Before
    public void setup()
    {
        generators = new HashMap<>();
        generators.put(DEFAULT_GENERATOR_ID.toASCIIString() + "@" + DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_NAME);

        when(i18nResolver.getText(PROVIDER_NAME_KEY)).thenReturn(PROVIDER_NAME_DISPLAY);
        when(transactionTemplate.<Map<String, String>>execute(any(TransactionCallback.class))).thenReturn(generators);

        provider = new ThirdPartyStreamsFilterOptionProvider(service, i18nResolver, transactionTemplate);
    }

    @Test
    public void assertProviderFilterDisplayName()
    {
        StreamsFilterOption option = getProviderNameFilterOption();
        assertThat(option.getDisplayName(), equalTo(PROVIDER_NAME_DISPLAY));
    }

    @Test
    public void assertProviderFilterI18nKey()
    {
        StreamsFilterOption option = getProviderNameFilterOption();
        assertThat(option.getI18nKey(), equalTo(PROVIDER_NAME_KEY));
    }

    @Test
    public void assertProviderFilterHelpTextKey()
    {
        StreamsFilterOption option = getProviderNameFilterOption();
        assertThat(option.getHelpTextI18nKey(), equalTo(PROVIDER_NAME_HELP_KEY));
    }

    @Test
    public void assertProviderFilterType()
    {
        StreamsFilterOption option = getProviderNameFilterOption();
        assertThat(option.getFilterType(), equalTo(StreamsFilterType.SELECT));
    }

    @Test
    public void assertThatProviderValuesUseTheGeneratorIdIfItExists()
    {
        StreamsFilterOption option = getProviderNameFilterOption();
        assertThat(option.getValues(), hasEntry(DEFAULT_GENERATOR_ID.toASCIIString() + "@" + DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_NAME));
    }

    private StreamsFilterOption getProviderNameFilterOption()
    {
        return Iterables.getOnlyElement(
                Iterables.filter(provider.getFilterOptions(), input -> (input.getKey().equals(PROVIDER_NAME))));
    }
}
