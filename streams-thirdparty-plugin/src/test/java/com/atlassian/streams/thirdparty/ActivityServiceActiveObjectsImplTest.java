package com.atlassian.streams.thirdparty;

import java.net.URI;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities;
import com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Applications;
import com.atlassian.streams.thirdparty.ThirdPartyStreams.MockFeedContentSanitizer;
import com.atlassian.streams.thirdparty.ThirdPartyStreams.ThirdPartyStreamsDatabaseUpdater;
import com.atlassian.streams.thirdparty.ao.ActivityEntity;
import com.atlassian.streams.thirdparty.ao.ActorEntity;
import com.atlassian.streams.thirdparty.ao.MediaLinkEntity;
import com.atlassian.streams.thirdparty.ao.ObjectEntity;
import com.atlassian.streams.thirdparty.ao.TargetEntity;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.ValidationErrors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.Hsql;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_ACTIVITY_ID;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_ACTIVITY_ID_2;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_ACTIVITY_ID_3;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_ACTIVITY_OBJ_ID;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_GENERATOR_ID;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_GENERATOR_ID_2;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.DEFAULT_GENERATOR_NAME;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities.allActivities;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities.hasActivities;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities.hasNoActivities;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities.withActivityObject;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities.withApplication;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities.withTitle;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Activities.withUsername;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.ActivityObjects.withDisplayName;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.ActivityObjects.withId;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.Matchers.Applications.hasApplications;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.activity;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.activityObject;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.application;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.image;
import static com.atlassian.streams.thirdparty.ThirdPartyStreams.user;
import static com.atlassian.streams.thirdparty.api.ActivityQuery.all;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(ActiveObjectsJUnitRunner.class)
@Data(ThirdPartyStreamsDatabaseUpdater.class)
@Jdbc(Hsql.class)
public class ActivityServiceActiveObjectsImplTest
{
    private static final String BASE_URI = "http://jira";
    private static final String STRM_PROJECT_KEY = "STRM";
    private static final String STRM_ISSUE_KEY = "STRM-123";
    private static final URI STRM_PROJECT_URI = URI.create(BASE_URI + "/browse/STRM");
    private static final URI STRM_ISSUE_URI = URI.create(BASE_URI + "/browse/STRM-123");
    private static final String AO_PROJECT_KEY = "AO";
    private static final String AO_ISSUE_KEY = "AO-456";
    private static final URI AO_PROJECT_URI = URI.create(BASE_URI + "/browse/AO");
    private static final URI AO_ISSUE_URI = URI.create(BASE_URI + "/browse/AO-456");
    private static final String DUMB_PROJECT_KEY = "DUMB";
    private static final String DUMB_ISSUE_KEY = "DUMB-789";
    private static final URI DUMB_PROJECT_URI = URI.create(BASE_URI + "/browse/DUMB");
    private static final URI DUMB_ISSUE_URI = URI.create(BASE_URI + "/browse/DUMB-789");

    private static final String ALT_BASE_URI = "http://schmira";
    private static final URI ALT_STRM_PROJECT_URI = URI.create(ALT_BASE_URI + "/browse/STRM");
    private static final URI ALT_STRM_ISSUE_URI = URI.create(ALT_BASE_URI + "/browse/STRM-123");

    private static final URI PROJECT_OBJECT_TYPE = URI.create("http://object/type/of/jira/project");
    private static final URI ISSUE_OBJECT_TYPE = URI.create("http://object/type/of/jira/issue");

    private static final String USERNAME = "user";
    private static final String USER_FULL_NAME = "You Sir";
    private static final URI USER_PROFILE_PAGE_URI = URI.create("http://my/page");
    private static final URI USER_PROFILE_IMG_URI = URI.create("http://my/face");
    private static final String ANON_USERNAME = "anonymous";
    private static final String ANON_USER_FULL_NAME = "Anony Mouse";
    private static final URI ANON_USER_PROFILE_IMG_URI = URI.create("http://people/anon.jpg");

    private ActiveObjects ao;
    private EntityManager entityManager;
    private ActivityService service;
    private ActivityServiceActiveObjectsImpl serviceImpl;
    private MockFeedContentSanitizer sanitizer;
    private EntityAssociationProviders entityAssociationProviders;
    private UserManager userManager;
    private UserProfileAccessor userProfileAccessor;
    private UserProfile defaultUserProfile;
    private com.atlassian.sal.api.user.UserProfile userProfile;
    private ApplicationProperties applicationProperties;
    private com.atlassian.sal.api.user.UserProfile salUserProfile;

    @Before
    public void setupService()
    {
        assertNotNull(entityManager);

        sanitizer = new MockFeedContentSanitizer();

        entityAssociationProviders = mock(EntityAssociationProviders.class);

        userManager = mock(UserManager.class);
        userProfile = mock(com.atlassian.sal.api.user.UserProfile.class);

        userProfileAccessor = mock(UserProfileAccessor.class);
        defaultUserProfile = new UserProfile.Builder(ANON_USERNAME)
            .fullName(ANON_USER_FULL_NAME)
            .profilePictureUri(some(ANON_USER_PROFILE_IMG_URI))
            .build();
        when(userProfileAccessor.getUserProfile(any(), any())).thenReturn(null);
        when(userProfileAccessor.getAnonymousUserProfile(any())).thenReturn(defaultUserProfile);
        salUserProfile = mock(com.atlassian.sal.api.user.UserProfile.class);

        when(entityAssociationProviders.getEntityURI(any()))
            .thenReturn(none(URI.class));
        when(entityAssociationProviders.getFilterKey(any()))
            .thenReturn(none(String.class));

        applicationProperties = mock(ApplicationProperties.class);

        when(applicationProperties.getBaseUrl()).thenReturn("http://localhost:3390/streams");

        setupEntityAssociations(STRM_PROJECT_KEY, STRM_PROJECT_URI, STRM_ISSUE_KEY, STRM_ISSUE_URI, true, true);
        setupEntityAssociations(AO_PROJECT_KEY, AO_PROJECT_URI, AO_ISSUE_KEY, AO_ISSUE_URI, true, true);
        setupEntityAssociations(DUMB_PROJECT_KEY, DUMB_PROJECT_URI, DUMB_ISSUE_KEY, DUMB_ISSUE_URI, true, true);

        ao = new TestActiveObjects(entityManager);
        serviceImpl = new ActivityServiceActiveObjectsImpl(ao, entityAssociationProviders, sanitizer,
                                                           userManager, userProfileAccessor, applicationProperties);
        service = new ActivityServiceDelegator(serviceImpl);
    }

    private void setupEntityAssociations(String projectKey, URI projectUri, String issueKey, URI issueUri, boolean canView, boolean canEdit)
    {
        EntityIdentifier projId = new EntityIdentifier(PROJECT_OBJECT_TYPE, projectKey, projectUri);
        EntityIdentifier issueId = new EntityIdentifier(ISSUE_OBJECT_TYPE, issueKey, issueUri);
        when(entityAssociationProviders.getEntityAssociations(projectUri)).thenReturn(ImmutableList.of(projId));
        when(entityAssociationProviders.getEntityAssociations(URI.create(projectKey))).thenReturn(ImmutableList.of(projId));
        when(entityAssociationProviders.getEntityAssociations(issueUri)).thenReturn(ImmutableList.of(issueId, projId));
        when(entityAssociationProviders.getEntityAssociations(URI.create(issueKey))).thenReturn(ImmutableList.of(issueId, projId));

        when(entityAssociationProviders.getEntityURI(projId)).thenReturn(some(projectUri));
        when(entityAssociationProviders.getEntityURI(issueId)).thenReturn(some(issueUri));

        when(entityAssociationProviders.getFilterKey(projId)).thenReturn(some(PROJECT_KEY));
        when(entityAssociationProviders.getFilterKey(issueId)).thenReturn(some(ISSUE_KEY.getKey()));

        when(entityAssociationProviders.getCurrentUserViewPermission(projId)).thenReturn(canView);
        when(entityAssociationProviders.getCurrentUserViewPermission(issueId)).thenReturn(canView);
        when(entityAssociationProviders.getCurrentUserEditPermission(projId)).thenReturn(canEdit);
        when(entityAssociationProviders.getCurrentUserEditPermission(issueId)).thenReturn(canEdit);
    }

    @Test
    public void assertThatActivitiesReturnsEmptyListWhenNoActivityHasBeenAdded()
    {
        assertThat(service.activities(all()), hasNoActivities());
    }

    @Test
    public void assertThatPostingAnActivitySuccessfullyStoresItInActiveObjects()
    {
        postValidActivity(activity());
        assertThat(size(service.activities(all())), is(equalTo(1)));
    }

    @Test
    public void assertThatPostingAnActivityWithRemoteUsernameReturnsActivityWithPoster()
    {
        when(userManager.getRemoteUsername()).thenReturn("user");
        Activity activity = postValidActivity(activity());
        assertThat(activity.getPoster(), is(equalTo(some("user"))));
    }

    @Test
    public void assertThatPostingAnActivityWithNoRemoteUsernameReturnsActivityWithNoPoster()
    {
        when(userManager.getRemoteUsername()).thenReturn(null);
        Activity activity = postValidActivity(activity());
        assertThat(activity.getPoster(), is(equalTo(Option.none())));
    }

    @Test
    public void assertThatPostingAnActivitySuccessfullyReturnsTheActivityWithPopulatedActivityId()
    {
        Activity activity = postValidActivity(activity());
        assertThat(activity.getActivityId(), is(not(equalTo(none(Long.class)))));
    }

    @Test
    public void assertThatPostingMultipleActivitiesSuccessfullyStoresAllInActiveObjects()
    {
        postValidActivity(activity(URI.create("http://www.example.org/activity1")));
        postValidActivity(activity(URI.create("http://www.example.org/activity2")));
        postValidActivity(activity(URI.create("http://www.example.org/activity3")));
        assertThat(size(service.activities(all())), is(equalTo(3)));
    }

    @Test
    public void assertThatProviderCanSetUserProfileFromRegisteredUsername()
    {
        prepareRegisteredUser(USERNAME, USER_FULL_NAME, USER_PROFILE_PAGE_URI, USER_PROFILE_IMG_URI);

        postValidActivity(activity(application(),
                                   new DateTime(),
                                   new UserProfile.Builder(USERNAME).build())
                          .id(some(DEFAULT_ACTIVITY_ID)));

        Activity activity = Iterables.getOnlyElement(service.activities(all()));
        assertTrue(activity.isRegisteredUser());
        assertThat(activity.getUser().getUsername(), equalTo(USERNAME));
    }

    @Test
    public void assertThatUserFullNameIsOverriddenInPostResultIfRegisteredUsernameIsSpecified()
    {
        prepareRegisteredUser(USERNAME, USER_FULL_NAME, USER_PROFILE_PAGE_URI, USER_PROFILE_IMG_URI);

        Activity activity = postValidActivity(activity(application(),
                                                       new DateTime(),
                                                       new UserProfile.Builder(USERNAME)
                                                           .fullName("SomeOtherName")
                                                           .build())
                                              .id(some(DEFAULT_ACTIVITY_ID)));

        assertThat(activity.getUser().getFullName(), equalTo(USER_FULL_NAME));
    }

    @Test
    public void assertThatUserProfilePageIsOverriddenInPostResultIfRegisteredUsernameIsSpecified()
    {
        prepareRegisteredUser(USERNAME, USER_FULL_NAME, USER_PROFILE_PAGE_URI, USER_PROFILE_IMG_URI);

        Activity activity = postValidActivity(activity(application(),
                                                       new DateTime(),
                                                       new UserProfile.Builder(USERNAME)
                                                           .profilePageUri(some(URI.create("http://someone/elses/page")))
                                                           .build())
                                              .id(some(DEFAULT_ACTIVITY_ID)));

        assertThat(activity.getUser().getProfilePageUri(), equalTo(some(USER_PROFILE_PAGE_URI)));
    }

    @Test
    public void assertThatUserImageIsOverriddenInPostResultIfRegisteredUsernameIsSpecified()
    {
        prepareRegisteredUser(USERNAME, USER_FULL_NAME, USER_PROFILE_PAGE_URI, USER_PROFILE_IMG_URI);

        Activity activity = postValidActivity(activity(application(),
                                                       new DateTime(),
                                                       new UserProfile.Builder(USERNAME)
                                                           .profilePictureUri(some(URI.create("http://someone/elses/face")))
                                                           .build())
                                              .id(some(DEFAULT_ACTIVITY_ID)));

        assertThat(activity.getUser().getProfilePictureUri(), equalTo(some(USER_PROFILE_IMG_URI)));
    }

    @Test
    public void assertThatUserFullNameIsHyperlinkedInTitle()
    {
        prepareRegisteredUser(USERNAME, USER_FULL_NAME, USER_PROFILE_PAGE_URI, USER_PROFILE_IMG_URI);

        Activity activity = postValidActivity(activity(application(),
                                                       new DateTime(),
                                                       new UserProfile.Builder(USERNAME).build())
                                              .id(some(DEFAULT_ACTIVITY_ID))
                                              .title(some(makeHtmlWithMentions(USER_FULL_NAME))));

        assertThat(activity.getTitle(), equalTo(some(makeHtmlWithHyperlinkedMentions(USER_FULL_NAME, USER_PROFILE_PAGE_URI))));
    }

    @Test
    public void assertThatUserFullNameIsHyperlinkedInContent()
    {
        prepareRegisteredUser(USERNAME, USER_FULL_NAME, USER_PROFILE_PAGE_URI, USER_PROFILE_IMG_URI);

        Activity activity = postValidActivity(activity(application(),
                                                       new DateTime(),
                                                       new UserProfile.Builder(USERNAME).build())
                                              .id(some(DEFAULT_ACTIVITY_ID))
                                              .content(some(makeHtmlWithMentions(USER_FULL_NAME))));

        assertThat(activity.getContent(), equalTo(some(makeHtmlWithHyperlinkedMentions(USER_FULL_NAME, USER_PROFILE_PAGE_URI))));
    }

    @Test
    public void assertThatDefaultUserNameIsUsedIfUserNameWasEmpty()
    {
        postValidActivity(activity(application(), new DateTime(), "").id(some(DEFAULT_ACTIVITY_ID)));

        Activity activity = Iterables.getOnlyElement(service.activities(all()));
        assertThat(activity.getUser().getUsername(), equalTo(ANON_USERNAME));
    }

    @Test
    public void assertThatDefaultUserFullNameIsUsedIfUserFullNameWasEmpty()
    {
        postValidActivity(activity(application(), new DateTime(),
                                   new UserProfile.Builder("user").fullName("").build())
                          .id(some(DEFAULT_ACTIVITY_ID)));

        Activity activity = Iterables.getOnlyElement(service.activities(all()));
        assertThat(activity.getUser().getFullName(), equalTo(ANON_USER_FULL_NAME));
    }

    @Test
    public void assertThatDefaultUserImageIsUsedIfUserHadNoImageUrl()
    {
        postValidActivity(activity(application(), new DateTime(), "user").id(some(DEFAULT_ACTIVITY_ID)));

        Activity activity = Iterables.getOnlyElement(service.activities(all()));
        assertThat(activity.getUser().getProfilePictureUri(), equalTo(some(ANON_USER_PROFILE_IMG_URI)));
    }

    @Test
    public void assertThatUserFullNameCanBeOverriddenEvenIfUserNameIsEmpty()
    {
        postValidActivity(activity(application(), new DateTime(),
                                   new UserProfile.Builder("").fullName("User").build())
                         .id(some(DEFAULT_ACTIVITY_ID)));

        Activity activity = Iterables.getOnlyElement(service.activities(all()));
        assertThat(activity.getUser().getFullName(), equalTo("User"));
    }

    @Test
    public void assertThatUserImageCanBeOverriddenEvenIfUserIsUnregistered()
    {
        URI pictureUri = URI.create("http://my/face");
        postValidActivity(activity(application(), new DateTime(),
                                   new UserProfile.Builder("user").profilePictureUri(some(pictureUri)).build())
                          .id(some(DEFAULT_ACTIVITY_ID)));

        Activity activity = Iterables.getOnlyElement(service.activities(all()));
        assertThat(activity.getUser().getProfilePictureUri(), equalTo(some(pictureUri)));
    }

    @Test
    public void assertThatSanitizeIsNotCalledWhenActivityPostedHasNoContentOrTitleOrSummary()
    {
        postValidActivity(activity());
        assertThat(sanitizer.called(), is(equalTo(0)));
    }

    @Test
    public void assertThatSanitizeIsCalledWhenActivityPostedHasContent()
    {
        postValidActivity(activity().content(some(html("this is my content"))));
        assertThat(sanitizer.called(), is(equalTo(1)));
    }

    @Test
    public void assertThatSanitizeIsCalledWhenActivityPostedHasTitle()
    {
        postValidActivity(activity().title(some(html("this is my title"))));
        assertThat(sanitizer.called(), is(equalTo(1)));
    }

    @Test
    public void assertThatSanitizeIsCalledWhenActivityObjectHasSummary()
    {
        postValidActivity(activity().object(activityObject().summary(some(html("this is my summary"))).build()));
        assertThat(sanitizer.called(), is(equalTo(1)));
    }

    @Test
    public void assertThatSanitizeIsCalledThriceWhenActivityHasTitleAndContentAndSummary()
    {
        postValidActivity(activity()
                          .title(some(html("this is my title")))
                          .content(some(html("this is my content")))
                          .object(activityObject().summary(some(html("this is my summary"))).build()));
        assertThat(sanitizer.called(), is(equalTo(3)));
    }

    @Test
    public void assertThatHtmlMarkupIsStrippedFromObjectDisplayName()
    {
        postValidActivity(activity()
                          .object(activityObject().displayName(some("This <em>is</em> an <font color=\"green\">object</font>")).build()));
        assertThat(service.activities(all()), hasActivities(withActivityObject(withDisplayName(equalTo("This is an object")))));
    }

    @Test
    public void assertThatIssueKeyInTargetUrlIsTransformedToIssueUrlInPostResult()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_ISSUE_KEY))).build()));

        assertThat(activity.getTarget().get().getUrl(), equalTo(some(STRM_ISSUE_URI)));
    }

    @Test
    public void assertThatProjectKeyInTargetUrlIsTransformedToProjectUrlInPostResult()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_PROJECT_KEY))).build()));
        assertThat(activity.getTarget().get().getUrl(), equalTo(some(STRM_PROJECT_URI)));
    }

    @Test
    public void assertThatIssueKeyInTargetUrlSetsTargetObjectTypeInPostResult()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_ISSUE_KEY))).build()));
        assertThat(activity.getTarget().get().getType(), equalTo(some(ISSUE_OBJECT_TYPE)));
    }

    @Test
    public void assertThatProjectKeyInTargetUrlSetsTargetObjectTypeInPostResult()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_PROJECT_KEY))).build()));
        assertThat(activity.getTarget().get().getType(), equalTo(some(PROJECT_OBJECT_TYPE)));
    }

    @Test
    public void assertThatTargetUrlForIssueIsRecomputedForEachQuery()
    {
        postValidActivity(activity().target(activityObject().url(some(URI.create(STRM_ISSUE_KEY))).build()));

        when(entityAssociationProviders.getEntityURI(new EntityIdentifier(ISSUE_OBJECT_TYPE, STRM_ISSUE_KEY,
                                                                          STRM_ISSUE_URI)))
            .thenReturn(some(ALT_STRM_ISSUE_URI));

        Activity queriedActivity = getOnlyElement(service.activities(all()));

        assertThat(queriedActivity.getTarget().get().getUrl(), equalTo(some(ALT_STRM_ISSUE_URI)));
    }

    @Test
    public void assertThatTargetUrlForProjectIsRecomputedForEachQuery()
    {
        postValidActivity(activity().target(activityObject().url(some(URI.create(STRM_PROJECT_KEY))).build()));

        when(entityAssociationProviders.getEntityURI(new EntityIdentifier(PROJECT_OBJECT_TYPE, STRM_PROJECT_KEY,
                                                                          STRM_PROJECT_URI)))
            .thenReturn(some(ALT_STRM_PROJECT_URI));

        Activity queriedActivity = getOnlyElement(service.activities(all()));

        assertThat(queriedActivity.getTarget().get().getUrl(), equalTo(some(ALT_STRM_PROJECT_URI)));
    }

    @Test
    public void assertThatIssueKeyIsHyperlinkedInTitle()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_ISSUE_KEY))).build())
                                              .title(some(makeHtmlWithMentions(STRM_ISSUE_KEY))));

        assertThat(activity.getTitle(), equalTo(some(makeHtmlWithHyperlinkedMentions(STRM_ISSUE_KEY, STRM_ISSUE_URI))));
    }

    @Test
    public void assertThatProjectKeyIsHyperlinkedInTitle()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_PROJECT_KEY))).build())
                                              .title(some(makeHtmlWithMentions(STRM_PROJECT_KEY))));

        assertThat(activity.getTitle(), equalTo(some(makeHtmlWithHyperlinkedMentions(STRM_PROJECT_KEY, STRM_PROJECT_URI))));
    }

    @Test
    public void assertThatIssueKeyIsHyperlinkedInContent()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_ISSUE_KEY))).build())
                                              .content(some(makeHtmlWithMentions(STRM_ISSUE_KEY))));

        assertThat(activity.getContent(), equalTo(some(makeHtmlWithHyperlinkedMentions(STRM_ISSUE_KEY, STRM_ISSUE_URI))));
    }

    @Test
    public void assertThatProjectKeyIsHyperlinkedInContent()
    {
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_PROJECT_KEY))).build())
                                              .content(some(makeHtmlWithMentions(STRM_PROJECT_KEY))));

        assertThat(activity.getContent(), equalTo(some(makeHtmlWithHyperlinkedMentions(STRM_PROJECT_KEY, STRM_PROJECT_URI))));
    }

    @Test
    public void assertThatIssueKeyIsHyperlinkedInContentWhenUserCanViewButNotEdit()
    {
        when(entityAssociationProviders.getCurrentUserEditPermission(
            new EntityIdentifier(ISSUE_OBJECT_TYPE, STRM_ISSUE_KEY, STRM_ISSUE_URI))).thenReturn(false);
        when(entityAssociationProviders.getCurrentUserEditPermission(
            new EntityIdentifier(PROJECT_OBJECT_TYPE, STRM_PROJECT_KEY, STRM_PROJECT_URI))).thenReturn(false);
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_ISSUE_KEY))).build())
                                              .content(some(makeHtmlWithMentions(STRM_ISSUE_KEY))));

        assertThat(activity.getContent(), equalTo(some(makeHtmlWithHyperlinkedMentions(STRM_ISSUE_KEY, STRM_ISSUE_URI))));
    }

    @Test
    public void assertThatProjectKeyIsHyperlinkedInContentWhenUserCanViewButNotEdit()
    {
        when(entityAssociationProviders.getCurrentUserEditPermission(
            new EntityIdentifier(ISSUE_OBJECT_TYPE, STRM_ISSUE_KEY, STRM_ISSUE_URI))).thenReturn(false);
        when(entityAssociationProviders.getCurrentUserEditPermission(
            new EntityIdentifier(PROJECT_OBJECT_TYPE, STRM_PROJECT_KEY, STRM_PROJECT_URI))).thenReturn(false);
        Activity activity = postValidActivity(activity()
                                              .target(activityObject().url(some(URI.create(STRM_PROJECT_KEY))).build())
                                              .content(some(makeHtmlWithMentions(STRM_PROJECT_KEY))));

        assertThat(activity.getContent(), equalTo(some(makeHtmlWithHyperlinkedMentions(STRM_PROJECT_KEY, STRM_PROJECT_URI))));
    }

    @Test
    public void assertThatActivityListReturnsPostedActivities()
    {
        postValidActivity(activity().title(some(html("this is a title!"))));
        assertThat(service.activities(all()), allActivities(withTitle(equalTo(html("this is a title!")))));
    }

    @Test
    public void assertThatActivityListSuccessfullyReturnsPostedActivityObjects()
    {
        postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID))
                          .object(activityObject().id(some(DEFAULT_ACTIVITY_OBJ_ID)).build()));
        assertThat(service.activities(all()), hasActivities(withActivityObject(withId(equalTo(DEFAULT_ACTIVITY_OBJ_ID)))));
    }

    @Test
    public void assertThatActivityListSuccessfullyReturnsPostedApplication()
    {
        postValidActivity(activity());
        assertThat(service.activities(all()), hasActivities(withApplication(Applications.withId(equalTo(DEFAULT_GENERATOR_ID)))));
    }

    @Test
    public void assertThatGeneratorsCanBeSuccessfullyRetrieved()
    {
        postValidActivity(activity());

        assertThat(service.applications(), Matchers.iterableWithSize(1));
    }

    @Test
    public void assertThatRetrievedGeneratorsAreCorrect()
    {
        postValidActivity(activity());

        assertThat(getOnlyElement(service.applications()).getId(), equalTo(DEFAULT_GENERATOR_ID));
    }

    @Test
    public void assertThatPostingMultipleActivitiesOfDifferentGeneratorIdAddsMultipleGeneratorObject()
    {
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID),
                                   new DateTime(), user()).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID_2),
                                   new DateTime(), user()).id(some(DEFAULT_ACTIVITY_ID)));

        assertThat(service.applications(), Matchers.iterableWithSize(2));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatGeneratorListReturnsPostedGeneratorsSuccessfully()
    {
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID),
                                   new DateTime(), user()).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID_2),
                                   new DateTime(), user()).id(some(DEFAULT_ACTIVITY_ID)));

        assertThat(service.applications(), hasApplications(Applications.withId(equalTo(DEFAULT_GENERATOR_ID_2)),
                                                           Applications.withId(equalTo(DEFAULT_GENERATOR_ID))));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatGeneratorListReturnsUniqueIdAndNameCombination()
    {
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID),
                                   new DateTime(), user()).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID),
                                   new DateTime(), user()).id(some(DEFAULT_ACTIVITY_ID)));

        assertThat(service.applications(), hasApplications(allOf(Applications.withId(equalTo(DEFAULT_GENERATOR_ID)),
                                                                 Applications.withDisplayName(equalTo(DEFAULT_GENERATOR_NAME)))));
    }

    @Test
    public void assertThatPostingAnActivityAlsoAddsTheActorEntity()
    {
        postValidActivity(activity());

        assertThat(ao.find(ActorEntity.class), is(not(Matchers.<Object>emptyArray())));
    }

    @Test
    public void assertThatPostingAnActivityAlsoAddsTheObjectEntity()
    {
        postValidActivity(activity().object(activityObject().build()));

        assertThat(ao.find(ObjectEntity.class), is(not(Matchers.<Object>emptyArray())));
    }

    @Test
    public void assertThatPostingAnActivityAlsoAddsTheTargetEntity()
    {
        postValidActivity(activity().target(activityObject().build()));

        assertThat(ao.find(TargetEntity.class), is(not(Matchers.<Object>emptyArray())));
    }

    @Test
    public void assertThatPostingAnActivityAlsoAddsTheMediaLinkEntity()
    {
        postValidActivity(activity().icon(image().build()));

        assertThat(ao.find(MediaLinkEntity.class), is(not(Matchers.<Object>emptyArray())));
    }

    @Test
    public void assertThatActivityListWithMinDateExcludesEarlierActivity()
    {
        DateTime date1 = new DateTime();
        DateTime date2 = date1.plusMinutes(1);
        postValidActivity(activity(application(), date1, user()).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date2, user()).id(some(DEFAULT_ACTIVITY_ID_2)));
        ActivityQuery query = ActivityQuery.builder().startDate(Option.some(date2.toDate())).build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(size(activities), equalTo(1));
        assertThat(activities, allActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_2))));
    }

    @Test
    public void assertThatActivityListWithMaxDateExcludesLaterActivity()
    {
        DateTime date1 = new DateTime();
        DateTime date2 = date1.plusMinutes(1);
        postValidActivity(activity(application(), date1, user()).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date2, user()).id(some(DEFAULT_ACTIVITY_ID_2)));
        ActivityQuery query = ActivityQuery.builder().endDate(Option.some(date2.toDate())).build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(size(activities), equalTo(1));
        assertThat(activities, allActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID))));
    }

    @Test
    public void assertThatActivityListWithUserNameExcludesOtherUsers()
    {
        DateTime date = new DateTime();
        String goodUser = "good";
        String badUser = "bad";
        prepareRegisteredUser(goodUser);
        prepareRegisteredUser(badUser);
        postValidActivity(activity(application(), date, goodUser).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date, badUser).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date, goodUser).id(some(DEFAULT_ACTIVITY_ID)));
        ActivityQuery query = ActivityQuery.builder().userNames(ImmutableList.of(goodUser)).build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(size(activities), equalTo(2));
        assertThat(activities, allActivities(withUsername(equalTo(goodUser))));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatActivityListWithMultipleUserNamesIncludesOnlySpecifiedUsers()
    {
        DateTime date = new DateTime();
        String goodUser1 = "good";
        String goodUser2 = "better";
        String badUser = "bad";
        prepareRegisteredUser(goodUser1);
        prepareRegisteredUser(goodUser2);
        prepareRegisteredUser(badUser);
        postValidActivity(activity(application(), date, goodUser1).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date, badUser).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date, goodUser2).id(some(DEFAULT_ACTIVITY_ID)));
        ActivityQuery query = ActivityQuery.builder().userNames(ImmutableList.of(goodUser1, goodUser2)).build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(size(activities), equalTo(2));
        assertThat(activities, allActivities(withUsername(anyOf(equalTo(goodUser1), equalTo(goodUser2)))));
    }

    @Test
    public void assertThatActivityListWithExcludedUserNamesExcludesThoseUsers()
    {
        DateTime date = new DateTime();
        String goodUser = "good";
        String badUser1 = "bad";
        String badUser2 = "worse";
        prepareRegisteredUser(goodUser);
        prepareRegisteredUser(badUser1);
        prepareRegisteredUser(badUser2);
        postValidActivity(activity(application(), date, badUser1).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date, goodUser).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(), date, badUser2).id(some(DEFAULT_ACTIVITY_ID)));
        ActivityQuery query = ActivityQuery.builder().excludeUserNames(ImmutableList.of(badUser1, badUser2)).build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(size(activities), equalTo(1));
        assertThat(activities, allActivities(withUsername(equalTo(goodUser))));
    }

    @Test
    public void assertThatActivityListWithProjectKeyExcludesOtherProjects()
    {
        createActivitiesWithAssociatedIssues(DEFAULT_ACTIVITY_ID, STRM_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_2, DUMB_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_3, AO_ISSUE_URI);

        ActivityQuery query = ActivityQuery.builder()
            .addEntityFilter(PROJECT_KEY, STRM_PROJECT_KEY)
            .addEntityFilter(PROJECT_KEY, AO_PROJECT_KEY)
            .build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(activities, hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID)),
                                             Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_3))));
    }

    @Test
    public void assertThatActivityListWithNotProjectKeyExcludesThoseProjects()
    {
        createActivitiesWithAssociatedIssues(DEFAULT_ACTIVITY_ID, STRM_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_2, DUMB_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_3, AO_ISSUE_URI);

        ActivityQuery query = ActivityQuery.builder()
            .addExcludeEntityFilter(PROJECT_KEY, STRM_PROJECT_KEY)
            .addExcludeEntityFilter(PROJECT_KEY, AO_PROJECT_KEY)
            .build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(activities, hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_2))));
    }

    @Test
    public void assertThatActivityListWithIssueKeyExcludesOtherIssues()
    {
        createActivitiesWithAssociatedIssues(DEFAULT_ACTIVITY_ID, STRM_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_2, DUMB_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_3, AO_ISSUE_URI);

        ActivityQuery query = ActivityQuery.builder()
            .addEntityFilter(ISSUE_KEY.getKey(), STRM_ISSUE_KEY)
            .addEntityFilter(ISSUE_KEY.getKey(), AO_ISSUE_KEY)
            .build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(activities, hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID)),
                                             Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_3))));
    }

    @Test
    public void assertThatActivityListWithNotIssueKeyExcludesThoseIssues()
    {
        createActivitiesWithAssociatedIssues(DEFAULT_ACTIVITY_ID, STRM_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_2, DUMB_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_3, AO_ISSUE_URI);

        ActivityQuery query = ActivityQuery.builder()
            .addExcludeEntityFilter(ISSUE_KEY.getKey(), STRM_ISSUE_KEY)
            .addExcludeEntityFilter(ISSUE_KEY.getKey(), AO_ISSUE_KEY)
            .build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(activities, hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_2))));
    }

    @Test
    public void assertThatActivityListExcludesIssuesIfUserHasNoViewPermission()
    {
        createActivitiesWithAssociatedIssues(DEFAULT_ACTIVITY_ID, STRM_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_2, DUMB_ISSUE_URI,
                                             DEFAULT_ACTIVITY_ID_3, AO_ISSUE_URI);

        when(entityAssociationProviders.getCurrentUserViewPermission(
            new EntityIdentifier(ISSUE_OBJECT_TYPE, STRM_ISSUE_KEY, STRM_ISSUE_URI))).thenReturn(false);
        when(entityAssociationProviders.getCurrentUserViewPermission(
            new EntityIdentifier(ISSUE_OBJECT_TYPE, AO_ISSUE_KEY, AO_ISSUE_URI))).thenReturn(false);

        Iterable<Activity> activities = service.activities(ActivityQuery.all());
        assertThat(activities, hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_2))));
    }

    @Test
    public void assertThatActivityListWithGeneratorKeyExcludesOtherGenerators()
    {
        DateTime date = new DateTime();
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID),
                                   date, user()).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID_2),
                                   date, user()).id(some(DEFAULT_ACTIVITY_ID_2)));
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID_2),
                                   date, user()).id(some(DEFAULT_ACTIVITY_ID_3)));

        ActivityQuery query = ActivityQuery.builder()
            .providerKeys(ImmutableList.of(DEFAULT_GENERATOR_ID + "@" + DEFAULT_GENERATOR_NAME))
            .build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(activities, hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID))));
    }

    @Test
    public void assertThatActivityListWithExcludeGeneratorKeyExcludesThatGenerator()
    {
        DateTime date = new DateTime();
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID),
                                   date, user()).id(some(DEFAULT_ACTIVITY_ID)));
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID_2),
                                   date, user()).id(some(DEFAULT_ACTIVITY_ID_2)));
        postValidActivity(activity(application(DEFAULT_GENERATOR_NAME, DEFAULT_GENERATOR_ID_2),
                                   date, user()).id(some(DEFAULT_ACTIVITY_ID_3)));

        ActivityQuery query = ActivityQuery.builder()
            .excludeProviderKeys(ImmutableList.of(DEFAULT_GENERATOR_ID + "@" + DEFAULT_GENERATOR_NAME))
            .build();

        Iterable<Activity> activities = service.activities(query);
        assertThat(activities, hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_2)),
                                             Activities.withId(equalTo(DEFAULT_ACTIVITY_ID_3))));
    }

    @Test
    public void assertThatDeletingWithAValidActivityIdIsSuccessful()
    {
        Activity activity = postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID)));
        assertTrue(service.delete(activity.getActivityId().get()));
    }

    @Test
    public void assertThatDeletingWithAnInvalidActivityIdIsNotSuccessful()
    {
        postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID)));
        assertFalse(service.delete(999));
    }

    @Test
    public void assertThatDeletingAnActivityRemovesTheActivityFromTheList()
    {
        Activity activity = postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID)));
        service.delete(activity.getActivityId().get());

        assertThat(service.activities(all()), hasNoActivities());
    }

    @Test
    public void assertThatDeletingAnActivityAlsoRemovesTheApplicationEntity()
    {
        Activity activity = postValidActivity(activity());
        service.delete(activity.getActivityId().get());

        assertThat(service.applications(), is(Matchers.emptyIterable()));
    }

    @Test
    public void assertThatDeletingAnActivityAlsoRemovesTheActorEntity()
    {
        Activity activity = postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID)));
        service.delete(activity.getActivityId().get());

        assertThat(ao.find(ActorEntity.class), is(Matchers.emptyArray()));
    }

    @Test
    public void assertThatDeletingAnActivityAlsoRemovesTheObjectEntity()
    {
        Activity activity = postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID))
                                              .object(activityObject().build()));
        service.delete(activity.getActivityId().get());

        assertThat(ao.find(ObjectEntity.class), is(Matchers.emptyArray()));
    }

    @Test
    public void assertThatDeletingAnActivityAlsoRemovesTheTargetEntity()
    {
        Activity activity = postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID))
                                              .target(activityObject().build()));
        service.delete(activity.getActivityId().get());

        assertThat(ao.find(TargetEntity.class), is(Matchers.emptyArray()));
    }

    @Test
    public void assertThatDeletingAnActivityAlsoRemovesTheMediaLinkEntity()
    {
        Activity activity = postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID))
                                              .icon(image().build()));
        service.delete(activity.getActivityId().get());

        assertThat(ao.find(MediaLinkEntity.class), is(Matchers.emptyArray()));
    }

    @Test
    public void assertThatInvalidActivityInDatabaseIsSilentlyOmittedFromResults()
    {
        postValidActivity(activity().id(some(DEFAULT_ACTIVITY_ID)));
        Activity activity2 = postValidActivity(activity(DEFAULT_ACTIVITY_ID_2));

        // hack the database entity for activity2 to make it invalid
        ActivityEntity entity2 = ao.get(ActivityEntity.class, activity2.getActivityId().get());
        entity2.setGeneratorDisplayName(null);
        entity2.save();

        assertThat(service.activities(all()), hasActivities(Activities.withId(equalTo(DEFAULT_ACTIVITY_ID))));
    }

    private void prepareRegisteredUser(String username)
    {
        prepareRegisteredUser(username, username.toUpperCase(), USER_PROFILE_PAGE_URI, USER_PROFILE_IMG_URI);
    }

    private void prepareRegisteredUser(String username, String fullName, URI profileUri, URI pictureUri)
    {
        UserProfile profile = new UserProfile.Builder(username)
                .fullName(fullName)
                .profilePageUri(some(profileUri))
                .profilePictureUri(some(pictureUri))
                .build();

        when(userProfileAccessor.getUserProfile(any(URI.class), eq(username))).thenReturn(profile);

        when(salUserProfile.getUsername()).thenReturn(username);
        when(userManager.getUserProfile(username)).thenReturn(salUserProfile);
    }

    private void createActivitiesWithAssociatedIssues(URI... args)
    {
        // create in reverse chronological order so the query results will come out in the same order
        DateTime time = new DateTime();
        for (int i = 0; i < args.length; i += 2)
        {
            postValidActivity(Activity.builder(application(), time, ThirdPartyStreams.user()).id(some(args[i]))
                              .target(activityObject().url(some(args[i + 1])).build()));
            time = time.minusHours(1);
        }
    }

    private Activity postValidActivity(Activity.Builder builder)
    {
        Either<ValidationErrors, Activity> ret = builder.build();
        if (ret.isLeft())
        {
            fail("Unexpected ValidationError: " + ret.left().get());
        }
        return service.postActivity(ret.right().get());
    }

    private Html makeHtmlWithMentions(String text)
    {
        return html("I'm mentioning " + text + " and then I'm mentioning " + text + " again");
    }

    private Html makeHtmlWithHyperlinkedMentions(String text, URI uri)
    {
        String linkElement = "<a href=\"" + uri + "\">" + text + "</a>";
        return html(makeHtmlWithMentions(text).toString().replace(text, linkElement));
    }
}
