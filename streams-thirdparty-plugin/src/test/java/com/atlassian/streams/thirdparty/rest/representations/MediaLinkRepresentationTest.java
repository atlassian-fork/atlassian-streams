package com.atlassian.streams.thirdparty.rest.representations;

import org.junit.Test;

import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.TestData.INVALID_URI_STRING;
import static com.atlassian.streams.thirdparty.TestData.PICTURE_URI;
import static com.atlassian.streams.thirdparty.TestData.RELATIVE_URI;
import static com.atlassian.streams.thirdparty.TestData.assertNotValidationError;
import static com.atlassian.streams.thirdparty.TestData.assertValidationError;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MediaLinkRepresentationTest
{
    private MediaLinkRepresentation rep;
    
    @Test
    public void toImageCopiesUrl()
    {
        rep = MediaLinkRepresentation.builder(PICTURE_URI).build();
        assertThat(assertNotValidationError(rep.toImage()).getUrl(), equalTo(PICTURE_URI));
    }

    @Test
    public void toImageRejectsInvalidUrl()
    {
        rep = new MediaLinkRepresentation.Builder(INVALID_URI_STRING).build();
        assertValidationError(rep.toImage());
    }

    @Test
    public void toImageRejectsNullUrl()
    {
        // need to use ctor since this is a mandatory parameter in Builder
        rep = new MediaLinkRepresentation(null, null, null, null);
        assertValidationError(rep.toImage());
    }

    @Test
    public void toImageRejectsRelativeUrl()
    {
        rep = MediaLinkRepresentation.builder(RELATIVE_URI).build();
        assertValidationError(rep.toImage());
    }
    
    @Test
    public void toImageCopiesHeight()
    {
        Integer height = 100;
        rep = MediaLinkRepresentation.builder(PICTURE_URI).height(some(height)).build();
        assertThat(assertNotValidationError(rep.toImage()).getHeight(), equalTo(some(height)));
    }
    
    @Test
    public void toImageCopiesWidth()
    {
        Integer width = 100;
        rep = MediaLinkRepresentation.builder(PICTURE_URI).width(some(width)).build();
        assertThat(assertNotValidationError(rep.toImage()).getWidth(), equalTo(some(width)));
    }
}
