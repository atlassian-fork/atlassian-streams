package com.atlassian.streams.bamboo.builder;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.v2.build.trigger.ChildDependencyTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.CodeChangedTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.DependencyTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.ScheduledTriggerReason;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.atlassian.bamboo.v2.trigger.InitialBuildTriggerReason;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Link;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.NonEmptyIterable;
import com.atlassian.streams.api.common.NonEmptyIterables;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.bamboo.BambooRendererFactory;
import com.atlassian.streams.bamboo.UriProvider;
import com.atlassian.streams.spi.StreamsUriBuilder;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.google.common.collect.ImmutableList;

import java.net.URI;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.bamboo.BambooActivityObjectTypes.build;
import static com.atlassian.streams.bamboo.BambooActivityObjectTypes.job;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.fail;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.pass;
import static com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason.CHILD;
import static com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason.DEPENDANT;
import static com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason.INITIAL;
import static com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason.MANUAL;
import static com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason.SCHEDULED;
import static com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason.UNKNOWN;
import static com.atlassian.streams.bamboo.builder.ResultsSummaryStreamsEntryBuilder.StreamsTriggerReason.UPDATED;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Builder to help construct {@code StreamsEntry}s from {@code ResultsSummary}s.
 */
public class ResultsSummaryStreamsEntryBuilder implements BambooStreamsEntryBuilder<ResultsSummary>
{
    private final UriProvider uriProvider;
    private final UserProfileAccessor userProfileAccessor;
    private final BambooRendererFactory rendererFactory;
    private final WebResourceManager webResourceManager;
    private final UserManager userManager;

    public ResultsSummaryStreamsEntryBuilder(final UriProvider uriProvider,
                                             UserProfileAccessor userProfileAccessor,
                                             BambooRendererFactory rendererFactory,
                                             WebResourceManager webResourceManager,
                                             UserManager userManager)
    {
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        this.userProfileAccessor = checkNotNull(userProfileAccessor, "userProfileAccessor");
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.webResourceManager = checkNotNull(webResourceManager, "webResourceManager");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public URI buildUri(final URI baseUri, final ResultsSummary buildSummary)
    {
        return uriProvider.getBuildResultUri(baseUri, buildSummary);
    }

    public URI buildId(final URI baseUri, final StreamsUriBuilder uriBuilder, final ResultsSummary buildSummary)
    {
        return uriBuilder.setUrl(buildUri(baseUri, buildSummary).toASCIIString()).setTimestamp(buildSummary.getBuildCompletedDate()).getUri();
    }

    public ActivityObject buildActivityObject(final URI baseUri, final ResultsSummary buildSummary)
    {
        return new ActivityObject(ActivityObject.params()
            .id(buildId(baseUri, new StreamsUriBuilder(), buildSummary).toASCIIString())
            .activityObjectType(job())
            .title(some(getBuildName(buildSummary)))
            .alternateLinkUri(buildUri(baseUri, buildSummary)));
    }

    private String getBuildName(final ResultsSummary buildSummary)
    {
        return buildSummary.getPlanName();
    }

    private String getBuildPlanResultName(final ResultsSummary buildSummary)
    {
        final ImmutableChain chain = buildSummary.getImmutableChain();
        if (chain.hasMaster())
        {
            return escapeHtml4(chain.getProject().getName()) + " &rsaquo; " +
                   escapeHtml4(chain.getMaster().getBuildName()) + " &rsaquo; " +
                   "<span class=\"icon icon-branch\"/> " +
                   escapeHtml4(chain.getBuildName()) + " &rsaquo; #" +
                   buildSummary.getBuildNumber();
        }
        else
        {
            return escapeHtml4(chain.getProject().getName()) + " &rsaquo; " +
                   escapeHtml4(chain.getBuildName()) + " &rsaquo; #" +
                   buildSummary.getBuildNumber();
        }
    }

    public Iterable<String> buildCategory(final ResultsSummary buildSummary)
    {
        return buildSummary.isSuccessful() ? ImmutableList.of("build.successful") : ImmutableList.of("build.failed");
    }

    public ActivityVerb buildVerb(final ResultsSummary buildSummary)
    {
        return buildSummary.isSuccessful() ? pass() : fail();
    }

    public Option<ActivityObject> buildTarget(final URI baseUri, final ResultsSummary buildSummary)
    {
        return some(new ActivityObject(ActivityObject.params()
            .id(buildId(baseUri, new StreamsUriBuilder(), buildSummary).toASCIIString())
            .activityObjectType(build())
            .titleAsHtml(some(new Html(getBuildPlanResultName(buildSummary))))
            .alternateLinkUri(uriProvider.getBuildPlanResultUri(baseUri, buildSummary.getImmutableChain(), buildSummary))));
    }

    public NonEmptyIterable<UserProfile> getAuthors(final URI baseUri, final ResultsSummary buildSummary)
    {
        final Set<UserProfile> authors = newHashSet();

        StreamsTriggerReason reason = getTriggerReason(buildSummary);
        switch (reason)
        {
            case MANUAL:
                for (UserProfile a : getManualBuildAuthor(baseUri, buildSummary))
                {
                    authors.add(a);
                }
                break;
            case CHILD:
                for (UserProfile a : getChildBuild(baseUri, buildSummary))
                {
                    authors.add(a);
                }
                break;
            case DEPENDANT:
                for (UserProfile a : getDependencyBuild(baseUri, buildSummary))
                {
                    authors.add(a);
                }
                break;
            default:
                for (final Author author : buildSummary.getUniqueAuthors())
                {
                    authors.add(userProfileAccessor.getUserProfile(baseUri, author.getName()));
                }
        }
        return NonEmptyIterables.from(authors).getOrElse(
                ImmutableNonEmptyList.of(userProfileAccessor.getAnonymousUserProfile(baseUri)));
    }

    private StreamsTriggerReason getTriggerReason(ResultsSummary buildSummary)
    {
        TriggerReason reason = buildSummary.getTriggerReason();
        if (reason instanceof ManualBuildTriggerReason)
        {
            return MANUAL;
        }
        else if (reason instanceof CodeChangedTriggerReason)
        {
            return UPDATED;
        }
        else if (reason instanceof DependencyTriggerReason)
        {
            return DEPENDANT;
        }
        else if (reason instanceof ChildDependencyTriggerReason)
        {
            return CHILD;
        }
        else if (reason instanceof ScheduledTriggerReason)
        {
            return SCHEDULED;
        }
        else if (reason instanceof InitialBuildTriggerReason)
        {
            return INITIAL;
        }
        return UNKNOWN;
    }

    private Option<UserProfile> getManualBuildAuthor(final URI baseUri, final ResultsSummary buildSummary)
    {
        if (isManuallyTriggered(buildSummary))
        {
            ManualBuildTriggerReason triggerReason = (ManualBuildTriggerReason) buildSummary.getTriggerReason();
            return some(userProfileAccessor.getUserProfile(baseUri, triggerReason.getUserName()));
        }
        return none();
    }

    private boolean isManuallyTriggered(ResultsSummary buildSummary)
    {
        return buildSummary.getTriggerReason() instanceof ManualBuildTriggerReason;
    }

    private Option<UserProfile> getChildBuild(final URI baseUri, ResultsSummary buildSummary)
    {
        if (buildSummary.getTriggerReason() instanceof ChildDependencyTriggerReason)
        {
            ChildDependencyTriggerReason reason = (ChildDependencyTriggerReason) buildSummary.getTriggerReason();
            if (isNotBlank(reason.getTriggeringBuildResultKey()))
            {
                String bambooLogoUrl = webResourceManager.getStaticPluginResource("com.atlassian.streams:streamsWebResources", "images/bamboo-logo-48.png", UrlMode.ABSOLUTE);
                return some(new UserProfile.Builder(reason.getTriggeringBuildResultKey()).
                    profilePageUri(some(uriProvider.getBuildResultUri(baseUri, reason.getTriggeringBuildResultKey()))).
                    profilePictureUri(some(URI.create(bambooLogoUrl))).
                    build());
            }
        }
        return none();
    }

    private Option<UserProfile> getDependencyBuild(final URI baseUri, ResultsSummary buildSummary)
    {
        if (buildSummary.getTriggerReason() instanceof DependencyTriggerReason)
        {
            DependencyTriggerReason reason = (DependencyTriggerReason) buildSummary.getTriggerReason();
            if (isNotBlank(reason.getTriggeringBuildResultKey()))
            {
                String bambooLogoUrl = webResourceManager.getStaticPluginResource("com.atlassian.streams:streamsWebResources", "images/bamboo-logo-48.png", UrlMode.ABSOLUTE);
                return some(new UserProfile.Builder(reason.getTriggeringBuildResultKey()).
                    profilePageUri(some(uriProvider.getBuildResultUri(baseUri, reason.getTriggeringBuildResultKey()))).
                    profilePictureUri(some(URI.create(bambooLogoUrl))).
                    build());
            }
        }
        return none();
    }

    public Iterable<Link> buildLinks(final URI baseUri, ResultsSummary buildSummary)
    {
        ImmutableList.Builder<Link> builder = ImmutableList.builder();
        builder.add(uriProvider.getBuildIconLink(baseUri, buildSummary));

        if (hasLoggedInUser())
        {
            builder.add(uriProvider.getReplyToLink(baseUri, buildSummary));

            buildSummary.getPlanIfExists().ifPresent(plan -> {
                boolean jobEnabled = !plan.isSuspendedFromBuilding();
                if (jobEnabled) {
                    if (!(plan instanceof ImmutableJob) ||
                            !((ImmutableJob) plan).getParent().isSuspendedFromBuilding()) {
                        builder.add(uriProvider.getBuildTriggerLink(baseUri, buildSummary));
                    }
                }
            });
        }

        return builder.build();
    }

    private boolean hasLoggedInUser()
    {
        return userManager.getRemoteUsername() != null;
    }

    public Renderer getRenderer(URI baseUri, ResultsSummary buildSummary)
    {
        return rendererFactory.newRenderer(baseUri, buildSummary, getTriggerReason(buildSummary));
    }

    public enum StreamsTriggerReason
    {
        MANUAL,
        UPDATED,
        DEPENDANT,
        CHILD,
        SCHEDULED,
        INITIAL,
        UNKNOWN
    }
}
