package com.atlassian.streams.bamboo;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityObjectTypes.TypeFactory;

import static com.atlassian.streams.api.ActivityObjectTypes.ATLASSIAN_IRI_BASE;
import static com.atlassian.streams.api.ActivityObjectTypes.newTypeFactory;

public final class BambooActivityObjectTypes
{
    private static final TypeFactory bambooTypes = newTypeFactory(ATLASSIAN_IRI_BASE);
    
    public static ActivityObjectType build()
    {
        return bambooTypes.newType("build");
    }

    public static ActivityObjectType job()
    {
        return bambooTypes.newType("job");
    }
    
    public static ActivityObjectType project()
    {
        return bambooTypes.newType("bamboo-project");
    }
}
