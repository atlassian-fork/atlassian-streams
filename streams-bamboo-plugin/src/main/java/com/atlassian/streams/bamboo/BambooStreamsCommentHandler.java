package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.comment.CommentManager;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.spi.StreamsCommentHandler;

import java.net.URI;

import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNAUTHORIZED;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;

public class BambooStreamsCommentHandler implements StreamsCommentHandler
{
    private final ResultsSummaryManager resultsSummaryManager;
    private final CommentManager commentManager;
    private final BambooUserManager bambooUserManager;
    private final UserManager userManager;
    private final ApplicationProperties applicationProperties;

    public BambooStreamsCommentHandler(ResultsSummaryManager resultsSummaryManager, CommentManager commentManager,
            BambooUserManager bambooUserManager, UserManager userManager, ApplicationProperties applicationProperties)
    {
        this.resultsSummaryManager = checkNotNull(resultsSummaryManager, "resultsSummaryManager");
        this.commentManager = checkNotNull(commentManager, "commentManager");
        this.bambooUserManager = checkNotNull(bambooUserManager, "bambooUserManager");
        this.userManager = checkNotNull(userManager, "userManager");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    /**
     * Post a reply comment to a Bamboo build.
     *
     * @param baseUri the baseUri to fill in for the URI of the comment. This
     *                may be relative or absolute based on context that
     *                postReply is being called in.
     * @param itemPath the items on the path. Should be in the format of {planKey}.
     * @param comment the comment for the item
     * @return The URI of the newly added comment.
     */
    public Either<PostReplyError, URI> postReply(URI baseUri, Iterable<String> itemPath, String comment)
    {
        String buildResultKey = getOnlyElement(itemPath);
        PlanResultKey planResultKey = PlanKeys.getPlanResultKey(buildResultKey); 
        BambooUser user = bambooUserManager.getBambooUser(userManager.getRemoteUsername());
        ResultsSummary resultsSummary = resultsSummaryManager.getResultsSummary(planResultKey);
        
        if (user == null)
        {
            return Either.left(new PostReplyError(UNAUTHORIZED));
        }
        commentManager.addComment(comment, user, resultsSummary.getId());

        return Either.right(URI.create(baseUri + "/browse/" + buildResultKey));
    }

    @Override
    public Either<PostReplyError, URI> postReply(final Iterable<String> itemPath, final String comment)
    {
        return postReply(URI.create(applicationProperties.getBaseUrl()), itemPath, comment);
    }
}
