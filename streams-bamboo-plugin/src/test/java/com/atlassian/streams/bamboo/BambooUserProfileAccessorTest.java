package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.testing.AbstractUserProfileAccessorTestSuite;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooUserProfileAccessorTest extends AbstractUserProfileAccessorTestSuite
{
    @Mock
    private BambooUserManager userManager;
    @Mock
    private StreamsI18nResolver i18nResolver;

    @Before
    public void createUserProfileBuilder()
    {
        userProfileAccessor = new BambooUserProfileAccessor(userManager, getApplicationProperties(), i18nResolver);
    }

    @Before
    public void prepareUserManager()
    {
        BambooUser user = mock(BambooUser.class);
        when(user.getFullName()).thenReturn("User");
        when(user.getEmail()).thenReturn("u@c.com");
        when(userManager.getBambooUser("user")).thenReturn(user);

        BambooUser userWithSpaceInUsername = mock(BambooUser.class);
        when(userWithSpaceInUsername.getFullName()).thenReturn("User 2");
        when(userWithSpaceInUsername.getEmail()).thenReturn("u2@c.com");
        when(userManager.getBambooUser("user 2")).thenReturn(userWithSpaceInUsername);

        BambooUser userWithFunnyCharactersInName = mock(BambooUser.class);
        when(userWithFunnyCharactersInName.getFullName()).thenReturn("User <3&'>");
        when(userWithFunnyCharactersInName.getEmail()).thenReturn("u3@c.com");
        when(userManager.getBambooUser("user3")).thenReturn(userWithFunnyCharactersInName);
    }

    @Override
    protected String getProfilePathTemplate()
    {
        return "/browse/user/{username}";
    }

    @Override
    protected String getProfilePicturePathTemplate()
    {
        return null;
    }
}
