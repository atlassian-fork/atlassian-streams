package com.atlassian.streams.bamboo;

import java.net.URI;

import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;
import com.atlassian.streams.testing.AbstractEntityAssociationProviderTest;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.bamboo.BambooActivityObjectTypes.project;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BambooEntityAssociationProviderTest extends AbstractEntityAssociationProviderTest
{
    @Mock
    private BambooPermissionManager permissionManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private Project project;
    
    @Override
    public StreamsEntityAssociationProvider createProvider()
    {
        return new BambooEntityAssociationProvider(applicationProperties, permissionManager, projectManager);
    }
    
    @Override
    protected String getProjectUriPath(String key)
    {
        return "/browse/" + key;
    }
    
    @Override
    protected URI getProjectEntityType()
    {
        return project().iri(); 
    }
    
    @Override
    protected void setProjectExists(String key, boolean exists)
    {
        when(projectManager.getProjectByKey(key)).thenReturn(exists ? project : null);
    }
    
    @Override
    protected void setProjectViewPermission(String key, boolean permitted)
    {
        when(projectManager.getProjectByKey(PROJECT_ENTITY_KEY)).thenReturn(project);
        when(permissionManager.hasPermission(BambooPermission.READ, project, null)).thenReturn(permitted);
    }
    
    @Override
    protected void setProjectEditPermission(String key, boolean permitted)
    {
        when(projectManager.getProjectByKey(PROJECT_ENTITY_KEY)).thenReturn(project);
        when(permissionManager.hasPermission(BambooPermission.WRITE, project, null)).thenReturn(permitted);
    }
}
