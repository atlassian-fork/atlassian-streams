package com.atlassian.streams.bamboo;

import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
public class BambooFormatPreferenceProviderTest
{
    BambooFormatPreferenceProvider formatPreferenceProvider;

    @Before
    public void createFormatPreferenceProvider()
    {
        formatPreferenceProvider = new BambooFormatPreferenceProvider();
    }

    @Test
    public void assertThatTimeZoneIsTakenFromSystemProperties()
    {
        String timeZone = System.getProperty("user.timezone");

        try
        {
            System.setProperty("user.timezone", "Asia/Hong_Kong");
            assertThat(formatPreferenceProvider.getUserTimeZone(), is(equalTo(DateTimeZone.forID("Asia/Hong_Kong"))));
        }
        finally
        {
            System.setProperty("user.timezone", timeZone);
        }
    }

    @Test
    public void assertThatDefaultTimeZoneIsReturnedWhenTimeZoneSystemPropertyIsEmpty()
    {
        String timeZone = System.getProperty("user.timezone");

        try
        {
            System.clearProperty("user.timezone");
            assertThat(formatPreferenceProvider.getUserTimeZone(), is(equalTo(DateTimeZone.getDefault())));
        }
        finally
        {
            System.setProperty("user.timezone", timeZone);
        }
    }

    @Test
    public void assertThatDefaultTimeZoneIsReturnedWhenTimeZoneSystemPropertyIsNotAValidTimeZone()
    {
        String timeZone = System.getProperty("user.timezone");

        try
        {
            System.setProperty("user.timezone", "invalid zone");
            assertThat(formatPreferenceProvider.getUserTimeZone(), is(equalTo(DateTimeZone.getDefault())));
        }
        finally
        {
            System.setProperty("user.timezone", timeZone);
        }
    }
}
